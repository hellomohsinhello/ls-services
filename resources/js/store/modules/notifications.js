

const state = {
    notifications:[
        'hello',
        'world',
        'how',
        'are'
    ],
};

const mutations = {
    ADD_NOTIFICATION: (state, data) => {
        state.notifications.push(data)
    },
    ADD_NOTIFICATIONS: (state, data) => {
        state.notifications = data
    },
};

const actions = {
    addNotification({ commit }, data) {
        commit('ADD_NOTIFICATION', data);
    },
    addNotifications({ commit }, data) {
        commit('SET_NOTIFICATIONS', data);
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};

