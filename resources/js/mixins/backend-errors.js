import { Message } from 'element-ui';

export default {
    data(){
        return {
            errors:[]
        }
    },
    methods: {
        has(field) {
            return this.errors.hasOwnProperty(field);
        },
        any() {
            return Object.keys(this.errors).length > 0;
        },
        get(field) {
            if (this.errors[field]) {
                return this.errors[field][0];
            }
        },
        clear(field) {
            if (field) {
                delete this.errors[field];

                return;
            }

            this.errors = {};
        },
        record(data){
            console.log(data.response);
            Message({
                message: data.response.data.message,
                type: 'error',
                duration: 5 * 1000,
            });
            this.errors = data.response.data.errors;
        },
        hasAny(fields){
            return fields.some((field) => this.has(field) );
        }
    },
};
