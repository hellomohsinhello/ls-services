import Vue from 'vue';
import Cookies from 'js-cookie';
import ElementUI from 'element-ui';
import App from './views/App';
import store from './store';
import router from '@/router';
import i18n from './lang'; // Internationalization
import '@/icons'; // icon
import '@/permission'; // permission control
// import VueInstant from 'vue-instant'

window.moment = require('moment');
window.localforage = require('localforage');

import * as filters from './filters'; // global filters

Vue.use(ElementUI, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value),
});

Date.prototype.toJSON = function () { return moment(this).format(); }

Vue.use(require('vue-shortkey'));
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBoUyIEWdkjCc6OcoEAOymKn8kjnCt6hoM',
        libraries: 'places,drawing', // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    autobindAllEvents: true,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
})
// Vue.use(VueInstant)

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

Vue.config.productionTip = false;

const ROLE_ADMIN = 'admin';
console.log(ROLE_ADMIN);

import Echo from 'laravel-echo'
import {getToken} from "@/utils/auth";

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    encrypted: false,
    auth: {
        headers: {
            Authorization: 'Bearer ' + getToken()
        },
    },
    // namespace:'',
    // authEndpoint: 'http://localhost:8000/api/auth/user',
    // wsHost: window.location.hostname,
    // wsPort: 6001,
    // disableStats: true,
});

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App),
});
