import axios from 'axios';
import { Message } from 'element-ui';
import { getToken, setToken } from '@/utils/auth';
import router from 'vue-router';

// Create axios instance
const service = axios.create({
    baseURL: process.env.MIX_BASE_API,
    timeout: 10000, // Request timeout
});

// Request intercepter
service.interceptors.request.use(
    config => {
        const token = getToken();
        if (token) {
            config.headers['Authorization'] = 'Bearer ' + getToken(); // Set JWT token
            // Echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + getToken();
        }
        // config.headers['Accept'] = 'text/html, application/xhtml+xml, application/xml;q=0.9, image/webp, */*;q=0.8';

        return config;
    },
    error => {
        // Do something with request error
        console.log(error); // for debug
        Promise.reject(error);
    }
);

// response pre-processing
service.interceptors.response.use(
    response => {

        if (response.headers.authorization) {
            setToken(response.headers.authorization);
            response.data.token = response.headers.authorization;
        }

        return response.data;
    },
    error => {

        if (error.response.status === 403) {
            // console.log('Permission Error')
            Message({
                message: 'Forbidden: You don\'t have permission to access.',
                type: 'error',
                duration: 5 * 1000,
            });
            router.push('/Login')
        }

        /*let message = error.message;
        if (error.response.data && error.response.data.errors) {
            message = error.response.data.errors;
        } else if (error.response.data && error.response.data.error) {
            message = error.response.data.error;
        }*/

        // console.info(error.code);

        /*Message({
            message: message,
            type: 'error',
            duration: 5 * 1000,
        });*/
        return Promise.reject(error);
    },
);

export default service;
