/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const BuildingSurvey = {
    path: '/building-survey',
    component: Layout,
    redirect: '/building-survey/assignments',
    meta:{
        permissions: ['view menu building inspection'],
    },
    children: [
        {
            path: 'assignments',
            component: () => import('@/views/BuildingSurvey/AssignmentList'),
            name: 'BuildingSurveyList',
            meta: {
                title: 'Building & Facilities Management Consultancy',
                icon: 'side-inspection',
                noCache: false,
                // roles: ['admin','bs head'],
            },
            roles: ['admin','bs head'],
        },
        {
            path: 'assignments/:id(\\d+)/properties',
            component: () => import('@/views/BuildingSurvey/PropertyList'),
            name: 'BuildingSurveyPropertyList',
            meta: {
                title: 'Property List',
                noCache: true,
                // roles: ['admin'],
            },
            hidden: true,
        },
        {
            path: 'assignments/properties/:id(\\d+)/defects',
            component: () => import('@/views/BuildingSurvey/DefectList'),
            name: 'BuildingSurveyDefectList',
            meta: {
                title: 'Defect List',
                noCache: true,
                // roles: ['admin'],
            },
            hidden: true,
        },

    ],
};

export default BuildingSurvey;
