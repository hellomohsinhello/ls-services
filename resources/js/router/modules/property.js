/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const propertyRoutes = {
    path: '/properties',
    component: Layout,
    redirect: '/properties',
    children: [
        {
            path: 'properties/:id(\\d+)/defects',
            component: () => import('@/views/properties/Defects'),
            name: 'propertyDefects',
            meta: { title: 'Property Defects', noCache: true },
            hidden: true,
        },
        {
            path: 'properties/:id(\\d+)/valuation',
            component: () => import('@/views/properties/Valuation'),
            name: 'propertyValuation',
            meta: { title: 'Property Valuation', noCache: true },
            hidden: true,
        },
    ],
};

export default propertyRoutes;
