/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const PropertyValuation = {
    path: '/property-valuation',
    component: Layout,
    redirect: '/property-valuation/assignments',
    meta:{
        permissions: ['view menu property valuation']
    },
    children: [
        {
            path: 'assignments',
            component: () => import('@/views/PropertyValuation/AssignmentList'),
            name: 'PropertyValuationList',
            meta: {
                title: 'Property Valuation',
                icon: 'side-valuation',
                noCache: false,
                // permissions: ['view menu property valuation'],
                // roles: ['admin'],
            },
        },
        {
            path: ':id(\\d+)/properties',
            component: () => import('@/views/PropertyValuation/PropertyList'),
            name: 'PropertyValuationPropertyList',
            meta: {
                title: 'Property Detail',
                noCache: true,
                // roles: ['admin'],
            },
            hidden: true,
        },
        {
            path: 'assignments/properties/:id(\\d+)/valuation',
            component: () => import('@/views/PropertyValuation/PropertyValuation'),
            name: 'PropertyValuationPropertyValuation',
            meta: {
                title: 'Property Valuation',
                noCache: true,
                // roles: ['admin'],
            },
            hidden: true,
        },
    ],
};

export default PropertyValuation;
