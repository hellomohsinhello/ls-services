/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const assignmentRoutes = {
    path: '/assignments',
    component: Layout,
    redirect: '/assignments',
    children: [
        {
            path: '/assignments',
            component: () => import('@/views/assignments/List'),
            name: 'Assignment',
            meta: {
                title: 'Assignment',
                icon: 'assignment',
                noCache: false
            },
        },
        {
            path: 'assignments/:id(\\d+)/properties',
            component: () => import('@/views/assignments/Properties'),
            name: 'assignmentProperties',
            meta: { title: 'Assignment Properties', noCache: true },
            hidden: true,
        },
        {
            path: 'properties/:id(\\d+)/defects',
            component: () => import('@/views/properties/Defects'),
            name: 'propertyDefects',
            meta: { title: 'Property Defects', noCache: true },
            hidden: true,
        },
        {
            path: 'properties/:id(\\d+)/valuation',
            component: () => import('@/views/properties/Valuation'),
            name: 'propertyValuation',
            meta: { title: 'Property Valuation', noCache: true },
            hidden: true,
        },
    ],
};

export default assignmentRoutes;
