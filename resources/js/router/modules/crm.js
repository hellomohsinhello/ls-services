/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const FitOutManagement = {
    path: '/crm',
    component: Layout,
    redirect: '/fit-out-management/projects-list',
    title: 'CRM',
    meta:{
        // title: 'Fit-out Management',
        permissions: [],
    },
    children: [
        {
            path: 'jobs',
            component: () => import('@/views/CRM/JobList'),
            name: 'CRMJobList',
            meta: {
                title: 'CRM',
                icon: 'assignment',
                noCache: false,
                // roles: ['admin', 'owner','contractor','tenant'],
            },
        },
        /*{
            path: 'projects/:id(\\d+)/stages',
            component: () => import('@/views/FitOutManagement/ProjectStages'),
            name: 'UnitDetail',
            meta: {
                title: 'Unit Detail',
                noCache: false,
                // roles: ['admin', 'owner','contractor','tenant'],
            },
            hidden: true,
        },*/

        /*{
            path: 'assignments/:id(\\d+)/properties',
            component: () => import('@/views/assignments/Properties'),
            name: 'assignmentProperties',
            meta: { title: 'Assignment Properties', noCache: true },
            hidden: true,
        },
        {
            path: 'properties/:id(\\d+)/defects',
            component: () => import('@/views/properties/Defects'),
            name: 'propertyDefects',
            meta: { title: 'Property Defects', noCache: true },
            hidden: true,
        },
        {
            path: 'properties/:id(\\d+)/valuation',
            component: () => import('@/views/properties/Valuation'),
            name: 'propertyValuation',
            meta: { title: 'Property Valuation', noCache: true },
            hidden: true,
        },*/
    ],
};

export default FitOutManagement;
