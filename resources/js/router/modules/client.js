/** When your routing table is too long, you can split it into small modules**/
import Layout from '@/layout';

const clientRoutes = {
    path: '/clients',
    component: Layout,
    redirect: '/clients',
    name: 'Client',
    alwaysShow: true,
    meta: {
        title: 'Clients',
        icon: 'side-clients',
        permissions: ['view menu client'],
    },
    children: [
        /** Client managements */
        {
            path: 'clients',
            component: () => import('@/views/clients/List'),
            name: 'ClientList',
            meta: { title: 'Client List', icon: 'client' },
        },
        {
            path: 'clients/contacts',
            component: () => import('@/views/clients-contacts/List'),
            name: 'ClientContactList',
            meta: { title: 'Contacts List', icon: 'contact' },
        },
    ],
};

export default clientRoutes;
