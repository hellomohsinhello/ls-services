import Resource from '@/api/resource';
import request from '@/utils/request';

class StatusResource extends Resource {
    constructor() {
        super('statuses');
    }

    listResource(resource, query) {
        return request({
            url: `/${this.uri}/resource/${resource}`,//'/' + this.uri + '/resource/' + resource,
            method: 'get',
            params: query,
        });
    }
}

export { StatusResource as default };
