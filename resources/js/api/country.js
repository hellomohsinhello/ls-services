import Resource from '@/api/resource';

class CountryResource extends Resource {
    constructor() {
        super('countries');
    }

}

export { CountryResource as default };
