import request from '@/utils/request';
import Resource from '@/api/resource';

class BuildingResource extends Resource {
    constructor() {
        super('buildings');
    }


}

export { BuildingResource as default };
