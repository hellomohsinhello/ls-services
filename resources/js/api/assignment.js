import request from '@/utils/request';
import Resource from '@/api/resource';

class AssignmentResource extends Resource {
    constructor() {
        super('assignments');
    }

    updateStatus(id, status) {
        return request({
            url: '/' + this.uri + '/' + id + '/status',
            method: 'put',
            data: status,
        });
    }

    propertiesList(query) {
        return request({
            url: `/${this.uri}/properties`,
            method: 'get',
            params: query,
        });
    }

    inspectionTypeList(assignment) {
        return request({
            url: `/${this.uri}/${assignment}/inspection-types`,
            method: 'get',
        });
    }
}

export { AssignmentResource as default };
