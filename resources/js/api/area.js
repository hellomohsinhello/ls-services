import Resource from '@/api/resource';

class AreaResource extends Resource {
    constructor() {
        super('areas');
    }

}

export { AreaResource as default };
