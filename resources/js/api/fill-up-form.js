import request from '@/utils/request';
import Resource from '@/api/resource';

class FomFillUpFormResource extends Resource {

    constructor(formName) {
        super(`fuf/${formName}`);
    }

    listByTask(task,query) {
        return request({
            url: `/${this.uri}/${task}`,
            method: 'get',
            params: query,
        });
    }

    updateStatus(form,query) {
        return request({
            url: `/${this.uri}/${form}/status`,
            method: 'post',
            data: query,
        });
    }

}

export { FomFillUpFormResource as default };
