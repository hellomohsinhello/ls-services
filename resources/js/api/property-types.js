import request from '@/utils/request';
import Resource from '@/api/resource';

class PropertyTypesResource extends Resource {
    constructor() {
        super('property-types');
    }

}

export { PropertyTypesResource as default };
