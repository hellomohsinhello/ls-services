import request from '@/utils/request';

/**
 * Simple RESTful resource class
 */
class Resource {
    constructor(uri) {
        this.uri = uri;
    }
    list(query) {
        return request({
            url: '/' + this.uri,
            method: 'get',
            params: query,
        });
    }
    listAll() {
        return request({
            url: `records/${this.uri}`,
            method: 'get',
        });
    }
    resourceListAll(resource_id,resource) {
        return request({
            url: `/${this.uri}/${resource_id}/${resource}`,
            method: 'get',
        });
    }
    get(id) {
        return request({
            url: '/' + this.uri + '/' + id,
            method: 'get',
        });
    }
    store(resource) {
        return request({
            url: '/' + this.uri,
            method: 'post',
            data: resource,
        });
    }
    update(id, resource) {
        return request({
            url: '/' + this.uri + '/' + id,
            method: 'put',
            data: resource,
        });
    }
    sort(resources) {
        return request({
            url: `/${this.uri}/sort`,
            method: 'put',
            data: resources,
        });
    }
    destroy(id) {
        return request({
            url: '/' + this.uri + '/' + id,
            method: 'delete',
        });
    }
    suggestions(query) {
        return request({
            url: `/suggestions/${this.uri}/${query}`,
            method: 'get'
        });
    }

    updateAttribute(id, data) {
        return request({
            url: `/${this.uri}/${id}/update-attributes`,
            method: 'put',
            data: data,
        });
    }



}

export { Resource as default };
