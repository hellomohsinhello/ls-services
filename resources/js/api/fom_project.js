import Resource from '@/api/resource';
import request from "@/utils/request";

class FomProjectResource extends Resource {
    constructor() {
        super('fom-projects');
    }

    getProjectWithTasks(id, query) {
        return request({
            url: `${this.uri}/${id}/groups/tasks`,
            method: 'get',
            params: query,
        });
    }

    updateProjectTaskGroup(id, resource) {
        return request({
            url: `${this.uri}/${id}/groups/update`,
            method: 'put',
            data: resource,
        });
    }
    updateProjectTask(id, resource) {
        return request({
            url: `${this.uri}/${id}/tasks/update`,
            method: 'put',
            data: resource,
        });
    }
    updateProjectTaskNotification(id, resource) {
        return request({
            url: `${this.uri}/${id}/tasks/notify`,
            method: 'put',
            data: resource,
        });
    }
}

export { FomProjectResource as default };
