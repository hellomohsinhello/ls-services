import Resource from '@/api/resource';

class TaskGoupAssignResource extends Resource {
    constructor() {
        super('task-group-assign');
    }

}

export { TaskGoupAssignResource as default };
