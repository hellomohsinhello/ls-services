import request from '@/utils/request';
import Resource from '@/api/resource';
import { getToken } from '@/utils/auth';

class AssignmentResource extends Resource {
    constructor() {
        super('properties');
    }

    updateStatus(id, status) {
        return request({
            url: '/' + this.uri + '/' + id + '/status',
            method: 'put',
            data: status,
        });
    }

    defectsList(property,query) {
        return request({
            url: `/${this.uri}/${property}/defects`,
            method: 'get',
            params: query,
        });
    }

    getReport(property,query) {
        /*request.use(
            config => {
                config.headers['Accepts'] = 'image/!*';

                return config;
            }
        );*/
        return request({
            url: `/${this.uri}/${property}/report`,
            method: 'get',
            params: query,
        });
    }

}

export { AssignmentResource as default };
