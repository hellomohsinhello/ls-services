import request from '@/utils/request';
import Resource from '@/api/resource';

class AssignmentResource extends Resource {
    constructor() {
        super('defects');
    }

    updateStatus(id, status) {
        return request({
            url: '/' + this.uri + '/' + id + '/status',
            method: 'put',
            data: status,
        });
    }

    propertiesList(query) {
        return request({
            url: `/${this.uri}/properties`,
            method: 'get',
            params: query,
        });
    }

}

export { AssignmentResource as default };
