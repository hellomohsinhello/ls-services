import request from '@/utils/request';
import Resource from '@/api/resource';

class UserResource extends Resource {
    constructor() {
        super('users');
    }

    permissions(id) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'get',
        });
    }

    updatePermission(id, permissions) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'put',
            data: permissions,
        });
    }

    activities(id) {
        return request({
            url: `/${this.uri}/${id}/activities`,
            method: 'get',
        });
    }

    updatePassword(id, resource) {
        return request({
            url: `/${this.uri}/${id}/password`,
            method: 'put',
            data: resource,
        });
    }
}

export { UserResource as default };
