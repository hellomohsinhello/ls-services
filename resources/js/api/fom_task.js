import Resource from '@/api/resource';
import request from "@/utils/request";

class FomProjectResource extends Resource {
    constructor() {
        super('fom-tasks');
    }

    downloadFile(url) {
        return request({
            url: `${url}`,
            method: 'get',
        });
    }
}

export { FomProjectResource as default };
