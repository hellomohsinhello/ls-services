import request from '@/utils/request';
import Resource from '@/api/resource';

class ClientResource extends Resource {
    constructor() {
        super('clients');
    }

    typeList() {
        return request({
            url: '/' + this.uri + '/types',
            method: 'get',
        });
    }

    contactList(client_id) {
        return request({
            url: `/${this.uri}/${client_id}/client-contacts`,
            method: 'get',
        });
    }

}

export { ClientResource as default };
