import request from '@/utils/request';
import Resource from '@/api/resource';

class RoleResource extends Resource {
    constructor() {
        super('roles');
    }

    permissions(id) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'get',
        });
    }
    updateInfo(id, resource) {
        return request({
            url: this.uri + '/' + id + '/info/',
            method: 'put',
            data: resource,
        });
    }
}

export { RoleResource as default };
