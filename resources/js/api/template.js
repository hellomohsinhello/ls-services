import request from '@/utils/request';
import Resource from '@/api/resource';

class TemplateResource extends Resource {
    constructor() {
        super('templates');
    }

}

export { TemplateResource as default };
