import request from '@/utils/request';
import Resource from '@/api/resource';

class ClientContactResource extends Resource {
    constructor() {
        super('client-contacts');
    }


    permissions(id) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'get',
        });
    }

    updatePermission(id, permissions) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'put',
            data: permissions,
        });
    }
}

export { ClientContactResource as default };
