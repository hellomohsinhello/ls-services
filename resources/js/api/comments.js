import Resource from '@/api/resource';
import request from "@/utils/request";

class CommentResource extends Resource {
    constructor() {
        super('comments');
    }
    getByProject(id) {
        return request({
            url: '/' + this.uri + '/projects/' + id,
            method: 'get',
        });
    }
    /*storeByResource(project) {
        return request({
            url: '/' + this.uri,
            method: 'post',
            data: resource,
        });
    }*/
}

export { CommentResource as default };
