import request from '@/utils/request';
import Resource from '@/api/resource';

class ServiceResource extends Resource {
    constructor() {
        super('services');
    }


    permissions(id) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'get',
        });
    }

    updatePermission(id, permissions) {
        return request({
            url: '/' + this.uri + '/' + id + '/permissions',
            method: 'put',
            data: permissions,
        });
    }
}

export { ServiceResource as default };
