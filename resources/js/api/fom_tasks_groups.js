import Resource from '@/api/resource';
import request from "@/utils/request";

class FomTasksGroupsResource extends Resource {
    constructor() {
        super('fom-tasks-groups');
    }

    projectGroupTasks(query) {
        return request({
            url: `${this.uri}/groups/tasks`,
            method: 'get',
            params: query,
        });
    }

}

export { FomTasksGroupsResource as default };
