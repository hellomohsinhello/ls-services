const steps = [
    {
        element: '#hamburger-container',
        popover: {
            title: 'Hamburger',
            description: 'Open && Close sidebar',
            position: 'bottom',
        },
    },
    {
        element: '#breadcrumb-container',
        popover: {
            title: 'Breadcrumb',
            description: 'Indicate the current page location',
            position: 'bottom',
        },
    },
    {
        element: '#header-search',
        popover: {
            title: 'Page Search',
            description: 'Page search, quick navigation',
            position: 'left',
        },
    },
    {
        element: '#screenfull',
        popover: {
            title: 'Screenfull',
            description: 'Set the page into fullscreen',
            position: 'left',
        },
    },
    {
        element: '#size-select',
        popover: {
            title: 'Switch Size',
            description: 'Switch the system size',
            position: 'left',
        },
    },
    {
        element: '#tags-view-container',
        popover: {
            title: 'Tags view',
            description: 'The history of the page you visited',
            position: 'bottom',
        },
        padding: 0,
    },
    {
        element: '#filter-container',
        popover: {
            title: 'Projects filter options',
            description: 'Here you can search & filter projects according to your need.',
            position: 'bottom',
        },
    },
    {
        element: '#add-new-project',
        popover: {
            title: 'Add New Project',
            description: 'By clicking this button you will be able to see the form for creating new project.',
            position: 'bottom',
        },
    },
    {
        element: '#project-card',
        popover: {
            title: 'Project Card',
            description: 'Each card represents a project and you can see the information here.',
            position: 'right',
        },
    },
    {
        element: '#project-current-status',
        popover: {
            title: 'Current Status / Stage',
            description: 'View project current status and stage of completion.',
            position: 'right',
        },
    },
    {
        element: '#project-detail',
        popover: {
            title: 'Project Detail Page',
            description: 'Go to project details page all the tasks on each project will be listed there.',
            position: 'right',
        },
    },
    {
        element: '#project-edit',
        popover: {
            title: 'Update Info',
            description: 'Update project info button to displace form to update info.',
            position: 'left',
        },
    },
];

export default steps;
