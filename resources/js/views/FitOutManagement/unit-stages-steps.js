const steps = [
    {
        element: '#project-name',
        popover: {
            title: 'Project Name',
            description: 'Name of the project.',
            position: 'right',
        },
    },
    {
        element: '#project-actions',
        popover: {
            title: 'Actions',
            description: 'Actions to download report.',
            position: 'left',
        },
    },
    {
        element: '#steps-control',
        popover: {
            title: 'Steps Control',
            description: 'For go next and previous between stages.',
            position: 'right',
        },
    },
    {
        element: '#project-steps',
        popover: {
            title: 'Project Steps',
            description: 'projects steps or life cycle.',
            position: 'top',
        },
    },
    {
        element: '#tasks-list',
        popover: {
            title: 'Tasks List',
            description: 'These are all the tasks in current stage.',
            position: 'top',
        },
    },
];

export default steps;
