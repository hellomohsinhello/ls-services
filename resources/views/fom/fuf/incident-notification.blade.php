<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery and Access Permit Form Details</title>
</head>
<body>
<h1>Incident Notification</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" style="border-collapse: collapse;" width="100%">
    <tr>
        <th>Name</th>
        <th width="200px">Value</th>
    </tr>
    <tr>
        <td>IS THIS WORK RELATED?</td>
        <td>{{ $form->related }}</td>
    </tr>
    <tr>
        <td>If not, why?</td>
        <td>{{ $form->if_not_why }}</td>
    </tr>
    <tr>
        <td>IS THIS AN INCIDENT OR A NEAR MISS?</td>
        <td>{{ $form->near_miss }}</td>
    </tr>
    <tr>
        <td>WHAT WAS AFFECTED?</td>
        <td>{{ $form->affected }}</td>
    </tr>
    <tr>
        <td>IS THIS AN INCIDENT OR A NEAR MISS?</td>
        <td>
            @foreach($form->near_misses as $miss)
                {{ $miss }},
            @endforeach
        </td>
    </tr>
    <tr>
        <td>DATE OF OCCURRENCE</td>
        <td>{{ $form->date_of_occurrence }}</td>
    </tr>
    <tr>
        <td>INCIDENT / ACCIDENT DESCRIPTION</td>
        <td>{{ $form->accident_description }}</td>
    </tr>
    <tr>
        <td>ACTION</td>
        <td>{{ $form->action }}</td>
    </tr>
    <tr>
        <td>REPORTING PARTY DETAILS</td>
        <td>{{ $form->reporting_party_details }}</td>
    </tr>
    <tr>
        <td>Immediate Notification to EMAAR / Facility Management / Security </td>
        <td>{{ $form->reporting_party_details }}</td>
    </tr>
    <tr>
        <td>Notifiable to Government Authorities/Agencies? Specify</td>
        <td>{{ $form->government_authorities }}</td>
    </tr>
</table>

</body>
</html>