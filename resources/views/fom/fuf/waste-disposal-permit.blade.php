<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


</head>
<body>
<h1>Waste Disposal/Recycling</h1>


<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table  border="1" width="100%" style="border-collapse: collapse;">
    <tr >
        <th >Name</th>
        <th >Value</th>
    </tr>
    <tr >
        <td >Date and Time</td>
        <td >{{ $form->date_and_time }}</td>
    </tr>
    <tr >
        <td >Permit No</td>
        <td >{{ $form->permit_no }}</td>
    </tr>
    <tr >
        <td >Company</td>
        <td >{{ $form->co_company }}</td>
    </tr>
    <tr >
        <td >Address</td>
        <td >{{ $form->co_address }}</td>
    </tr>
    <tr >
        <td >Primary Contact Name</td>
        <td >{{ $form->c_primary_contact_name }}</td>
    </tr>
    <tr >
        <td >Title</td>
        <td >{{ $form->c_title }}</td>
    </tr>
    <tr >
        <td >Address</td>
        <td >{{ $form->c_address }}</td>
    </tr>
    <tr >
        <td >City</td>
        <td >{{ $form->c_city }}</td>
    </tr>
    <tr >
        <td >Contact Number</td>
        <td >{{ $form->c_contact_number }}</td>
    </tr>
    <tr >
        <td >Email</td>
        <td >{{ $form->c_email }}</td>
    </tr>
    <tr >
        <td  colspan="2"><b >DESCRIBE WASTE/RECYCLING</b></td>
    </tr>
    <tr >
        <td >Generated From</td>
        <td >{{ $form->w_generated_from }}</td>
    </tr>
    <tr >
        <td >By Whom</td>
        <td >{{ $form->w_by_whom }}</td>
    </tr>
    <tr >
        <td >Describe Material</td>
        <td >{{ $form->w_describe_material }}</td>
    </tr>
    <tr >
        <td >Quantity: (kg, l, m³ )</td>
        <td >{{ $form->w_quantity }}</td>
    </tr>
    <tr >
        <td >How is it stored?</td>
        <td >{{ $form->w_how_stored }}</td>
    </tr>
    <tr >
        <td >Disposal Method</td>
        <td >{{ $form->w_disposal_method }}</td>
    </tr>
    <tr >
        <td >Disposal Location</td>
        <td >{{ $form->w_disposal_location }}</td>
    </tr>
    <tr >
        <td >Comments</td>
        <td >{{ $form->w_comments }}</td>
    </tr>
</table>
</body>
</html>