<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

</head>
<body>
<h1>Vehicle Parking Register</h1>


<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" style="border-collapse: collapse;" width="100%">
    <tr>
        <th width="30px">SN</th>
        <th>Name</th>
        <th>Contact No.</th>
        <th>Contractor /Sub-contractor</th>
        <th>Parking Space No.</th>
        <th>Vehicle Registration No.</th>
        <th>Emirates of Registration</th>
        <th>Vehicle Brand and model</th>
        <th>Unit No.</th>
        <th>Validity</th>
        <th>Parking Floor</th>
    </tr>
    @foreach($form->vehicles as $vehicle)
        <tr>
            <td>{{ $loop->index }}</td>
            <td>{{ $vehicle->name }}</td>
            <td>{{ $vehicle->contact_no }}</td>
            <td>{{ $vehicle->contractor }}</td>
            <td>{{ $vehicle->parking_space_no }}</td>
            <td>{{ $vehicle->vehicle_registration_no }}</td>
            <td>{{ $vehicle->emirates_of_registration }}</td>
            <td>{{ $vehicle->vehicle_brand_and_model }}</td>
            <td>{{ $vehicle->unit_no }}</td>
            <td>{{ $vehicle->validity }}</td>
            <td>{{ $vehicle->parking_floor }}</td>
        </tr>
    @endforeach
</table>

</body>
</html>