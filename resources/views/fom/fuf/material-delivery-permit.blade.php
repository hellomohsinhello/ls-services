<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery and Access Permit Form Details</title>
</head>
<body>
<h1>Delivery and Access Permit</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>


<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Company</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>LS Permit No.</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>COMPANY INFORMATION</b></td>
    </tr>
    <tr>
        <td>Primary Contact Name</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Title</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>City</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Fax No.</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Phone No.</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>COMPANY INFORMATION</b></td>
    </tr>
    <tr>
        <td>Make &amp; Model</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Vehicle Height</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Vehicle Weight</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Driver's Name</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Driver's License Number</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Insurance Validity Period</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>License Plate Number</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Delivery Date</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Unit</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Description of Material (s)</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Delivery Day (s)</td>
        <td>
            @foreach($form->v_delivery_days as $day)
                {{$day}},
            @endforeach
        </td>
    </tr>
</table>
</body>
</html>