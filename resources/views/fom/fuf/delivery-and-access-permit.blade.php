<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<h1>Certificate of Conformity</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>


<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td colspan="2"><b>COMPANY INFORMATION</b></td>
    </tr>
    <tr>
        <td>Company</td>
        <td>{{ $form->co_company }}</td>
    </tr>
    <tr>
        <td>LS Permit No</td>
        <td>{{ $form->co_ls_permit_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->co_address }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>CONTACT INFORMATION</b></td>
    </tr>
    <tr>
        <td>Primary Contact Name</td>
        <td>{{ $form->c_primary_contact_name }}</td>
    </tr>
    <tr>
        <td>Title</td>
        <td>{{ $form->c_title }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->c_address }}</td>
    </tr>
    <tr>
        <td>Fax No.</td>
        <td>{{ $form->c_fax_no }}</td>
    </tr>
    <tr>
        <td>Phone No.</td>
        <td>{{ $form->c_phone_no }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->c_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>VEHICLE AND DRIVER DETAILS</b></td>
    </tr>
    <tr>
        <td>Make &amp; Model</td>
        <td>{{ $form->d_make_model }}</td>
    </tr>
    <tr>
        <td>Vehicle Height</td>
        <td>{{ $form->d_vehicle_height }}</td>
    </tr>
    <tr>
        <td>Vehicle Registration</td>
        <td>{{ $form->d_vehicle_registration }}</td>
    </tr>
    <tr>
        <td>Vehicle Weight</td>
        <td>{{ $form->d_vehicle_weight }}</td>
    </tr>
    <tr>
        <td>Driver's Name</td>
        <td>{{ $form->d_drivers_name }}</td>
    </tr>
    <tr>
        <td>Driver's License Number</td>
        <td>{{ $form->d_drivers_license_number }}</td>
    </tr>
    <tr>
        <td>Insurance Validity Period</td>
        <td>{{ $form->d_insurance_validity_period }}</td>
    </tr>
    <tr>
        <td>License Plate Number</td>
        <td>{{ $form->d_license_plate_number }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>VEHICLE AND DRIVER DETAILS</b></td>
    </tr>
    <tr>
        <td>Delivery Date</td>
        <td>{{ $form->v_delivery_date }}</td>
    </tr>
    <tr>
        <td>Unit</td>
        <td>{{ $form->v_unit }}</td>
    </tr>
    <tr>
        <td>Description of Material</td>
        <td>{{ $form->v_description_of_material }}</td>
    </tr>
</table>
</body>
</html>