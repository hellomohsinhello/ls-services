
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <title>Hello, world!</title>
</head>
<body>
<h1>Permit to Work</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Work Permit Number</td>
        <td>{{ $form->work_permit_number }}</td>
    </tr>
    <tr>
        <td>Company Name</td>
        <td>{{ $form->company_name }}</td>
    </tr>
    <tr>
        <td>Site Supervisor name</td>
        <td>{{  $form->site_supervisor_name }}</td>
    </tr>
    <tr>
        <td>Site Supervisor Contact Number</td>
        <td>{{  $form->site_supervisor_contact_number }}</td>
    </tr>
    <tr>
        <td>Approval for</td>
        <td>
            @foreach($form->approvals as $approval)
                <span>{{ $approval }}, </span>
            @endforeach
        </td>
    </tr>
    <tr>
        <td>Start Date</td>
        <td>{{ $form->start_date }}</td>
    </tr>
    <tr>
        <td>End Date</td>
        <td>{{ $form->end_date }}</td>
    </tr>
    <tr>
        <td>Approval for</td>
        <td>
            @foreach($form->design_approvals as $approval)
                <span>{{ $approval }}, </span>
            @endforeach
        </td>
    </tr>
    <tr>
        <td>Electricity Comment</td>
        <td>{{ $form->electricity_comment }}</td>
    </tr>
    <tr>
        <td>Insurance Comment</td>
        <td>{{ $form->insurance_comment }}</td>
    </tr>
    <tr>
        <td>Noisy &amp; Non- Noisy Works Comment</td>
        <td>{{ $form->noisy_non_noisy_works_comment }}</td>
    </tr>
    <tr >
        <td colspan="2"><b>Contractors</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" width="100%" style="border-collapse: collapse;">
                <tr>
                    <th>Name</th>
                    <th>No of Workers</th>
                </tr>
                @foreach($form->sub_contractors as $sub_contractor)
                    <tr>
                        <td>{{ $sub_contractor->contractor }}</td>
                        <td>{{ $sub_contractor->no_of_workers }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
</body>
</html>