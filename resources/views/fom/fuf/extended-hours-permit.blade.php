<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <title>Hello, world!</title>
</head>
<body>
<h1>Extended Hours & Friday Work Permit Form</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Company</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Date</td>
        <td>{{ $form->date }}</td>
    </tr>
    <tr>
        <td>Time (Start)</td>
        <td>{{ $form->time }}</td>
    </tr>
    <tr>
        <td>Time (End)</td>
        <td>{{ $form->time_end }}</td>
    </tr>
    <tr>
        <td>Location of Work</td>
        <td>{{ $form->location_of_work }}</td>
    </tr>
    <tr>
        <td>Description of Work</td>
        <td>{{ $form->description_of_work }}</td>
    </tr>
    <tr>
        <td>Equipment Needed</td>
        <td>{{ $form->equipment_needed }}</td>
    </tr>
    <tr>
        <td>Supervisor Name</td>
        <td>{{ $form->supervisor_name }}</td>
    </tr>
    <tr>
        <td>Supervisor Contact Number</td>
        <td>{{ $form->supervisor_contact_number }}</td>
    </tr>
    <tr>
        <td>Safety Representative Name</td>
        <td>{{ $form->safety_name }}</td>
    </tr>
    <tr>
        <td>Safety Representative Contact Number</td>
        <td>{{ $form->safety_contact_number }}</td>
    </tr>
</table>

</body>
</html>