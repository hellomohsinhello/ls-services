<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
<h1>Certificate of Conformity</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Date of Submission</td>
        <td>{{ $form->date_of_submission }}</td>
    </tr>
    <tr>
        <td>Name of Tenant</td>
        <td>{{ $form->name_of_tenant }}</td>
    </tr>
    <tr>
        <td>Shop/Unit No.</td>
        <td>{{ $form->dsa }}</td>
    </tr>
    <tr>
        <td>Tower</td>
        <td>{{ $form->tower }}</td>
    </tr>
    <tr>
        <td>File No.</td>
        <td>{{ $form->file_no }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Tenant/Tenant's Representative</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->t_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No.</td>
        <td>{{ $form->t_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->t_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->t_mobile_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->t_address }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->t_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Name of Consultant/Designer/Contractor</b></td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->c_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->c_mobile_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->c_address }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->c_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Fit Out Operation Site Representative</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->f_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No.</td>
        <td>{{ $form->f_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->f_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->f_mobile_no }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Main Contractor's Authorized Person</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="1" style="border-collapse: collapse;">
                <tr>
                    <th>SN</th>
                    <th>Description</th>
                    <th width="220px">Answer</th>
                </tr>
                <tr>
                    <td rowspan="4">1</td>
                    <td><b>Workplace Mobilization</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Is Contractor Contact Details displayed on the door i.e. PO Box S Eng. No
                        etc.
                    </td>
                    <td>
                        {{ $form->question_1 }}
                    </td>
                </tr>
                <tr>
                    <td>b) Are warning and mandatory instruction signs displayed (use PPE, Electric
                        risk and First Aid)
                    </td>
                    <td>
                        {{ $form->question_2 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Access restricted/ unauthorized entry prohibited sign displayed on the
                        door
                    </td>
                    <td>
                        {{ $form->question_3 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="6">2</td>
                    <td><b>Welfare Arrangements</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Is the first aid kit/material available</td>
                    <td>
                        {{ $form->question_4 }}
                    </td>
                </tr>
                <tr>
                    <td>b) Hygienic washroom and rest area is provided</td>
                    <td>
                        {{ $form->question_5 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Cool drinking water available are workplace</td>
                    <td>
                        {{ $form->question_6 }}
                    </td>
                </tr>
                <tr>
                    <td>d) Emergency contact detail are displayed i.e. Ambulance/Police/Fire
                        Department
                    </td>
                    <td>
                        {{ $form->question_7 }}
                    </td>
                </tr>
                <tr>
                    <td>e) Rubbish skips/trash bins provision made available</td>
                    <td>
                        {{ $form->question_8 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="5">3</td>
                    <td><b>Training</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Workplace for Safety Induction Training provided to all employees</td>
                    <td>
                        {{ $form->question_9 }}
                    </td>
                </tr>
                <tr>
                    <td>b) First Aider valid training certificate copy displayed at work place</td>
                    <td>
                        {{ $form->question_10 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Trained Fire Warden and certificate copy is available in case of hoot work
                        performed i.e. welding/ Gas/cutting works
                    </td>
                    <td>
                        {{ $form->question_11 }}
                    </td>
                </tr>
                <tr>
                    <td data-v-73df8729="">d) Working at height using Ladder (Tick Appropriate)
                        Training certificate copy displayed at work place for erection of scaffold
                    </td>
                    <td>
                        {{ $form->question_12 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="4">4</td>
                    <td><b>Fire Prevention and Fire Fighting</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Every 15 meter interval provided with one pair of portable fire
                        extinguisher (DCP) and CO2) at the workplace
                    </td>
                    <td>
                        {{ $form->question_13 }}
                    </td>
                </tr>
                <tr>
                    <td>b) Emergency escape and assembly point locations are familiar to all</td>
                    <td>
                        {{ $form->question_14 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Emergency procedure of respective building/premises explained to all</td>
                    <td>
                        {{ $form->question_15 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="5">5</td>
                    <td><b>Electricity</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Residual Current Device (RCD)/Earth Leakage Circuit Breaker (ELCB) fitted
                        distribution board is available.
                    </td>
                    <td>
                        {{ $form->question_16 }}
                    </td>
                </tr>
                <tr>
                    <td>b) Warning sign (electric shock risk) displayed on the distribution board
                    </td>
                    <td>
                        {{ $form->question_17 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Emergency light available at work place</td>
                    <td>
                        {{ $form->question_18 }}
                    </td>
                </tr>
                <tr>
                    <td>d) Workplace light and adequate ventilation arrangements are available</td>
                    <td>
                        {{ $form->question_19 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="8">6</td>
                    <td><b>General </b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a) Workplace specific health and safety plan prepared and available and
                        approved.
                    </td>
                    <td>
                        {{ $form->question_20 }}
                    </td>
                </tr>
                <tr>
                    <td>b) Workplace specific risk assessment and fire risk assessment available and
                        approved.
                    </td>
                    <td>
                        {{ $form->question_21 }}
                    </td>
                </tr>
                <tr>
                    <td>c) Safety representative/workplace supervisor available</td>
                    <td>
                        {{ $form->question_22 }}
                    </td>
                </tr>
                <tr>
                    <td>d) All employees are provided with work specific required PPE i.e. Safety
                        Shoes, Hard Hat, Goggles, Dust Mask and Safety Harness
                    </td>
                    <td>
                        {{ $form->question_23 }}
                    </td>
                </tr>
                <tr>
                    <td>e) SDS documents are available for all chemicals used at workplace</td>
                    <td>
                        {{ $form->question_24 }}
                    </td>
                </tr>
                <tr>
                    <td>f) Safe working at height procedure/instruction available are work i.e. use
                        of ladder/scaffold instruction
                    </td>
                    <td>
                        {{ $form->question_25 }}
                    </td>
                </tr>
                <tr>
                    <td>g) PPE board is available and has sufficient PPEs for incoming visitors.</td>
                    <td>
                        {{ $form->question_26 }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="5">7</td>
                    <td><b>Common Area </b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>a.) Common areas protected as per fit out manual.</td>
                    <td>
                        {{ $form->question_27 }}
                    </td>
                </tr>
                <tr>
                    <td>b.) Land Sterling work permit displayed on the unit door.</td>
                    <td>
                        {{ $form->question_28 }}
                    </td>
                </tr>
                <tr>
                    <td>c.) DM permit displayed on the unit door.</td>
                    <td>
                        {{ $form->question_29 }}
                    </td>
                </tr>
                <tr>
                    <td>d.) Contractor valid trade license displayed on the unit door.</td>
                    <td>
                        {{ $form->question_30 }}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        The main contractor undertaking to carry out the Fit-out renovation work in accordance with
                        safety practices and procedures outlined in the General International Construction Safety
                        Standards Guidelines. The main contractor is aware that they are fully liable for penalties for
                        falling to carryout their works in accordance to the aforementioned standards and guidelines.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        The main contractor is also aware that in the event ofLand Sterling issuing work orders or
                        prohibition notice, the main contractor is bounded to stop work immediately.
                        <br data-v-73df8729=""> <b data-v-73df8729="">
                            NOTE: All items and/or equipment not present at the time of inspection and listed above as a
                            NO will be the responsibility of the contractor to implement prior to the commencement of
                            fit out work. It is not the responsibility or liability of Facilities Management to check
                            that required changes have been implemented prior to the commencement of fit out activities.
                            All work undertaken by the main contractor, is done so entirely at their own risk.
                        </b></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>Name.</td>
        <td>{{ $form->a_contracotrs }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->a_titleposition }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Main Contractor's Authorized Person</b></td>
    </tr>
    <tr>
        <td>Name</td>
        <td>{{ $form->cc_name }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->cc_title_position }}</td>
    </tr>
    <tr>
        <td>Additional Comments:</td>
        <td>{{ $form->cc_additional_comments }}</td>
    </tr>
    <tr>
        <td>Name of Inspector</td>
        <td>{{ $form->cc_name_of_inspector }}</td>
    </tr>
    <tr>
        <td>Inspection Date</td>
        <td>{{ $form->cc_inspection_date }}</td>
    </tr>
</table>

</body>
</html>