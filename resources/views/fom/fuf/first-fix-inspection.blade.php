<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>
<h1>First Fix Inspection for Fit-out/Renovation</h1>


<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" style="border-collapse: collapse;" width="100%">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Name of Tenant</td>
        <td>{{ $form->t_name_of_tenant }}</td>
    </tr>
    <tr>
        <td>Shop/Unit No</td>
        <td>{{ $form->t_shop_unit_no }}</td>
    </tr>
    <tr>
        <td>File No.</td>
        <td>{{ $form->t_file_no }}</td>
    </tr>
    <tr>
        <td>Unit</td>
        <td>{{ $form->t_unit }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Name of Consultant/Designer/Contractor</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->c_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No</td>
        <td>{{ $form->c_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->c_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No</td>
        <td>{{ $form->c_mobile_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->c_address }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->c_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Fit Out Operation Site Representative</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->f_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No</td>
        <td>{{ $form->f_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->f_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No</td>
        <td>{{ $form->f_mobile_no }}</td>
    </tr>

</table>
<br>
<table border="1" style="border-collapse: collapse;">
    <tr>
        <th width="30px">SN</th>
        <th>Description</th>
        <th width="40px">Answer</th>
        <th>Remarks</th>
    </tr>
    <tr>
        <td>1</td>
        <td><b>First Fix Inspection</b></td>
        <td>{{ $form->question_1 }}</td>
        <td>{{ $form->question_1_remarks }}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Bulkhead and shadow gap installation as per approved drawing</td>
        <td>{{ $form->question_2 }}</td>
        <td>{{ $form->question_2_remarks }}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>CHW (Chilled Water) Pipe Pressure Testing at 16 bar - 24 hours and certificate including equipment calibration certificate</td>
        <td>{{ $form->question_3 }}</td>
        <td>{{ $form->question_3_remarks }}</td>
    </tr>
    <tr>
        <td>4</td>
        <td>CHW Chemical Flushing and Certificate including equipment calibration certificate</td>
        <td>{{ $form->question_4 }}</td>
        <td>{{ $form->question_4_remarks }}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Base Built Chill Water Line Flow Rate (L/S) Testing Before CHW Valve Connection- Submit Undertaking Letter If Comply</td>
        <td>{{ $form->question_5 }}</td>
        <td>{{ $form->question_5_remarks }}</td>
    </tr>
    <tr>
        <td>6</td>
        <td>SPR/Fire fighting Pipe line installations as per approved DCD & DCCA/DM drawings</td>
        <td>{{ $form->question_6 }}</td>
        <td>{{ $form->question_6_remarks }}</td>
    </tr>
    <tr>
        <td>7</td>
        <td>SPR/Fire Fighting Pipe work pressure testing at 16 bar - 24 hours and certificate</td>
        <td>{{ $form->question_7 }}</td>
        <td>{{ $form->question_7_remarks }}</td>
    </tr>
    <tr>
        <td>8</td>
        <td>Is SPR/Fire Fighting Pipe are colored (Red)</td>
        <td>{{ $form->question_8 }}</td>
        <td>{{ $form->question_8_remarks }}</td>
    </tr>
    <tr>
        <td>9</td>
        <td>Building Management to check if there is any damages in base built FA interface loop</td>
        <td>{{ $form->question_9 }}</td>
        <td>{{ $form->question_9_remarks }}</td>
    </tr>
    <tr>
        <td>10</td>
        <td>Ceiling partition as per approved DCD & DCCA/DM drawings</td>
        <td>{{ $form->question_10 }}</td>
        <td>{{ $form->question_10_remarks }}</td>
    </tr>
    <tr>
        <td>11</td>
        <td>Fire alarm installation as per approved DCD & DCCA/DM drawings</td>
        <td>{{ $form->question_11 }}</td>
        <td>{{ $form->question_11_remarks }}</td>
    </tr>
    <tr>
        <td>12</td>
        <td>Water supply pressure testing at 10 bar - 24 hours</td>
        <td>{{ $form->question_12 }}</td>
        <td>{{ $form->question_12_remarks }}</td>
    </tr>
    <tr>
        <td>13</td>
        <td>Drainage pipes gravity and flow testing.</td>
        <td>{{ $form->question_13 }}</td>
        <td>{{ $form->question_13_remarks }}</td>
    </tr>
    <tr>
        <td>14</td>
        <td>LPG Pressure Testing by DCD approved LPG Contractor (If required)</td>
        <td>{{ $form->question_14 }}</td>
        <td>{{ $form->question_14_remarks }}</td>
    </tr>
    <tr>
        <td>15</td>
        <td>FCU power provided</td>
        <td>{{ $form->question_15 }}</td>
        <td>{{ $form->question_15_remarks }}</td>
    </tr>
    <tr>
        <td>16</td>
        <td>FCU drainage installed and connected to CDP</td>
        <td>{{ $form->question_16 }}</td>
        <td>{{ $form->question_16_remarks }}</td>
    </tr>
    <tr>
        <td>17</td>
        <td>FCU locations match approved drawings</td>
        <td>{{ $form->question_17 }}</td>
        <td>{{ $form->question_17_remarks }}</td>
    </tr>
    <tr>
        <td>18</td>
        <td>Ceiling power installed as per DEWA approval (if any)</td>
        <td>{{ $form->question_18 }}</td>
        <td>{{ $form->question_18_remarks }}</td>
    </tr>
    <tr>
        <td>19</td>
        <td>Sprinklers and cables must not intersect through ducting.</td>
        <td>{{ $form->question_19 }}</td>
        <td>{{ $form->question_19_remarks }}</td>
    </tr>
    <tr>
        <td>20</td>
        <td>No proposed services penetrating the common area walls/party walls.</td>
        <td>{{ $form->question_20 }}</td>
        <td>{{ $form->question_20_remarks }}</td>
    </tr>
    <tr>
        <td>21</td>
        <td>Is there any mechanical fixing to existing window system or/and drilling on wall</td>
        <td>{{ $form->question_21 }}</td>
        <td>{{ $form->question_21_remarks }}</td>
    </tr>
</table>
</body>
</html>