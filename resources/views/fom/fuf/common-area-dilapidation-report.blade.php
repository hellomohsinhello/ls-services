<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>
<h1>Common Area Dilapidation</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Project Manager</td>
        <td>{{ $form->project_manager }}</td>
    </tr>
    <tr>
        <td>Inspection Date</td>
        <td>{{ $form->inspection_date }}</td>
    </tr>
    <tr>
        <td>Report Issue Date</td>
        <td>{{ $form->report_issue_date }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Items</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" width="100%" style="border-collapse: collapse;">
                <tr>
                    <th>Location/Floor</th>
                    <th>Defect/Breach</th>
                    <th>Description</th>
                    <th>Action Required</th>
                </tr>
                @foreach($form->items as $item)
                <tr>
                    <td>{{ $item->location_floor }}</td>
                    <td>{{ $item->defect_breach }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->action_required }}</td>
                </tr>
                @endforeach

            </table>
        </td>
    </tr>
</table>

</body>
</html>