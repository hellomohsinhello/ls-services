<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>H&S Induction/I.D. Card Form</title>
</head>
<body>
<h1>Exit Pass for Material or Equipment</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Date of Exit</td>
        <td>{{ $form->b_date_of_exit }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>COMPANY INFORMATION</b></td>
    </tr>
    <tr>
        <td>Company</td>
        <td>{{ $form->co_company }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->co_address }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>CONTACT INFORMATION</b></td>
    </tr>
    <tr>
        <td>Primary Contact Name</td>
        <td>{{ $form->c_primary_contact_name }}</td>
    </tr>
    <tr>
        <td>Title</td>
        <td>{{ $form->c_title }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->c_address }}</td>
    </tr>
    <tr>
        <td>City</td>
        <td>{{ $form->c_city }}</td>
    </tr>
    <tr>
        <td>Contact Number</td>
        <td>{{ $form->c_contact_number }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->c_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>MATERIAL or EQUIPMENT INFORMATION</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" style="border-collapse: collapse;" width="100%">
                <tr>
                    <th>No.</th>
                    <th>Description of Material or Equipment</th>
                    <th>Quantity</th>
                    <th>Remarks</th>
                </tr>
                @foreach($form->materials as $material)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $material->desc }}</td>
                        <td>{{ $material->quantity }}</td>
                        <td>{{ $material->remarks }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>

</body>
</html>