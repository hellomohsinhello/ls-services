<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <h1>Permit to Work Form Details</h1>

</head>
<body>
<h1>Permit to Work</h1>


<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th width="200px">Value</th>
    </tr>
    <tr>
        <td>Contractor Name</td>
        <td>{{ $form->contractor_name }}</td>
    </tr>
    <tr>
        <td>Contractor Contact No</td>
        <td>{{ $form->contractor_contact_no }}</td>
    </tr>
    <tr>
        <td>Site Supervisor Name</td>
        <td>{{ $form->site_supervisor_name }}</td>
    </tr>
    <tr>
        <td>Site Supervisor Contact No</td>
        <td>{{ $form->site_supervisor_contact_no }}</td>
    </tr>
    <tr>
        <td>Issuance Date</td>
        <td>{{ $form->issuance_date }}</td>
    </tr>
    <tr>
        <td>Validity Date</td>
        <td>{{ $form->validity_date }}</td>
    </tr>
    <tr>
        <td>Pre Work Condition</td>
        <td>{{ $form->pre_work_condition }}</td>
    </tr>
    <tr>
        <td>Post Work Condition</td>
        <td>{{ $form->post_work_condition }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Areas</b></td>
    </tr>
    <tr>
        <th>Working Area</th>
        <th>Description of Work</th>
    </tr>
    @foreach($form->areas as $area)
        <tr>
            <td>{{ $area->area }}</td>
            <td>{{ $area->description }}</td>
        </tr>
    @endforeach
</table>

</body>
</html>