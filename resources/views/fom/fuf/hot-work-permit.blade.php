<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery and Access Permit Form Details</title>
</head>
<body>
<h1>Hot Work Permit</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td colspan="2"><b>DETAILS OF THE PERMIT RECEIVER</b></td>
    </tr>
    <tr>
        <td>Company Name</td>
        <td>{{ $form->company_name }}</td>
    </tr>
    <tr>
        <td>Disposal Method</td>
        <td>{{ $form->company_name }}</td>
    </tr>
    <tr>
        <td>Location(s)</td>
        <td>{{ $form->location }}</td>
    </tr>
    <tr>
        <td>Permit Receiver</td>
        <td>{{ $form->permit_receiver }}</td>
    </tr>
    <tr>
        <td>Contact Mobile Number</td>
        <td>{{ $form->contact_mobile_number }}</td>
    </tr>
    <tr>
        <td>Permit Holder (Supervisor)</td>
        <td>{{ $form->permit_holder }}</td>
    </tr>
    <tr>
        <td>Name of Operative</td>
        <td>{{ $form->name_of_operative }}</td>
    </tr>
    <tr>
        <td>Employee Number</td>
        <td>{{ $form->employee_number }}</td>
    </tr>
    <tr>
        <td>Date and Time (Start)</td>
        <td>{{ $form->date_and_time }}</td>
    </tr>
    <tr>
        <td>Date and Time (End)</td>
        <td>{{ $form->date_and_time_end }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>PERMIT VALIDATIONS</b></td>
    </tr>
    <tr>
        <td>Date and Time (Start)</td>
        <td>{{ $form->s2_date_and_time }}</td>
    </tr>
    <tr>
        <td>Date and Time (End)</td>
        <td>{{ $form->s2_date_and_time_end }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>PRE-REQUISITE </b></td>
    </tr>
    <tr>
        <td>A- Flammable materials removed from location</td>
        <td>{{ $form->q1 }}</td>
    </tr>
    <tr>
        <td>B- Welding terminals terminated.</td>
        <td>{{ $form->q2 }}</td>
    </tr>
    <tr>
        <td>C- Adequate earthing.</td>
        <td>{{ $form->q3 }}</td>
    </tr>
    <tr>
        <td>D- Area of work-clean and dry.</td>
        <td>{{ $form->q4 }}</td>
    </tr>
    <tr>
        <td>E- Appropriate fire extinguisher provided.</td>
        <td>{{ $form->q5 }}</td>
    </tr>
    <tr>
        <td>F- Operatives trained in using fire extinguisher</td>
        <td>{{ $form->q6 }}</td>
    </tr>
    <tr>
        <td>G- Fire Blankets (non-flammable) provided.</td>
        <td>{{ $form->q7 }}</td>
    </tr>
    <tr>
        <td>H- Fire watch deployed.</td>
        <td>{{ $form->q8 }}</td>
    </tr>
    <tr>
        <td>I- Area barricaded.</td>
        <td>{{ $form->q9 }}</td>
    </tr>
    <tr>
        <td>J- Appropriate PPE provided.</td>
        <td>{{ $form->q10 }}</td>
    </tr>
    <tr>
        <td>K- Appropriate working platform.</td>
        <td>{{ $form->q11 }}</td>
    </tr>
    <tr>
        <td>L- Safety/warning signs in place.</td>
        <td>{{ $form->q12 }}</td>
    </tr>
    <tr>
        <td>M- Adequate ventilation.</td>
        <td>{{ $form->q13 }}</td>
    </tr>
    <tr>
        <td>N- Condition of the welding cables.</td>
        <td>{{ $form->q14 }}</td>
    </tr>
    <tr>
        <td>O- Adequate illumination.</td>
        <td>{{ $form->q15 }}</td>
    </tr>
    <tr>
        <td>P- Flash back arrestors - gas cylinders at least 3 meters away from workplace.</td>
        <td>{{ $form->q16 }}</td>
    </tr>
    <tr>
        <td>Q- Cylinders kept in trolley and secured.</td>
        <td>{{ $form->q17 }}</td>
    </tr>
    <tr>
        <td>R- Regulators in good condition.</td>
        <td>{{ $form->q18 }}</td>
    </tr>
    <tr>
        <td>S- Gas test required.</td>
        <td>{{ $form->q19 }}</td>
    </tr>
    <tr>
        <td>T- Isolation of equipment required.</td>
        <td>{{ $form->q20 }}</td>
    </tr>
</table>

</body>
</html>