<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <title>H&S Induction/I.D. Card Form</title>
</head>
<body>
<h1>H&S Induction/I.D. Card Form</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Request</td>
        <td>{{ $form->request }}</td>
    </tr>
    <tr>
        <td>Contractor Contact Number</td>
        <td>{{ $form->contractor_contact_number }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Cards</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" width="100%" style="border-collapse: collapse;">
                <tr>
                    <th>NAME</th>
                    <th>EMIRATES ID</th>
                    <th>NATIONALITY</th>
                    <th>JOB TITLE</th>
                    <th>COST</th>
                </tr>
                @foreach($form->cards as $card)
                    <tr>
                        <td>{{ $card->name }}</td>
                        <td>{{ $card->emirates_id }}</td>
                        <td>{{ $card->nationality }}</td>
                        <td>{{ $card->job_title }}</td>
                        <td>{{ $card->cost }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
</body>
</html>