<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery and Access Permit Form Details</title>
</head>
<body>
<h1>HSE Requirements Breach Report</h1>
<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Contractor Name</td>
        <td>{{ $form->contractor_name }}</td>
    </tr>
    <tr>
        <td>Date</td>
        <td>{{ $form->date }}</td>
    </tr>
    <tr>
        <td>Initiated by</td>
        <td>{{ $form->initiated_by }}</td>
    </tr>
    <tr>
        <td>LS Project Manager</td>
        <td>{{ $form->ls_project_manager }}</td>
    </tr>
    <tr>
        <td>Document Reference</td>
        <td>{{ $form->document_reference }}</td>
    </tr>
    <tr>
        <td>Requirement</td>
        <td>{{ $form->requirement }}</td>
    </tr>
    <tr>
        <td>Observation</td>
        <td>{{ $form->observation }}</td>
    </tr>
    <tr>
        <td>DISPOSITION</td>
        <td>
            @foreach($form->dispositions as $disposition)
                {{ $disposition }},
            @endforeach
        </td>
    </tr>
</table>
</body>
</html>