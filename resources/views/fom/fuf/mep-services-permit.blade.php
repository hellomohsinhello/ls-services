<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>
<h1>MEP Services Permit</h1>


<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>

<table border="1" style="border-collapse: collapse;" width="100%">
    <tr>
        <th>Name</th>
        <th width="200px">Value</th>
    </tr>
    <tr>
        <td>Tower and Unit Number</td>
        <td>{{ $form->location_and_details }}</td>
    </tr>
    <tr>
        <td>Contractor Name</td>
        <td>{{ $form->contractor_name }}</td>
    </tr>
    <tr>
        <td>Contractor Contact No</td>
        <td>{{ $form->contractor_contact_no }}</td>
    </tr>
    <tr>
        <td>Site Supervisor Name</td>
        <td>{{ $form->site_supervisor_name }}</td>
    </tr>
    <tr>
        <td>Site Supervisor Contact No.</td>
        <td>{{ $form->site_supervisor_contact_no }}</td>
    </tr>
    <tr>
        <td>Issuance Date</td>
        <td>{{ $form->issuance_date }}</td>
    </tr>
    <tr>
        <td>Validity Date</td>
        <td>{{ $form->validity_date }}</td>
    </tr>
    <tr>
        <td colspan="2" >Section 3- Inspection and Witnessing Request</td>
    </tr>
    <tr>
        <td colspan="2">
            <ol>
                @if($form->witnessing_request_1)
                    <li>Water proofing as per approved water-proofing membrane details and onsite testing.</li>
                @endif
                @if($form->witnessing_request_2)
                    <li>Drainage pipes gravity and flow testing.</li>
                @endif
                @if($form->witnessing_request_3)
                    <li>Water supply pressure testing at 6 bar - 24 hours</li>
                @endif
                @if($form->witnessing_request_4)
                    <li>CHW (Chilled Water) Pipe Pressure Testing at 12 bar - 24 hours</li>
                @endif
                @if($form->witnessing_request_5)
                    <li>CHW Chemical Flushing</li>
                @endif
                @if($form->witnessing_request_6)
                    <li>SPR/Fire Fighting Pipe work pressure testing at 12 bar - 24 hours</li>
                @endif
                @if($form->witnessing_request_7)
                    <li>LPG Pressure Testing by DCD approved LPG Contractor</li>
                @endif
                @if($form->witnessing_request_8)
                    <li>Base Built Chill Water Line Flow Rate (L/S) Testing Before CHW Valve Connection- Submit Undertaking Letter If Comply</li>
                @endif
                @if($form->witnessing_request_9)
                    <li>Ceiling Closure</li>
                @endif
            </ol>
        </td>
    </tr>
    <tr>
        <td colspan="2" >Section 4 - Request for Connection, Opening, and Closing to Building Systems</td>
    </tr>
    <tr>
        <td colspan="2">
            <ol>
                @if($form->building_systems_request_1)
                    <li>Chilled Water Valve Connection.</li>
                @endif
                @if($form->building_systems_request_2)
                    <li>Chilled Water Valve Opening.</li>
                @endif
                @if($form->building_systems_request_3)
                    <li>Chilled Water Valve Closing.</li>
                @endif
                @if($form->building_systems_request_4)
                    <li>Domestic Water Valve Connection.</li>
                @endif
                @if($form->building_systems_request_5)
                    <li>Domestic Water Valve Opening.</li>
                @endif
                @if($form->building_systems_request_6)
                    <li>Domestic Water Valve Closing.</li>
                @endif
                @if($form->building_systems_request_7)
                    <li>Zone Control Valve (ZCV) Connection -DCD approval and approved pressure test certificate to attach.</li>
                @endif
                @if($form->building_systems_request_8)
                    <li>Zone Control Valve (ZCV) Opening -DCD approval and approved pressure test certificate to attach.</li>
                @endif
                @if($form->building_systems_request_9)
                    <li>Zone Control Valve (ZCV) Closing -DCD approval and approved pressure test certificate to attach.</li>
                @endif
                @if($form->building_systems_request_10)
                    <li>Kitchen Extract Hood and Ecology Testing and Commissioning.</li>
                @endif
                @if($form->building_systems_request_11)
                    <li>Fire Alarm Interface and Commissioning by building nominated contractor.</li>
                @endif
                @if($form->building_systems_request_12)
                    <li>Drainage connection.</li>
                @endif
                @if($form->building_systems_request_13)
                    <li>DEWA meter installation.</li>
                @endif
                @if($form->building_systems_request_14)
                    <li>Water meter installation.</li>
                @endif
            </ol>
        </td>
    </tr>
    <tr>
        <td colspan="2" >Section 5 - Site Observation and Status</td>
    </tr>
    <tr>
        <td>BM/LS Comments:</td>
        <td>{{ $form->bm_ls_comments }}</td>
    </tr>

</table>


</body>
</html>