<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <title>Attendance Form Details</title>
</head>
<body>
<h1>Attendance Form</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th>Value</th>
    </tr>
    <tr>
        <td>Day</td>
        <td>{{ $form->day }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Attendances</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="1" width="100%" style="border-collapse: collapse;">
                <tr>
                    <th>No</th>
                    <th>ID No</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Designation</th>
                    <th>Passport</th>
                </tr>
                @foreach($form->attendances as $card)
                <tr>
                    <td>{{ $loop->index++ }}</td>
                    <td>{{ $card->id_no }}</td>
                    <td>{{ $card->name }}</td>
                    <td>{{ $card->company }}</td>
                    <td>{{ $card->designation }}</td>
                    <td>{{ $card->passport }}</td>
                </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
</body>
</html>