<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>H&S Induction/I.D. Card Form</title>
</head>
<body>
<h1>Disciplinary Action Issued</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th width="200px">Value</th>
    </tr>
    <tr>
        <td>Warning Type</td>
        <td>{{ $form->warning_type }}</td>
    </tr>
    <tr>
        <td>EMPLOYEE NAME</td>
        <td>{{ $form->employee_name }}</td>
    </tr>
    <tr>
        <td>Designation</td>
        <td>{{ $form->designation }}</td>
    </tr>
    <tr>
        <td>Date of Warning</td>
        <td>{{ $form->date_of_warning }}</td>
    </tr>
    <tr>
        <td>ID Number</td>
        <td>{{ $form->id_number }}</td>
    </tr>
    <tr>
        <td>REASON FOR WARNING</td>
        <td>{{ $form->warned_for }}</td>
    </tr>
    <tr>
        <td>COUNSELING COMMENTS</td>
        <td>{{ $form->counseling_comments }}</td>
    </tr>
    <tr>
        <td>EMPLOYEE'S COMMENTS</td>
        <td>{{ $form->employees_comments }}</td>
    </tr>
    <tr>
        <td>Violation Date Time</td>
        <td>{{ $form->violation_date_time }}</td>
    </tr>
    <tr>
        <td>Warning No.</td>
        <td>{{ $form->warning_no }}</td>
    </tr>
    <tr>
        <td>Company</td>
        <td>{{ $form->company }}</td>
    </tr>
    <tr>
        <td>Location/Floor/Unit Number</td>
        <td>{{ $form->location }}</td>
    </tr>
    <tr>
        <td>COUNSELING COMMENTS</td>
        <td>{{ $form->counseling_comments }}</td>
    </tr>
    <tr>
        <td>WHAT POLICY/PROCEDURE OR OTHER CONDUCT RULE HAS BEEN VIOLATED?</td>
        <td>{{ $form->violated }}</td>
    </tr>
    <tr>
        <td>IF CONDUCT IS REPEATED OR OTHER DISCIPLINARY PROBLEMS DEVELOP, WHAT ADDITIONAL ACTION WILL LIKELY RESULT?
            (Be specific, e.g. further disciplinary action, removal)
        </td>
        <td>{{ $form->additional_action }}</td>
    </tr>
    <tr>
        <td>RESTATE, FOR THE EMPLOYEE’S BENEFIT, THE EXPECTED PERFORMANCE STANDARDS WHICH MUST BE MET IN ORDER TO
            CORRECT THE PROBLEM.
        </td>
        <td>{{ $form->restate }}</td>
    </tr>
    <tr>
        <td>HAS EMPLOYEE BEEN WARNED ABOUT THIS ACTION BEFORE?</td>
        <td>{{ $form->warned_before }}</td>
    </tr>
    <tr>
        <td>How?</td>
        <td>{{ $form->warned_before_type }}</td>
    </tr>
    <tr>
        <td>BY WHOM? (Name and Title)</td>
        <td>{{ $form->warned_before_by_whom }}</td>
    </tr>
    <tr>
        <td>WHAT IS EMPLOYEE BEING REMOVED FOR?</td>
        <td>{{ $form->removed_for }}</td>
    </tr>
</table>
</body>
</html>