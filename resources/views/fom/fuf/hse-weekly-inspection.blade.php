<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Delivery and Access Permit Form Details</title>
</head>
<body>
<h1>HSE Weekly Inspection</h1>

<h2>Unit Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Building</th>
        <th width="200px">{{ $form->task->task_group->project->building->name }}</th>
    </tr>
    <tr>
        <th>Unit</th>
        <th width="200px">{{ $form->task->task_group->project->units }}</th>
    </tr>
    <tr>
        <th>Contractor</th>
        <th width="200px">{{ $form->task->task_group->project->contractor->firstname }} {{ $form->task->task_group->project->contractor->lastname }}</th>
    </tr>
    <tr>
        <th>Tenant</th>
        <th width="200px">{{ $form->task->task_group->project->tenant->firstname }} {{ $form->task->task_group->project->tenant->lastname }}</th>
    </tr>
</table>

<h2>Form Details</h2>
<table border="1" width="100%" style="border-collapse: collapse;">
    <tr>
        <th>Name</th>
        <th width="200px">Value</th>
    </tr>
    <tr>
        <td colspan="2"><b>Tenant/Tenant's Representative:</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->t_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No.</td>
        <td>{{ $form->t_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->t_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->t_mobile_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->t_address }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->t_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Name of Consultant/Designer/Contractor</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->c_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No.</td>
        <td>{{ $form->c_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Title/Position</td>
        <td>{{ $form->c_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->c_mobile_no }}</td>
    </tr>
    <tr>
        <td>Address</td>
        <td>{{ $form->c_address }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $form->c_email }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>Fit Out Operation Site Representative</b></td>
    </tr>
    <tr>
        <td>Contact Person</td>
        <td>{{ $form->r_contact_person }}</td>
    </tr>
    <tr>
        <td>Office Tel No.</td>
        <td>{{ $form->r_office_tel_no }}</td>
    </tr>
    <tr>
        <td>Warning Type</td>
        <td>{{ $form->r_title_position }}</td>
    </tr>
    <tr>
        <td>Mobile No.</td>
        <td>{{ $form->r_mobile_no }}</td>
    </tr>
    <tr>
        <td colspan="2">

            <table width="100%" border="1" style="border-collapse: collapse;">
                <tr>
                    <th width="30px">SN</th>
                    <th>Description</th>
                    <th width="50px">Score</th>
                    <th width="60px">Points Awarded</th>
                    <th width="60px">Percent</th>
                </tr>
                <tr>
                    <td rowspan="5">1</td>
                    <td colspan="4"><b>Workplace Mobilization</b></td>
                </tr>
                <tr>
                    <td>a) Is Contractor Contact Details displayed on the door i.e. PO Box S Eng. No etc.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Are warning and mandatory instruction signs displayed (use PPE, Electric risk and First
                        Aid)
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Access restricted/ unauthorized entry prohibited sign displayed on the door</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>15</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="11">2</td>
                    <td colspan="4"><b>Permits/HSE Task Permits</b></td>
                </tr>
                <tr>
                    <td>a) Applicable Work Permit is posted</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Has a Risk Assessment/STA been developed for the task and readily accessible at the
                        workface.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Have all personnel engaged in the task signed onto the Risk Assessment/STA</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Has the Risk Assessment/STA been reviewed and signed by the job Supervisor</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>e) Does the STA identify Permit To Work requirements associated with the task</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>f) Does the STA adequately reflect the task being performed</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>g) Are the STA task steps being followed</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>h) Have all potential hazards been identified (detail major hazards that have not been accounted
                        for)
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>i) Does the STA reflect any field modifications</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>45</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="6">3</td>
                    <td colspan="4"><b>Training</b></td>
                </tr>
                <tr>
                    <td>a) Workplace for Safety Induction Training provided to all employees</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) First Aider valid training certificate copy displayed at work place</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Trained Fire Warden and certificate copy is available in case of hot work performed i.e. welding/ Gas/cutting works
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Working at height using scaffold Training certificate copy
                        displayed at work place for erection of scaffold
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>20</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="8">5</td>
                    <td colspan="4"><b>Fire Prevention and Fire Fighting </b></td>
                </tr>
                <tr>
                    <td>a) Every 15 meter interval provided with one pair of portable fire extinguisher (DCP) and CO2 at
                        the workplace
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Emergency escape and assembly point locations are familiar to all.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Emergency procedure of respective building/premises explained to all</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Flammables stored properly.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>e) Containers labeled as to content.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>f) Fire extinguishers have current inspection and tagged.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>30</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="8">6</td>
                    <td colspan="4"><b>Electricity </b></td>
                </tr>
                <tr>
                    <td>a) Residual Current Device (RCD)/Earth Leakage Circuit Breaker (ELCB) fitted distribution board
                        is available.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Industrial sockets, cables, plug and connectors only to be used.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Temporary DBs must not exceed 16 amps and only competent electrician has authority to connect
                        or plug any electrical or power tools and equipment.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Warning sign (electric shock risk) displayed on the distribution board.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>e) Emergency light available at work place.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>f) Workplace light and adequate ventilation arrangements are available.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>30</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="6">7</td>
                    <td colspan="4"><b>Housekeeping </b></td>
                </tr>
                <tr>
                    <td>a) Project work areas are clean and free of excess trash, debris.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Walkways and passageways are clear.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Material or equipment properly stored.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Electrical cords, hoses etc are elevated to prevent trip hazards.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>30</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="6">8</td>
                    <td colspan="4"><b>Housekeeping </b></td>
                </tr>
                <tr>
                    <td>a) Project work areas are clean and free of excess trash, debris.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Walkways and passageways are clear.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Material or equipment properly stored.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Electrical cords, hoses etc are elevated to prevent trip hazards.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>20</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="6">9</td>
                    <td colspan="4"><b>Scaffolds and Ladders</b></td>
                </tr>
                <tr>
                    <td>a) Built, altered and dismantled by competent person.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Tagged correctly.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Proper ladder for the job performed/properly secured.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) Scaffold and/or ladder being used has current inspection.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>20</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="10">10</td>
                    <td colspan="4"><b>General</b></td>
                </tr>
                <tr>
                    <td>a) Workplace specific health and safety plan prepared and available and approved.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b) Workplace specific risk assessment and fire risk assessment available and approved.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c) Safety representative/workplace supervisor available.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) All employees are provided with work specific required PPE i.e. Safety Shoes, Hard Hat,
                        Goggles, Dust Mask and Safety Harness when required.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>e) SDS documents are available for all chemicals used at workplace.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>f) Safe working at height procedure/instruction available are work i.e. use of ladder/scaffold
                        instruction.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>g) PPE board is available and has sufficient PPEs for incoming visitors.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>h) Safety notice board is available for posting of the safety plan, method statement and risk
                        assessments , Safety Task Assignment and other relevant permits.
                    </td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>20</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td rowspan="7">11</td>
                    <td colspan="4"><b>Common Area</b></td>
                </tr>
                <tr>
                    <td>a.) Common areas protected as per fit out manual.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>b.) Land Sterling work permit displayed on the unit door.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>c.) DCCA permit displayed on the unit door.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>d) DCCA approved drawings readily available.at the unit.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td>e.) Contractor valid trade license displayed on the unit door.</td>
                    <td>5</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td><b>Possible Points Awarded</b></td>
                    <td>25</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td><b>Points Scored</b></td>
                    <td>275</td>
                    <td>{{ $form->dasas }}</td>
                    <td>{{ $form->dasas }}</td>
                </tr>
                <tr>
                    <td colspan="2"><b>Safety Assessment Summary</b></td>
                    <td colspan="3">
                        {{ $form->dasas }}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>
</html>