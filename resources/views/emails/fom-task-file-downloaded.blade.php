@component('mail::message')
# {{ $task->setting->title }} update!

Hello!

The **{{ $task->setting->title }}** in **{{ $task->task_group->setting->title }}**  has been downloaded by **{{ $user->firstname }} {{ $user->lastname }}**
Go to your profile to see the update.

@component('mail::button', ['url' => url("?#/fit-out-management/projects/{$task->project->id}/stages?group={$task->task_group->id}&step={$task->task_group->step}" )])
    Go to Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
