@component('mail::message')

Hello!

> Task *{{ $task->setting->title }}* is **{{ $task->status }}** by **{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}** please visit portal for details.

@if(\request()->has('comment'))
Says:
>{{ \request('comment') }}
@endif

@component('mail::button', ['url' => url("?#/fit-out-management/projects/{$task->project->id}/stages?group={$task->task_group->id}&step={$task->task_group->step}" )])
Go to Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
