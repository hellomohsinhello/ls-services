@component('mail::message')
# A New Land Sterling Services Account.

Hello!

Land Sterling portal Account has been created using **{{ $user->email }}** with the password **{{ \request('password') }}**.

Please visit your profile for more details.

@component('mail::button', ['url' => url("?#/fit-out-management" )])
Login Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
