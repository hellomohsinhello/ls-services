@component('mail::message')

Hello!

> Task *{{ $task->setting->title }}* fill up form submitted by **{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}** please visit portal for details.

@component('mail::button', ['url' => url("?#/fit-out-management/projects/{$task->project->id}/stages?group={$task->task_group->id}&step={$task->task_group->step}" )])
Go to Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
