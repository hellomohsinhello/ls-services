@component('mail::message')
# **{{ $task->setting->title }}** file uploaded successfully.

Hello!

>**{{ $task->setting->title }}** file uploaded successfully.

Please visit your profile for more details.

@component('mail::button', ['url' => url("?#/fit-out-management/projects/{$task->project->id}/stages?group={$task->task_group->id}&step={$task->task_group->step}" )])
    Go to Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
