@component('mail::message')
# {{ $group->setting->title }} Assigned

Hello!

> Group **{{ $group->setting->title }}** of tasks **assigned** to you by **{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}** please visit portal for details.


**Due Date** {{ $group->due_date->toDayDateTimeString() }}

Task's in this group.

@foreach($group->tasks as $task)
    @if($task->status != 'Not Required')
        {{ $task->setting->title }}
    @endif
@endforeach

@component('mail::button', ['url' => url("?#/fit-out-management/projects/{$group->project->id}/stages?group={$group->id}&step={$group->step}")])
    Go to Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
