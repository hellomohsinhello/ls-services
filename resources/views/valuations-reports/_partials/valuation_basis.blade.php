@if(in_array('Market value',$property->basis))
    <tr>
        <td>
            ‘Market value is the estimated amount for which an asset or liability should exchange on the valuation date between a willing buyer and a willing seller in an arm’s length transaction, after proper marketing and where the parties had each acted knowledgeably, prudently and without compulsion.’
        </td>
    </tr>
@endif
@if(in_array('Fair value',$property->basis))
    <tr>
        <td>'Fair value is the price that would be received to sell an asset or paid to transfer a liability in an orderly transaction between market participants at the measurement date.’</td>
    </tr>
@endif
@if(in_array('Investment Value',$property->basis))
    <tr>
        <td>'Investment value is the value of an asset to a particular owner or prospective owner for individual investment or operational objectives.'</td>
    </tr>
@endif