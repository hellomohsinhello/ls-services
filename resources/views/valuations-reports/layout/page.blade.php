<page size="A4">
    <div class="section-wrapper">
        @include('valuations-reports.layout.header')



        @yield('page-content')


    </div>

    @include('valuations-reports.layout.footer')

</page>