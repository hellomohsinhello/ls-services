<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<footer>
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td>Land Sterling Property Consultants | Confidential |
                <span class="page-num">Page <span class="default-font">[page]</span> of <span class="default-font">[sitepages]</span></span>
            </td>
        </tr>
        </tbody>
    </table>
</footer>
</body>
</html>