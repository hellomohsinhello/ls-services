<table class="table-1" cellspacing="0">
    <tbody>
    <tr><th>APPROVED BY</th></tr>
    <tr>
        <td class="p-0">

            <table class="table-blue-head" cellspacing="0">
                <tbody>
                <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Qualification</th>
                    <th>Membership Number</th>
                    <th>Signature</th>
                </tr>
                @if(request()->has('final') && $property->report_cosigned_by_valuer)
                    <tr>
                        <td>{{ $property->valuer->fullname }}</td>
                        <td>{{ $property->valuer->position }}</td>
                        <td>{{ $property->valuer->qualification }}</td>
                        <td>{{ $property->valuer->professional_membership_number }}</td>
                        <td><img class="img-fluid" style="max-width: 110px;" src="{{ $property->valuer->signature_path }}"></td>
                    </tr>
                @endif
                @if(request()->has('final'))
                    <tr>
                        <td>{{ $property->assignment->manager->fullname }}</td>
                        <td>{{ $property->assignment->manager->position }}</td>
                        <td>{{ $property->assignment->manager->qualification }}</td>
                        <td>{{ $property->assignment->manager->professional_membership_number }}</td>
                        <td><img class="img-fluid" style="max-width: 110px;" src="{{ $property->assignment->manager->signature_path }}"></td>
                    </tr>
                @endif

                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

