@foreach($property->appendices as $appendix)
    <page size="A4" id="content-pages">
        <div class="section-wrapper">

            <div class="contents">
                <h1 class="main-heading">{{ $appendix->title }}</h1>

                <table class="table-1 image-table" cellspacing="0">
                    <tbody>

                    <tr>
                        <td><img class="img-fluid" src="{{ $appendix->path }}" style="height:100%;max-height:1150px; width:100%; min-width:100%;max-width:100%;"></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </page>
@endforeach