<page size="A4" id="content-pages">
    <div class="section-wrapper">



        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>Definition of
                        @foreach($property->basis as $b)
                            {{ $b }}{{ ($loop->last)? '':',' }}
                        @endforeach
                        : RICS & IVSC</th></tr>
                @include('valuations-reports._partials.valuation_basis')
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>Valuation Methodology</th></tr>
                @if(in_array('Comparison Method',$property->methods))
                    <tr>
                        <td>The Sales Comparison Method is a process involving analysing sales and asking prices of similar units and comparing these to the subject property. Comparative analysis isolates similarities and differences in the property rights appraised, market conditions, size, location and physical features.</td>
                    </tr>
                @endif
                @if(in_array('Cost Method',$property->methods) or in_array('Cost Method (Under Construction)',$property->methods))
                    <tr>
                        <td>Depreciated Replacement Cost (DRC) is based on an estimate of the Market Value for the current use of the land in addition to the current gross replacement costs of improvements less allowances for physical deterioration and all relevant forms of obsolescence and optimization.</td>
                    </tr>
                @endif
                @if(in_array('Income Method',$property->methods))
                    <tr>
                        <td>The Income Capitalization Method is a process involving analysing the potential rental income and deducted expenses to work out the net rental income. This income is then capitalized at a rate to work out the Market Value of the property.</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>Valuation Assumptions and Caveat</th></tr>
                <tr>
                    <td><strong>General</strong><br><br>
                        We assume no responsibility for economic or physical factors which may affect the opinions in this report which occur after the valuation date.<br><br>

                        We have relied extensively on information provided during discussions and in hardcopy by The Client. Whilst we believe that the data collected is accurate
                        and reliable, Land Sterling has not, as part of the valuation, performed an independent audit or review of the information gathered and does not express an
                        opinion or any other form of assurance on the accuracy of such information. No responsibility is assumed for errors or omissions, or for information not
                        disclosed which might otherwise affect the valuation estimate.<br><br>

                        No opinion is intended to be expressed for matters which require legal expertise or specialised investigation or knowledge beyond that customarily
                        employed by property valuers. We were acting as independent and external Valuers.
                        Unless otherwise noted, no consideration has been given in this valuation to the value of the property located on the premises which is considered to be
                        personal property, only the real immovable property has been considered.<br><br>

                        Maps and exhibits included in this report are for illustration only as an aid in visualizing matters discussed within the report. They should not be considered
                        as surveys or relied upon for any other purpose, nor should they be removed from, reproduced or used apart from the report.<br><br>

                        This valuation has been prepared in accordance with the Valuation Standards of the Royal Institution of Chartered Surveyors (RICS), UAE Law and
                        Regulations, and also in conformity with the International Valuation Standards(IVS).<br><br>

                        Our valuation report is only for the use of our client and no responsibility is accepted to any third party for the whole or any part of its contents. Neither the whole nor any part of this valuation nor any reference there may be included in any published document, circular or statement nor published in any way without our prior written approval of the form or context in which it may appear<br><br>

                        <strong>Compliance of property</strong><br><br>
                        The property is valued assuming that it is in full compliance with all applicable state and local environmental regulations and laws.<br><br>
                        The property is valued assuming that all applicable zoning and use regulations and restrictions have been and will be complied with, unless otherwise stated.<br><br>

                        <strong>Condition of Land</strong><br><br>
                        Our enquiries have not revealed any contamination affecting the property or neighbouring properties which would affect our valuation.<br><br>
                        However, should it be established subsequently that contamination, seepage or pollution exists at the properties or on any neighbouring land or that the premises have been or are being put to a contaminative use this might reduce the values now reported.<br><br>

                        <strong>Suitability for Debt Financing or Mortgage Security</strong><br><br>
                        On the basis of the information provided and subject to the assumptions and limiting conditions outlined in this report, we are of the opinion that the property is suitable security for first mortgage finance purposes. This is based on the assumption that the lender complies with its own lending guidelines as well as prudent finance industry lending practices.<br><br>

                        The lender will consider all prudent aspects of credit risk for any potential borrower, including the borrower’s ability to service and repay any mortgage loan. Further, the valuation is prepared on the assumption that the lender is providing mortgage financing at a conservative and prudent loan to value ratio.
                        <br><br>
                        We anticipate a Marketing Period of '{{ $property->marketing_period }}' to achieve a sale of the property subject to an appropriate marketing strategy.<br><br>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>


    </div>

</page>