<page size="A4" id="content-pages">
    <div class="section-wrapper">

        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">SUMMARY PAGE</th></tr>

                <tr>
                    <td class="grey-bg">Valuation Company</td>
                    <td>Land Sterling Property Consultants</td>
                </tr>
                <tr>
                    <td class="grey-bg">Date of inspection</td>
                    <td>{{ optional($property->inspection_datetime)->toFormattedDateString() }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Reference Number</td>
                    <td>{{ $property->ref_number }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Applicant's Name</td>
                    <td>{{ $property->applicant_name }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Owner Name</td>
                    <td>{{ $property->current_owner }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Property Address (New)</td>
                    <td>{{ $property->address_new }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Property Address (Old)</td>
                    <td>{{ $property->address_old }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Developer</td>
                    <td>{{ $property->developer }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Project</td>
                    <td>Sulafa Tower</td>
                </tr>
                <tr>
                    <td class="grey-bg">Community</td>
                    <td>{{  $property->community->name }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Occupancy Status</td>
                    <td>{{ $property->occupancy }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Market Value (AED) </td>
                    <td>CALCUATED</td>
                </tr>
                <tr>
                    <td class="grey-bg">Estimated rental income (AED)</td>
                    <td>CALCUATED</td>
                </tr>
                <tr>
                    <td class="grey-bg">Plot Area</td>
                    <td> {{ $property->area }} {{ $property->area_unit }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Built –up Area</td>
                    <td>{{ $property->bua }} {{ $property->bua_unit }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Property Type</td>
                    <td>Apartment</td>
                </tr>
                <tr>
                    <td class="grey-bg">Inspection Type</td>
                    <td>{{ $property->type->title }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Valuation Approach/Methodology</td>
                    <td>{{ $property->primary_market_value }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Age of Building</td>
                    <td>{{ $property->age_years }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Estimated Remaining Economic life</td>
                    <td>{{ $property->life_years - $property->age_years }}</td>
                </tr>
                <tr>
                    <td class="grey-bg"> Interest to be valued/Tenure</td>
                    <td>{{ $property->interests_text }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Completion Status</td>
                    <td>{{ $property->property_status }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Total Construction Cost</td>
                    <td>{{ $property->total_construction_cost }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Existing market value at the current millstone ( as is )</td>
                    <td>{{ $property->existing_market_value }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Estimated market value upon Completion</td>
                    <td>{{ $property->estimated_market_value }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Other remarks highlighted by valuer</td>
                    <td>{{ $property->other_remarks }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Key Defects in Property</td>
                    <td>{{ $property->other_10 }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">As per Approved Municipality Plans</td>
                    <td>{{ ($property->approved_municipality_plans)? 'Yes':'No' }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Property Condition</td>
                    <td>{{ $property->property_condition }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Passing Rent</td>
                    <td>{{ $property->passing_rent }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Number of units</td>
                    <td>{{ $property->no_of_units }}</td>
                </tr>
                <tr>
                    <td class="grey-bg">Is the valuation done in accordance with RICS standards</td>
                    <td>Yes</td>
                </tr>
                </tbody>

            </table>
        </div>

    </div>
</page>