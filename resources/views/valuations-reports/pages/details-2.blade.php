<page size="A4" id="content-pages">
    <div class="section-wrapper">


        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="10">SURROUNDINGS</th></tr>


                @php
                    $surroundings_chunk = collect($property->surroundings)->chunk(3);
                @endphp

                @foreach($surroundings_chunk as $surroundings)
                    <tr>
                        @foreach($surroundings as $surrounding)
                            <td class="checkbox-width b-0">
                                <span class="checked-box" style="display: inline-block;"></span>
                                <span class="check-label b-0">{{ $surrounding }}</span>
                            </td>
                        @endforeach
                    </tr>
                @endforeach

                </tbody>
            </table>


            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>MARKET COMMENTARY</th></tr>
                <tr>
                    <td>
                        {!! $property->market_commentary !!}
                    </td>
                </tr>
                </tbody>
            </table>


            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>ASSUMPTIONS</th></tr>
                <tr>
                    <td>
                        <p>
                            {{ $property->assumptions }}
                        </p>
                        <p>
                            {{ $property->special_assumptions }}
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>

            @if(count($property->rentels))
                <table class="table-1" cellspacing="0">
                    <tbody>
                    <tr><th>Comparables for Market Rent</th></tr>
                    <tr>
                        <td class="p-0">
                            <table class="table-blue-head" cellspacing="0">
                                <tbody>
                                <tr>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Transaction Type</th>
                                    <th>Size(SqFt)</th>
                                    <th>Rate/(SqFt)</th>
                                    <th>Market Rent(AED)</th>
                                    <th>Type of Unit</th>
                                    <th>Additional Details</th>
                                </tr>

                                @foreach($property->rentels as $rentel)
                                    <tr>
                                        <td>{{ $rentel->date->toDateString() }}</td>
                                        <td>{{ $rentel->type }}</td>
                                        <td>{{ $rentel->transaction_type }}</td>
                                        <td>{{ $rentel->size }}</td>
                                        <td>CALCUATED</td>
                                        <td>{{ $rentel->market_rent }}</td>
                                        <td>{{ $rentel->unit_type }}</td>
                                        <td>{{ $rentel->details }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>



                        </td>
                    </tr>
                    </tbody>
                </table>
            @endif

        </div>


    </div>

</page>
