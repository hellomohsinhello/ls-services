<page size="A4" id="content-pages">
    <div class="section-wrapper">

        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="10">LAND DETAILS</th></tr>

                <tr>
                    <th class="check-head">Location</th>
                    <th class="check-head">Road surface</th>
                    <th class="check-head">Leveling</th>
                    <th class="check-head">Parking</th>
                    <th class="check-head">Boundaries</th>
                </tr>
                <tr>
                    <td class="checkbox-width b-0">
                        <span class="checked-box" style="display: inline-block;"></span>
                        <span class="check-label b-0">{{ $property->location }}</span>
                    </td>

                    <td class="checkbox-width b-0">
                        <span class="checked-box" style="display: inline-block;"></span>
                        {{--<span class="check-label b-0">Covered</span>--}}
                        <span class="check-label b-0">{{ $property->road_surface }}</span>
                    </td>

                    <td class="checkbox-width b-0">
                        <span class="checked-box" style="display: inline-block;"></span>
                        {{--<span class="check-label b-0">Yes</span>--}}
                        <span class="check-label b-0">{{ $property->leveling }}</span>
                    </td>

                    <td class="checkbox-width b-0">
                        <span class="checked-box" style="display: inline-block;"></span>
                        {{--<span class="check-label b-0">Available</span>--}}
                        <span class="check-label b-0">{{ $property->parking }}</span>
                    </td>

                    <td class="checkbox-width b-0">
                        <span class="checked-box" style="display: inline-block;"></span>
                        {{--<span class="check-label b-0">Fenced</span>--}}
                        <span class="check-label b-0">{{ $property->boundaries }}</span>
                    </td>
                </tr>
                </tbody>
            </table>



            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="4">SUMMARY</th></tr>
                <tr>
                    <td class="grey-bg">Age (Years)</td>
                    <td>{{ $property->age_years }}</td>
                    <td class="grey-bg">Remaining Life (Years)</td>
                    <td>{{ $property->life_years - $property->age_years }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Developer</td>
                    {{--<td>Al Sayyah Investment</td>--}}
                    <td>{{ $property->developer }}</td>
                    <td class="grey-bg">Development</td>
                    <td>{{ $property->development }}</td>
                    {{--<td>Dubai Marina</td>--}}
                </tr>

                <tr>
                    <td class="grey-bg">Built Up Area</td>
                    <td>{{ $property->bua }} {{ $property->bua_unit }}</td>
                    <td class="grey-bg">Land Area</td>
                    <td>{{ $property->area }} {{ $property->area_unit }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Tenure Status</td>
                    <td>{{ $property->tenure_status }}</td>
                    <td class="grey-bg">Occupancy</td>
                    <td>Approx 90% (Community)</td>
                </tr>

                <tr>
                    <td class="grey-bg">No of units</td>
                    <td>{{ $property->no_of_units }}</td>
                    <td class="grey-bg">Other Details</td>
                    <td class="red-bold">{{ $property->other_details }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Property Condition</td>
                    <td colspan="3">{{ $property->property_condition }}</td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Property Status</td>
                    <td colspan="3">
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>
                                <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                    <span class="checked-box" style="display: inline-block;"></span>
                                    <span class="check-label b-0">{{ $property->property_status }}</span>
                                </td>

                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Lift</td>
                    <td colspan="3">

                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>
                                <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                    <span class="checked-box" style="display: inline-block;"></span>
                                    <span class="check-label b-0">{{ $property->lift }}</span>
                                </td>

                            </tr>


                            </tbody>
                        </table>


                    </td>
                </tr>


                </tbody>
            </table>


            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">EXTERNALLY</th></tr>

                <tr class="b-grey">
                    <td class="blue-color">Boundary Wall</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>
                                @foreach($property->boundary_walls as $wall)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $wall }}</span>
                                    </td>
                                @endforeach

                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Yard Covering</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->yard_coverings as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach

                            </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Structure</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>
                                @foreach($property->structures as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach
                            </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>


                <tr class="b-grey">
                    <td class="blue-color">Cladding</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>
                                @foreach($property->cladding as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach
                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>



                <tr class="b-grey">
                    <td class="blue-color">Windows</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->windows as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach

                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>



                <tr class="b-grey">
                    <td class="blue-color">Doors</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->doors as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach
                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>



                <tr class="b-grey">
                    <td class="blue-color">Facilities</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->facilities as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach

                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>


                </tbody>
            </table>


            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">INTERNALLY</th></tr>

                <tr class="b-grey">
                    <td class="blue-color">Finishing</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                    <span class="checked-box" style="display: inline-block;"></span>
                                    <span class="check-label b-0">{{ $property->finishing  }}</span>
                                </td>

                            </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Air-Conditioning</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->air_conditioning as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach


                            </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">View</td>
                    <td>
                        <table cellspacing="0" class="table-inner">
                            <tbody>
                            <tr>

                                @foreach($property->view as $item)
                                    <td class="checkbox-width b-0" style="padding-left: 0 !important;">
                                        <span class="checked-box" style="display: inline-block;"></span>
                                        <span class="check-label b-0">{{ $item }}</span>
                                    </td>
                                @endforeach
                            </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>


                <tr class="b-grey">
                    <td class="blue-color">Main Services</td>
                    <td>
                        {{ $property->other_3 }}
                    </td>
                </tr>

                </tbody>
            </table>



            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">PROPERTY DESCRIPTION</th></tr>


                @if($property->internally)
                    @foreach($property->internally as $item)
                        <tr class="b-grey">
                            <td class="blue-color">{{ $item['title'] }}</td>
                            <td>{{ $item['content'] }}</td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>


        </div>


    </div>

</page>