{{--
@if(count($property->methods) <= 2)

@else
    <page size="A4" id="content-pages">
        <div class="section-wrapper">


            <div class="contents">
                <h1 class="main-heading">{{ $property->assignment->title }}</h1>

                @includeWhen(in_array('Comparison Method',$property->methods ?? []),'valuations-reports.methods.comparison',['prop'=>$property])

                @includeWhen(in_array('Cost Method',$property->methods ?? []),'valuations-reports.methods.cost',['prop'=>$property])

            </div>


        </div>

    </page>
@endif--}}

<page size="A4" id="content-pages">
    <div class="section-wrapper">


        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>



            @includeWhen(in_array('Comparison Method',$property->methods ?? []),'valuations-reports.methods.comparison',['prop'=>$property])

            @includeWhen(in_array('Cost Method',$property->methods ?? []),'valuations-reports.methods.cost',['prop'=>$property])

            @includeWhen(in_array('Cost Method (Under Construction)',$property->methods ?? []),'valuations-reports.methods.cost-uc',['prop'=>$property])

            @includeWhen(in_array('Income Method',$property->methods ?? []),'valuations-reports.methods.income',['prop'=>$property])

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>VALUATION RATIONALE</th></tr>
                <tr>
                    <td>
                        {{ $property->valuation_rationale_1 }}

                        <br><br>
                        {{ $property->valuation_rationale_2 }}

                    </td>
                </tr>
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>SURVEYOR DECLARATION</th></tr>
                <tr>
                    <td>Land Sterling confirms that the appointed Valuer has the necessary market knowledge, skills, and understanding to undertake this valuation competently and to provide an objective and unbiased valuation</td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>

</page>