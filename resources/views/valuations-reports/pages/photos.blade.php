@php
    $photos_chunk = $property->photos->take($property->include_photos_count)->chunk(8)
@endphp

@foreach($photos_chunk as $photos)
    <!-- Page 6 -->
    <page size="A4" id="content-pages">
        <div class="section-wrapper">

            <div class="contents">
                <h1 class="main-heading">{{ $property->photos_page_title }}</h1>
                <div>
                    @foreach($photos as $photo)
                        {{--{{ dd($photo) }}--}}

                        <div style="width:48%;float:left;padding-left: 1%">
                            <h3 class="img-title">{{ $photo->title }}</h3>
                            @if($property->water_marked_images)
                                <img class="img-fluid" src="{{ $photo->getPath('watermark') }}" style="max-height:280px;min-height:280px; object-position: center; object-fit: cover;">
                            @else
                                <img class="img-fluid" src="{{ $photo->path }}" style="max-height:280px;min-height:280px; object-position: center; object-fit: cover;">
                            @endif
                            {{--</td>--}}
                        </div>

                    @endforeach
                </div>
            </div>
        </div>

    </page>
    <!-- Page 6 end -->
@endforeach