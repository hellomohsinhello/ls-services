<page size="A4" id="content-pages">
    <div class="section-wrapper">



        <div class="contents">
            <h1 class="main-heading">{{ $property->assignment->title }}</h1>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="4">SUMMARY</th></tr>
                <tr>
                    <td class="grey-bg">Instruction Date</td>
                    <td>{{ $property->instruction_date->format('d-m-Y') }}</td>
                    <td class="grey-bg">Applicant Name</td>
                    <td>{{ $property->applicant_name }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Valuation Date</td>
                    <td>{{ optional($property->valuation_date)->format('d-m-Y') }}</td>

                    <td class="grey-bg">Property Type</td>
                    <td>{{ $property->property_type->title }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Inspection Date</td>
                    <td>{{ $property->inspection_datetime->format('d-m-Y') }}</td>
                    {{--<td>{{ dd($property) }}26-12-2019</td>--}}
                    <td class="grey-bg">Interest to be Valued</td>
                    <td>{{ $property->interests_text }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Purpose of Valuation</td>
                    <td>{{ $property->purpose_text }}</td>
                    <td class="grey-bg">-</td>
                    <td>-</td>
                </tr>

                <tr>
                    <td class="grey-bg">Methodology</td>
                    <td>{{ $property->primary_market_value }}</td>
                    <td class="grey-bg">Market Value</td>
                    <td class="red-bold">CALCUATED</td>
                </tr>

                <tr>
                    <td class="grey-bg">Current Owner</td>
                    <td>{{ $property->current_owner }}</td>
                    <td class="grey-bg">-</td>
                    <td>-</td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Basis of Valuation</td>
                    <td colspan="3">
                        @if(in_array('Market value',$property->basis))
                            ‘Market value is the estimated amount for which an asset or liability should exchange on the valuation date between a willing buyer and a willing seller in an arm’s length transaction, after proper marketing and where the parties had each acted knowledgeably, prudently and without compulsion.’
                            <br>
                        @endif
                        @if(in_array('Fair value',$property->basis))
                            'Fair value is the price that would be received to sell an asset or paid to transfer a liability in an orderly transaction between market participants at the measurement date.’
                            <br>
                        @endif
                        @if(in_array('Investment Value',$property->basis))
                            'Investment value is the value of an asset to a particular owner or prospective owner for individual investment or operational objectives.'
                        @endif
                    </td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Previous Involvement Declaration</td>
                    <td colspan="3">{{ $property->other_6 }}</td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Inspection</td>
                    <td colspan="3">The property was inspected by <b>{{ $property->inspector->fullname }}</b> who is also the author of this report. This valuation has been reviewed by <b>{{ $property->assignment->manager->fullname }}</b>. We are acting as External Valuers and we confirm that both valuers have sufficient market knowledge and the skills and understanding to undertake this valuation competently</td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Nature and source of the information relied upon</td>
                    <td colspan="3">The valuer has relied upon information provided by the Client and/or the Client’s legal representative or other professional advisers relating to tenure, tenancies, rights of way, restrictive covenants and other relevant matters. The valuer has assumed that the information provided by the client is accurate</td>
                </tr>

                <tr class="b-grey">
                    <td class="blue-color">Extent of Investigation</td>
                    <td colspan="3">The subject property was given a visual examination. These inspections and investigations were limited by the Valuer’s reasonable professional judgment to areas which were appropriate. The inspection was limited to those areas clearly visible at time of inspection, therefore no comment is made on any inaccessible or unexposed areas. The valuer has not arranged for testing of services (e.g. electrical).</td>
                </tr>
                </tbody>
            </table>


            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">LOCATION</th></tr>
                <tr>
                    <td class="grey-bg">Address (New)</td>
                    <td>{{ $property->address_new }}</td>
                </tr>

                <tr>
                    <td class="grey-bg">Address (Old)</td>
                    <td>{{ $property->address_old }}</td>

                </tr>

                <tr>
                    <td class="grey-bg">GPS</td>
                    <td><a href="https://www.google.com/maps?q={{ $property->gps }}" target="_blank">{{ $property->gps }}</a></td>
                </tr>
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>Micro Location</th></tr>
                <tr>
                    <td>{{ $property->other_8 }}</td>
                </tr>
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th>Access & Surroundings</th></tr>
                <tr>
                    <td>{{ $property->other_9 }}</td>
                </tr>
                </tbody>
            </table>

            <table class="table-1" cellspacing="0">
                <tbody>
                <tr><th colspan="2">Location</th></tr>
                <tr>
                    <td><img class="img-fluid" src="{{ $property->maps_location->marker_img_path }}"></td>
                    <td style="vertical-align: top;"><img class="img-fluid" src="{{ $property->maps_location->polygon_img_path }}"></td>
                </tr>
                </tbody>
            </table>


        </div>


    </div>

</page>