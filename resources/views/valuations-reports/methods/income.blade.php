
  <table class="table-1" cellspacing="0" style="margin-top: 15px !important;">
   <tbody>
    <tr>
        <th>Income Method</th>
    </tr>
    
    <tr>   
    <td class="p-0">
    <table class="table-blue-head" cellspacing="0">
            <tbody>
    <tr>
        <th >Type </th>
        <th >Description </th>
        <th >No of Units </th>
        <th >Area(SqFt) </th>
        <th >Rent/Unit | Rate/Area</th>
        <th colspan="2">Estimated Rent(AED)</th>
    </tr>
    @foreach($prop->breakdowns as $breakdown)
    @if($breakdown->breakdown_type == 'income')
    <tr>
        <td>{{ $breakdown->floors_units }}</td>
        <td>{{ $breakdown->type }}</td>
        <td>{{ $breakdown->no_of_units }}</td>
        <td>{{ $breakdown->area }}</td>
        <td>{{ $breakdown->rent_unit_or_rent_area }}</td>
        <td colspan="2">CALCULATED</td>
    </tr>
    @endif
    @endforeach
    <tr>
        <td><strong>Total</strong></td>
        <td colspan="5"><strong>CALCULATED</strong></td>
    </tr>
    <tr>
        <td class="grey-bg t-center"><strong>Operational Costs</strong></td>
        <td class="grey-bg t-center" colspan="3"></td>
        <td class="grey-bg t-center"><strong>%</strong></td>
        <td class="grey-bg t-center" colspan="2"><strong>Cost (AED)</strong></td>
    </tr>
    @foreach($prop->breakdowns as $breakdown)
    @if($breakdown->breakdown_type == 'cost')
    <tr>
        <td>Operating Costs</td>
        <td colspan="3"></td>
        <td> {{ $breakdown->deduction }} </td>
        <td colspan="3">CALCULATED</td>
    </tr>
    @endif
    @endforeach

    <tr>
        <td colspan="5">Net Income (AED)</td>
        <td colspan="2">CALCULATED</td>
    </tr>

    <tr>
        <td colspan="4">Capitalization Rate (%)</td>
        <td colspan="1">{{ $prop->income_method_capitalisation_rate }}</td>
        <td colspan="1"></td>
    </tr>
    @foreach($prop->breakdowns as $breakdown)
    @if($breakdown->breakdown_type == 'additional')
    <tr>
        <td colspan="5">{{ $breakdown->description }}</td>
        <td colspan="1">{{ $breakdown->value }}</td>
    </tr>
    @endif
    @endforeach
    
    
    <tr>
        <td class="t-center red-bg" colspan="1" style="border-right:1px solid #fff;">Market Value (AED)  </td>
        <td class="t-center red-bg" colspan="4">Seven Thousand Five Hundred UAE Dirhams</td>
        <td class="t-center red-bg" colspan="1">CALCULATED</td>
    </tr>
    
    </tbody>
    </table>
    
    </td>
    </tr>
    </tbody>
</table>