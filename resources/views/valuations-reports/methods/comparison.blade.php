<table class="table-1" cellspacing="0">
    <tbody>

    <tr><th>VALUATION CALCULATIONS</th></tr>
    <tr><th class="grey-bg" style="font-size: 11px;">Comparables for Market Rent</th></tr>
    <tr>
        <td class="p-0">
            <table class="table-blue-head" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Transaction Type</th>
                    <th>BUA(SqFt)</th>
                    <th>Rate/(SqFt)</th>
                    <th>Price(AED)</th>
                    @if($prop->detailed_method)
                        <th style="text-align: center;">
                            Adjustment Type
                            <table class="table-blue-head" cellspacing="0" style="border-top: 1px solid #fff;margin-top: 6px;">
                                <tbody>
                                <tr>
                                    <th>{{ $prop->adj_name_1 }}</th>
                                    <th>{{ $prop->adj_name_2 }}</th>
                                    <th>{{ $prop->adj_name_3 }}</th>
                                </tr>
                                </tbody>
                            </table>
                        </th>
                    @endif
                    <th>Adj. Avg Rate (AED)/SqFt</th>
                    <th>Additional Details</th>
                </tr>

                @foreach($prop->comparables as $comparable)
                    <tr>
                        <td>{{ optional($comparable->date)->format('d-m-Y') }}</td>
                        <td>{{ $comparable->type }}</td>
                        <td>{{ $comparable->transaction_type }}</td>
                        <td>{{ $comparable->land_area }}</td>
                        <td>CALCULATED</td>
                        <td>CALCULATED</td>
                        @if($prop->detailed_method)
                            <td class="p-0">
                                <table cellspacing="0" style="height: 44px;">
                                    <tbody>
                                    <tr>
                                        <td>{{ $comparable->adjustment_1 }}</td>
                                        <td>{{ $comparable->adjustment_2 }}</td>
                                        <td>{{ $comparable->adjustment_3 }}</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        @endif
                        <td>CALCULATED</td>
                        <td>{{ $comparable->details }}</td>
                    </tr>
                @endforeach



                <tr>
                    <td colspan="3"><strong>Average</strong></td>
                    <td><strong>CALCULATED</strong></td>
                    <td><strong>CALCULATED</strong></td>
                    <td><strong>CALCULATED</strong></td>
                    @if($prop->detailed_method)
                    <td></td>
                    @endif
                    <td><strong>CALCULATED</strong></td>
                    <td></td>
                </tr>

                <tr>
                    <td class="grey-bg t-center" colspan="3">Calculations</td>
                    <td class="grey-bg t-center" colspan="2">Size(SqFt)</td>
                    <td class="grey-bg t-center">Rate/SqFt</td>
                    <td class="grey-bg t-center">Value (AED)</td>
                    <td class="grey-bg t-center" colspan="{{ ($prop->detailed_method)? 1:2 }}"></td>
                </tr>

                <tr>
                    <td class="t-center" colspan="3"></td>
                    <td class="t-center" colspan="2">CALCULATED</td>
                    <td class="t-center">CALCULATED</td>
                    <td class="t-center"><strong>CALCULATED</strong></td>
                    <td class="t-center" colspan="{{ ($prop->detailed_method)? 1:2 }}"></td>
                </tr>

                <tr>
                    <td class="t-center red-bg" colspan="3">Market Value (Rounded)</td>
                    <td class="t-center red-bg" colspan="{{ ($prop->detailed_method)? 5:6 }}">Seven Hundred Twenty-five Thousand UAE Dirhams</td>

                </tr>
                <tr>

                    <td colspan="{{ ($prop->detailed_method)? 6:7 }}">Annual market rent (AED)</td>
                    <td colspan="2">CALCULATED</td>
                </tr>

                </tbody>
            </table>
        </td>

    </tr>
    </tbody>
</table>