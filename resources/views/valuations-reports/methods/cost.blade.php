<table class="table-1" cellspacing="0" style="margin-top: 15px !important; margin-bottom: 15px !important">
    <tbody>

    <tr><th>VALUATION CALCULATIONS</th></tr>
    <tr><th class="grey-bg" style="font-size: 11px;">Cost Method</th></tr>
    <tr>
        <td class="p-0">
            <table class="table-blue-head" cellspacing="0" width="100%">
                <tbody>
   
    <tr>
        <td>Element</td>
        <td>Size (SqFt)</td>
        <td>Rate (AED/SqFt)</td>
        <td>Age (Years)</td>
        <td>Life Expectancy (Years)</td>
        <td>% Depreciation</td>
        <td>Value (AED)</td>
    </tr>

    @foreach($prop->costs as $cost)
        @if ($cost->type == 'normal')
            <tr>
                <td>{{ $cost->description }}</td>
                <td>{{ $cost->area_length }}</td>
                <td>{{ $cost->rate_area }}</td>
                <td>{{ $cost->age }}</td>
                <td>{{ $cost->life_expectancy }}</td>
                <td>CALCULATED</td>
                <td>CALCULATED</td>
            </tr>
        @endif
    @endforeach

    <tr>
        <td colspan="6"><strong>Depreciation</strong></td>

        <td style="color:#9e0d21;" colspan="1"><strong>CALCULATED</strong></td>
    </tr>
    <tr>
        <td class="grey-bg t-center" colspan="2">Developer's Profit</td>
        <td class="grey-bg t-center" colspan="4">{{ $prop->cost_method_dev_profit }} %</td>
        <td class="grey-bg t-center" colspan="1">CALCULATED</td>
    </tr>
    @foreach($prop->costs as $cost)
        @if ($cost->type == 'additional')
            <tr>
                <td colspan="6"><strong>{{ $cost->description }}</strong></td>
                <td colspan="1"><strong>{{ $cost->total_cost }}</strong></td>
            </tr>
        @endif
    @endforeach
    <tr>

        <td class="t-center red-bg" colspan="2">Total value (AED)</td>

        <td class="t-center red-bg" colspan="4">Two Million Seven Hundred Thirty-nine Thousand Eight Hundred  UAE Dirhams</td>
        <td class="t-center red-bg">CALCULATED</td>
    </tr>
    
    
    
     </tbody>
            </table>
        </td>

    </tr>
    </tbody>
</table>