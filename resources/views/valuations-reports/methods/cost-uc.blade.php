<table class="table-1" cellspacing="0">
    <tbody>
    <tr>
        <th>Cost Method (Under Construction)</th>
    </tr>

    <tr>

        <td class="p-0">
            <table class="table-blue-head" cellspacing="0">
                <tbody>
                <tr>
                    <th>Element</th>
                    <th>Area</th>
                    <th>% Completion</th>
                    <th>(AED/SqFt)</th>
                    <th>Cost - As is (AED)</th>
                    <th>Cost - As Completed (AED)</th>
                </tr>

                @foreach($prop->costs_uc as $cost)
                    @if($cost->type == 'normal')
                        <tr>
                            <td>{{ $cost->description }}</td>
                            <td>{{ $cost->area_length }}</td>
                            <td>{{ $cost->complete_percent }}</td>
                            <td>{{ $cost->rate_area }}</td>
                            <td>{{ number_format(\App\Laravue\MethodCalculations::costAsIs($cost)) }}</td>
                            <td>{{ number_format(\App\Laravue\MethodCalculations::costAsCompleted($cost)) }}</td>
                        </tr>
                    @endif
                @endforeach

                <tr>
                    <td colspan="1" class="grey-bg t-center"><strong>Fixed Costs</strong></td>
                    <td colspan="6" class="grey-bg t-center"></td>
                </tr>
                @foreach($prop->costs_uc as $cost)
                    @if($cost->type == 'permanent')
                        <tr>
                            <td>{{ $cost->description }}</td>
                            <td>{{ $cost->area_length }}</td>
                            <td>{{ $cost->complete_percent }}</td>
                            <td></td>
                            <td>{{ number_format(\App\Laravue\MethodCalculations::permanentCostAsIs($cost,$prop->costs_uc)) }} </td>
                            <td>{{ number_format(\App\Laravue\MethodCalculations::permanentCostAsCompleted($cost,$prop->costs_uc)) }} </td>
                        </tr>
                    @endif
                @endforeach
                <tr>
                    <td colspan="4"><strong>Developer's Profit</strong></td>
                    <td><strong>{{ $prop->cost_method_uc_dev_profit_as_is }} %</strong></td>
                    <td><strong>{{ $prop->cost_method_uc_dev_profit_as_comp }} %</strong></td>
                </tr>



                <tr>
                    <td colspan="4" class="t-center red-bg"><strong>Total value (AED)</strong></td>
                    <td class="t-center red-bg"><strong>{{ number_format(\App\Laravue\MethodCalculations::total_cost_as_is($prop->costs_uc,$prop)) }}</strong></td>
                    <td class="t-center red-bg"><strong>{{ number_format(\App\Laravue\MethodCalculations::total_cost_as_completed($prop->costs_uc,$prop)) }}</strong></td>
                </tr>
                </tbody>
            </table>

        </td>

    </tr>

    </tbody>
</table>