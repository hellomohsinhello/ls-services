<table border="1">
    <tr>
        <th style="background-color: #0c4789;color: #FFFFFF;" align="center" width="10">#</th>
        @if ($property->template->settings->location)
            <th  style="background-color: #0c4789;color: #FFFFFF;" align="center" width="10">LOCATION</th>
        @endif
        @if ($property->template->settings->element)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="15">ELEMENT</th>
        @endif
        @if ($property->template->settings->description)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">DESCRIPTION</th>
        @endif
        @if ($property->template->settings->recommendation)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">RECOMMENDATION</th>
        @endif
        @if ($property->template->settings->liable_parties)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">LIABLE PARTIES</th>
        @endif
        @if ($property->template->settings->component)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">COMPONENT</th>
        @endif
        @if ($property->template->settings->poac)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="7">POAC</th>
        @endif
        @if ($property->template->settings->rec)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="7">REC</th>
        @endif
        @if ($property->template->settings->status)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="7">STATUS</th>
        @endif
        @if ($property->template->settings->unit)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">UNIT</th>
        @endif
        @if ($property->template->settings->priority)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="7">PRIORITY</th>
        @endif
        @if ($property->template->settings->quantity)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">QUANTITY</th>
        @endif
        @if ($property->template->settings->rate)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">RATE</th>
        @endif
        @if ($property->template->settings->rate && $property->template->settings->quantity)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">TOTAL</th>
        @endif
        @if ($property->template->settings->nrm_code)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="12">NRM CODE</th>
        @endif
        @if ($property->template->settings->conditions)
            <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="20">CONDITIONS</th>
        @endif
        <th style="background-color: #0c4789;color: #FFFFFF;"  align="center" width="30">DEFECT</th>
    </tr>
    @foreach($defects as $defect)
        <tr>
            <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $loop->index + 1 }}</td>
            @if ($property->template->settings->location)
                <td  align="center" style="word-wrap: break-word;">{{ $defect->location }}</td>
            @endif
            @if ($property->template->settings->element)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ optional($defect->element)->name }}</td>
            @endif
            @if ($property->template->settings->description)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->description }}</td>
            @endif
            @if ($property->template->settings->recommendation)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->recommendation }}</td>
            @endif
            @if ($property->template->settings->liable_parties)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->liable_parties }}</td>
            @endif
            @if ($property->template->settings->status)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->status }}</td>
            @endif
            @if ($property->template->settings->component)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->component }}</td>
            @endif
            @if ($property->template->settings->poac)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->poac }}</td>
            @endif
            @if ($property->template->settings->rec)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->rec }}</td>
            @endif
            @if ($property->template->settings->unit)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->unit }}</td>
            @endif
            @if ($property->template->settings->priority)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->priority }}</td>
            @endif
            @if ($property->template->settings->quantity)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->quantity }}</td>
            @endif
            @if ($property->template->settings->rate)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->rate }}</td>
            @endif
            @if ($property->template->settings->rate && $property->template->settings->quantity)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->rate * $defect->quantity }}</td>
            @endif
            @if ($property->template->settings->nrm_code)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->nrm_code }}</td>
            @endif
            @if ($property->template->settings->conditions)
                <td align="center" style="vertical-align:middle;word-wrap: break-word;">{{ $defect->conditions }}</td>
            @endif
            <td><img src="{{ (request()->get('format') == 'ms-excel')? $defect->image_path:$defect->image_url }}" width="100px"></td>
        </tr>
    @endforeach

</table>

<style>
    td{
        word-break: break-all;
    }
</style>
