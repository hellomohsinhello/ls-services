<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use \App\Laravue\Models\User;
use \App\Models\FomProject;
use Faker\Generator as Faker;

$factory->define(FomProject::class, function (Faker $faker) {
    $types = ['villa','home','shop','land'];
    $statuses = ['Occupied','Rented','Filled'];
    /* 'units' => json_encode([
            [
                'letter' => 'A',
                'floors' => rand(1,5),
                'levels'=> rand(1,5),
                'prefix' => rand(0,1)
            ],[
                'letter' => 'B',
                'floors' => rand(1,5),
                'levels'=> rand(1,5),
                'prefix' => rand(0,1)
            ],
        ]),*/
    return [
        'building_id' => \App\Models\FomBuilding::all()->random(1)->first(),
        'units' => 'A'.rand(1,9).'0'.rand(1,9),
        'owner_id' => User::all()->random(1)->first(),
        'tenant_id' => User::all()->random(1)->first(),
        'contractor_id' => User::all()->random(1)->first(),
//        'tenant_id' => User::whereHas("roles", function($q){ $q->where("name", \App\Laravue\Acl::ROLE_TENANT); })->get()->random(1)->first(),
//        'contractor_id' => User::whereHas("roles", function($q){ $q->where("name", \App\Laravue\Acl::ROLE_CONTRACTOR); })->get()->random(1)->first(),
        'type' => $types[rand(0,3)],
        'status' => $statuses[rand(0,1)],
        'area' => rand(50,100),
        'levels' => rand(50,100),
        'oa_email' => 'oa@gmail.com',
        'building_management_email' => 'bm@gmail.com'
    ];
});
