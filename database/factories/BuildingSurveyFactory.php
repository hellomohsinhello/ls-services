<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6),
        'due_date' => now()->addDays(rand(-10,10)),
        'manager' => \App\Laravue\Models\User::all()->random(1)->first(),
        'client_ref' => $faker->ean8,
        'report_lang' => (['ar','en'])[rand(0,1)],
        'client_contact' => \App\Laravue\Models\User::all()->random(1)->first(),
        'progress' => rand(1,100),
        'is_payment_received' => rand(0,1),
        'team' => 'required',
    ];
});
