<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\ClientContact::class, function (Faker $faker) {
    $jobTitle = $faker->jobTitle;
    $area =  \App\Models\Area::all()->random(1)->first();
    return [
        'salutation' => $faker->title,
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'designation' => $jobTitle,
        'designation_ar' =>  $jobTitle.' ar',
        'mobile_number' => $faker->phoneNumber,
        'phone_number' => $faker->phoneNumber,
        'client_id' => \App\Models\Client::all()->random(1)->first()->id,
        'area_id' => $area->id,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
