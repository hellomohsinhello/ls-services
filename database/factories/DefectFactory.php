<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Defect::class, function (Faker $faker) {

    $liable_parties = ['Developer','OA/FM'];

    $statuses = ['Incomp./Major', 'Incomp./Minor', 'Completed'];

    $units = ['Sq.m', 'Lump Sum','m'];

    $quantity = rand(5,15);
    $rate = rand(50,150);

    return [
        'number' => rand(10,15),
        'location' => $faker->streetAddress,
        'element_id' => \App\Models\Element::all()->random(1)->first()->id,
        'inspection_type_id' => rand(1,2),
        'description' => $faker->words(3,true),
        'recommendation' => $faker->words(3,true),
        'rec' => $faker->words(3,true),
        'poac' => $faker->words(3,true),
        'component' => $faker->words(3,true),
        'liable_parties' => $liable_parties[rand(0,1)],
        'status' => $statuses[rand(0,2)],
        'unit' => $units[rand(0,2)],
        'quantity' => $quantity,
        'rate' => $rate,
        'rating' => json_encode(
            [
                'ss_requirements' => rand(1,5),
                'po_condition' => rand(1,5),
                'fs_performance' => rand(1,5),
                'ee_performance' => rand(1,5),
            ]
        ),
        'conditions' => $faker->words(5, true),
        'nrm_code' => $faker->postcode,
    ];

});
