<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Lead;
use Faker\Generator as Faker;

$factory->define(Lead::class, function (Faker $faker) {

    $types = ['Inquiry','Proposal','Failed','Success'];
    $sources = ['Phone','Email'];

    $foor_breakdown = ['3BR','G','1SR'];
    $services = ['Building Survey','Property Valuation'];

    return [
        'type' => $types[rand(0,3)],
        'source' => $sources[rand(0,1)],
        'client_name' => $faker->name,
        'community' => $faker->address,
        'floor_breakdown' => $foor_breakdown[rand(0,2)],
        'rfp_received' => now()->subDays(rand(1,50)),
        'proposal_sent' => now()->subDays(rand(1,50)),
        'ls_ref' => '2019-VAL-12'.rand(10,20),
//        'project_name' => $faker->project_name,
        'request_service' => $services[rand(0,1)],
        'property_name' => $faker->address,
        'contact_name' => $faker->phoneNumber,
        'mobile' => $faker->phoneNumber,
        'telephone' => $faker->phoneNumber,
        'email' => $faker->companyEmail,
    ];
});
