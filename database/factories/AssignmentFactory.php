<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Assignment::class, function (Faker $faker) {
    return [
        'title' => $faker->streetAddress,
        'due_date' => now()->addDays(rand(-10,30)),
        'manager_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'client_contact_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'client_ref' => $faker->ean8,
        'report_lang' => (['ar','en'])[rand(0,1)],
        'user_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'status' => 'New',
        'is_portfolio' => rand(0,1),
        'ref_no' => assignment_ref_key(),
    ];
});
