<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BuildingSurveyProperty;
use Faker\Generator as Faker;

$factory->define(BuildingSurveyProperty::class, function (Faker $faker) {

    $template = \App\Models\Template::where('service_id',1)->get()->random(1)->first();
    $area = \App\Models\Area::all()->random(1)->first();

    $property_types = [
        'Villa',
        'Apartment',
        'Land',
        'Compound',
        'Tower',
        'Warehouse',
        'Mall - Shopping Center',
        'Building',
        'Office Units',
        'Office Unit',
        'Townhouse',
        'Industrial Facility',
        'Retail Unit',
        'Labour Camp',
        'Lands + Buildings',
        'School',
        'Temporary Structures',
        'Residential Villa'
    ];

    return [
        'template_id' => $template->id,
        'area_id' => $area->id,
        'surveyor_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'name' => $faker->buildingNumber . ' Property Name',
        'plot_number' => $faker->numerify('#####'),
        'property_number' => rand(500,1000),
        'property_type' => $property_types[rand(0,17)],
//        'assignment_id' => $assignment->id,
    ];
});
