<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Property::class, function (Faker $faker) {

    $assignment = \App\Models\Assignment::all()->random(1)->first();

    $area = \App\Models\Area::all()->random(1)->first();

    $units = ['sq f','sq m'];

    $service = $assignment->service;

    $template = \App\Models\Template::where('service_id',2)->first();

    return [
        'template_id' => $template->id,
        'property_number' => rand(500,1000),
        'instruction_date' => now()->addDays(rand(1,10)),
        'area' => $faker->numerify('###'),
        'area_unit' => $units[rand(0,1)],
        'property_type_id' => \App\Models\PropertyType::all()->random(1)->first()->id,
        'bua' => $faker->numerify('###'),
        'bua_unit' => $units[rand(0,1)],
        'client_contact_id' => \App\Laravue\Models\User::all()->random(1)->first()->id ,
        'valuer_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'applicant_name' => $faker->words(4, true),
        'inspection_type_id' => rand(1,5),//$assignment->service()->inspections()->where('service_id',$service->id)->get()->first()->id,
        'plot_number' => $faker->numerify('#####'),
        'area_id' => $area->id,
        'inspector_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'current_owner' => $faker->name,
        'assignment_id' => $assignment->id,
        'name' => $faker->buildingNumber . ' Property Name',
        'inspection_datetime' => now()->addDays(rand(0,15)),
    ];
});
