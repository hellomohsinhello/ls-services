<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\BuildingSurveyAssignment::class, function (Faker $faker) {

    $statuses = ['New', 'Assigned', 'Completed', 'Finalized', 'Approved'];
    $progress_statuses = ['Started', 'Insp/Test', 'Draft Report', 'Final Report'];
    $payment_statuses = ['Yes','No'];
    $services = ['BCA','RFS','RV','CA','SCM'];

    return [
        'title' => $faker->streetAddress,
        'due_date' => now()->addDays(rand(-10,30)),//$faker->dateTimeBetween('+1 week', '+1 month')->getTimestamp(),
        'manager_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'client_contact_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'client_ref' => $faker->ean8,
        'payment_received' => $payment_statuses[rand(0,1)],
        'progress_status' => $progress_statuses[rand(0,3)],
        'report_lang' => (['ar','en'])[rand(0,1)],
        'status' => $statuses[rand(0,4)],
        'ref_no' => bs_assignment_ref_key(),
        'draft_issued_at' => rand(0,1) ? now() : null,
        'final_issued_at' => rand(0,1) ? now() : null,
        'contract_executed_at' => rand(0,1) ? now() : null,
        'user_id' => \App\Laravue\Models\User::all()->random(1)->first()->id,
        'service' => $services[rand(0,4)],
        'price' => rand(3000,10000),
        'target_date' => now()->addDays(rand(-10,30)),
        'payment_percentage' => rand(0,100)
    ];
});
