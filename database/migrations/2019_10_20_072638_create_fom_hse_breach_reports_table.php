<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomHseBreachReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_hse_breach_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contractor_name')->nullable();
            $table->date('date')->nullable();
            $table->string('initiated_by')->nullable();
            $table->string('ls_project_manager')->nullable();
            $table->string('document_reference')->nullable();
            $table->text('requirement')->nullable();
            $table->text('observation')->nullable();
            $table->text('dispositions')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_hse_breach_reports');
    }
}
