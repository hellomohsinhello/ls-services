<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomTaskGroupSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_task_group_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->boolean('incoming')->nullable();
            $table->boolean('receiver_contractor')->nullable();
            $table->boolean('receiver_owner')->nullable();
            $table->boolean('receiver_tenant')->nullable();
            $table->boolean('receiver_fom_manager')->nullable();
            $table->boolean('sender_contractor')->nullable();
            $table->boolean('sender_owner')->nullable();
            $table->boolean('sender_tenant')->nullable();
            $table->boolean('sender_fom_manager')->nullable();
            $table->boolean('oa')->nullable();
            $table->boolean('building_management')->nullable();
            $table->unsignedInteger('timescale')->nullable();
            $table->integer('step')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_task_group_settings');
    }
}
