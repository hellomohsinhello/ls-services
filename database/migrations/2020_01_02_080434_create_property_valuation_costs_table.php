<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuationCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_valuation_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->float('area_length')->nullable();
            $table->float('rate_area')->nullable();
            $table->float('age')->nullable();
            $table->float('life_expectancy')->nullable();
            $table->boolean('apply_profit')->nullable();
            $table->string('type')->nullable();
            $table->unsignedInteger('property_id');
            $table->unsignedInteger('total_cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_valuation_costs');
    }
}
