<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable();
            $table->string('source')->nullable();
            $table->string('client_name')->nullable();
            $table->string('community')->nullable();
            $table->string('floor_breakdown')->nullable();
            $table->date('rfp_received')->nullable();
            $table->date('proposal_sent')->nullable();
            $table->string('ls_ref')->nullable();
//            $table->string('project_name')->nullable();
            $table->string('request_service')->nullable();
            $table->string('property_name')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('mobile')->nullable();
            $table->string('telephone')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
