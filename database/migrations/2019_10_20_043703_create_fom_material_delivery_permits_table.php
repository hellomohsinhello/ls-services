<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomMaterialDeliveryPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_material_delivery_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company')->nullable();
            $table->string('ls_permit_no')->nullable();
            $table->string('address')->nullable();
            $table->string('c_primary_contact_name')->nullable();
            $table->string('c_title')->nullable();
            $table->string('c_city')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_fax_no')->nullable();
            $table->string('c_phone_no')->nullable();
            $table->string('c_email')->nullable();
            $table->string('v_make_model')->nullable();
            $table->string('v_vehicle_height')->nullable();
            $table->string('v_vehicle_registration')->nullable();
            $table->string('v_vehicle_weight')->nullable();
            $table->string('v_drivers_name')->nullable();
            $table->string('v_drivers_license_number')->nullable();
            $table->string('v_insurance_validity_period')->nullable();
            $table->string('v_license_plate_number')->nullable();
            $table->date('v_delivery_date')->nullable();
            $table->string('v_unit')->nullable();
            $table->string('v_description_of_material')->nullable();
            $table->text('v_delivery_days')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_material_delivery_permits');
    }
}
