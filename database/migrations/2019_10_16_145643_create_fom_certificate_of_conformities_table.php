<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomCertificateOfConformitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_certificate_of_conformities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_of_submission')->nullable();
            $table->string('name_of_tenant')->nullable();
            $table->string('tower')->nullable();
            $table->string('file_no')->nullable();
            $table->string('t_contact_person')->nullable();
            $table->string('t_office_tel_no')->nullable();
            $table->string('t_title_position')->nullable();
            $table->string('t_mobile_no')->nullable();
            $table->string('t_address')->nullable();
            $table->string('t_email')->nullable();
            $table->string('c_title_position')->nullable();
            $table->string('c_mobile_no')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_email')->nullable();
            $table->string('f_contact_person')->nullable();
            $table->string('f_office_tel_no')->nullable();
            $table->string('f_title_position')->nullable();
            $table->string('f_mobile_no')->nullable();
            $table->string('question_1')->nullable();
            $table->string('question_2')->nullable();
            $table->string('question_3')->nullable();
            $table->string('question_4')->nullable();
            $table->string('question_5')->nullable();
            $table->string('question_6')->nullable();
            $table->string('question_7')->nullable();
            $table->string('question_8')->nullable();
            $table->string('question_9')->nullable();
            $table->string('question_10')->nullable();
            $table->string('question_11')->nullable();
            $table->string('question_12')->nullable();
            $table->string('question_13')->nullable();
            $table->string('question_14')->nullable();
            $table->string('question_15')->nullable();
            $table->string('question_16')->nullable();
            $table->string('question_17')->nullable();
            $table->string('question_18')->nullable();
            $table->string('question_19')->nullable();
            $table->string('question_20')->nullable();
            $table->string('question_21')->nullable();
            $table->string('question_22')->nullable();
            $table->string('question_23')->nullable();
            $table->string('question_24')->nullable();
            $table->string('question_25')->nullable();
            $table->string('question_26')->nullable();
            $table->string('question_27')->nullable();
            $table->string('question_28')->nullable();
            $table->string('question_29')->nullable();
            $table->string('question_30')->nullable();
            $table->string('a_contracotrs')->nullable();
            $table->string('a_titleposition')->nullable();
            $table->string('cc_name')->nullable();
            $table->string('cc_title_position')->nullable();
            $table->mediumText('cc_additional_comments')->nullable();
            $table->string('cc_name_of_inspector')->nullable();
            $table->date('cc_inspection_date')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_certificate_of_conformities');
    }
}
