<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomMepServicesPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_mep_services_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location_and_details')->nullable();
            $table->string('contractor_name')->nullable();
            $table->string('contractor_contact_no')->nullable();
            $table->string('site_supervisor_name')->nullable();
            $table->string('site_supervisor_contact_no')->nullable();
            $table->date('issuance_date')->nullable();
            $table->date('validity_date')->nullable();
            $table->boolean('witnessing_request_1')->nullable();
            $table->boolean('witnessing_request_2')->nullable();
            $table->boolean('witnessing_request_3')->nullable();
            $table->boolean('witnessing_request_4')->nullable();
            $table->boolean('witnessing_request_5')->nullable();
            $table->boolean('witnessing_request_6')->nullable();
            $table->boolean('witnessing_request_7')->nullable();
            $table->boolean('witnessing_request_8')->nullable();
            $table->boolean('witnessing_request_9')->nullable();
            $table->boolean('building_systems_request_1')->nullable();
            $table->boolean('building_systems_request_2')->nullable();
            $table->boolean('building_systems_request_3')->nullable();
            $table->boolean('building_systems_request_4')->nullable();
            $table->boolean('building_systems_request_5')->nullable();
            $table->boolean('building_systems_request_6')->nullable();
            $table->boolean('building_systems_request_7')->nullable();
            $table->boolean('building_systems_request_8')->nullable();
            $table->boolean('building_systems_request_9')->nullable();
            $table->boolean('building_systems_request_10')->nullable();
            $table->boolean('building_systems_request_11')->nullable();
            $table->boolean('building_systems_request_13')->nullable();
            $table->boolean('building_systems_request_14')->nullable();
            $table->mediumText('bm_ls_comments')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_mep_services_permits');
    }
}
