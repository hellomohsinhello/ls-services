<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalculationMethodPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calculation_method_property', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('property_id')->nullable();
            $table->unsignedInteger('calculation_method_id')->nullable();
            $table->text('calculations')->nullable();
            $table->boolean('is_primary_market_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calculation_method_property');
    }
}
