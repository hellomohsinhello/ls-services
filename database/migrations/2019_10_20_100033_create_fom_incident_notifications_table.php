<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomIncidentNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_incident_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('related')->nullable();
            $table->string('if_not_why')->nullable();
            $table->string('near_miss')->nullable();
            $table->string('affected')->nullable();
            $table->text('near_misses')->nullable();
            $table->date('date_of_occurrence')->nullable();
            $table->string('accident_description')->nullable();
            $table->string('action')->nullable();
            $table->string('reporting_party_details')->nullable();
            $table->string('notification_to_company')->nullable();
            $table->string('government_authorities')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_incident_notifications');
    }
}
