<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomDisciplinaryActionIssuedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_disciplinary_action_issueds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('warning_type')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('designation')->nullable();
            $table->date('date_of_warning')->nullable();
            $table->string('id_number')->nullable();
            $table->text('significant_comments')->nullable();
            $table->text('supervisors_name_and_title')->nullable();
            $table->text('warned_for')->nullable();
            $table->text('employees_comments')->nullable();
            $table->timestamp('violation_date_time')->nullable();
            $table->string('warning_no')->nullable();
            $table->string('company')->nullable();
            $table->string('location')->nullable();
            $table->text('counseling_comments')->nullable();
            $table->text('violated')->nullable();
            $table->text('additional_action')->nullable();
            $table->text('restate')->nullable();
            $table->string('warned_before')->nullable();
            $table->string('warned_before_type')->nullable();
            $table->string('warned_before_by_whom')->nullable();
            $table->text('removed_for')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_disciplinary_action_issueds');
    }
}
