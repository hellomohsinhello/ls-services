<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('assignment_id')->nullable();
            $table->boolean('hide_valuation')->nullable();
            $table->string('template_type')->nullable();
            $table->string('report_title')->nullable();
            $table->string('applicant_name')->nullable();
            $table->text('introduction')->nullable();
            $table->text('brief_description')->nullable();
            $table->text('address')->nullable();
            $table->text('tenure_status')->nullable();
            $table->text('purpose')->nullable();
            $table->date('valuation_date')->nullable();
            $table->text('final_notes')->nullable();
            $table->text('instruction')->nullable();
            $table->text('methodology')->nullable();
            $table->text('valuation_basis')->nullable();
            $table->boolean('forced_sale')->nullable();
            $table->boolean('is_page_signature')->nullable();
            $table->text('valuation_approach')->nullable();
            $table->text('valuation_calculation_summary')->nullable();
            $table->text('principal_risks')->nullable();
            $table->text('general_market_commentary')->nullable();
            $table->text('special_assumptions_appendices_b')->nullable();
            $table->text('general_market_commentary_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
