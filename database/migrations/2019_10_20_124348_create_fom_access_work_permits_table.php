<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomAccessWorkPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_access_work_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contractor_name')->nullable();
            $table->string('contractor_contact_no')->nullable();
            $table->string('site_supervisor_name')->nullable();
            $table->string('site_supervisor_contact_no')->nullable();
            $table->date('issuance_date')->nullable();
            $table->date('validity_date')->nullable();
            $table->string('pre_work_condition')->nullable();
            $table->string('post_work_condition')->nullable();
            $table->text('areas')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_access_work_permits');
    }
}
