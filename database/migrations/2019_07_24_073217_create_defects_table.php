<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('defects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('inspection_type_id')->nullable();
            $table->integer('number')->nullable();
            $table->mediumText('location')->nullable();
            $table->unsignedInteger('element_id')->nullable();
            $table->unsignedInteger('property_id')->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('recommendation')->nullable();
            $table->string('liable_parties')->nullable();
            $table->mediumText('component')->nullable();
            $table->mediumText('poac')->nullable();
            $table->mediumText('rec')->nullable();
            $table->json('rating')->nullable();
            $table->string('status')->nullable();
            $table->string('unit')->nullable();
            $table->string('priority')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('rate')->nullable();
            $table->string('audio')->nullable();
            $table->string('nrm_code')->nullable();
            $table->mediumText('conditions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('defects');
    }
}
