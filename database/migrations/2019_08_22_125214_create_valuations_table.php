<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valuations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('valuation_date')->nullable();
            $table->string('interest')->nullable();
            $table->string('purpose')->nullable();
            $table->unsignedTinyInteger('round_by')->nullable();
            $table->unsignedInteger('primary_market_value')->nullable();
            $table->unsignedTinyInteger('forced_sale_value')->nullable();
            $table->text('valuation_ration')->nullable();
            $table->text('market_research')->nullable();
            $table->text('tenure_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valuations');
    }
}
