<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuationInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('property_valuation_inspections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('inspection_date_time')->nullable();
            $table->string('inspection_tel')->nullable();
            $table->string('gps')->nullable();
            $table->mediumText('address_old')->nullable();
            $table->mediumText('address_new')->nullable();
            $table->string('location')->nullable();
            $table->string('parking')->nullable();
            $table->string('road_surface')->nullable();
            $table->string('boundaries')->nullable();
            $table->string('leveling')->nullable();
            $table->string('age_years')->nullable();
            $table->string('no_of_units')->nullable();
            $table->string('property_status')->nullable();
            $table->string('life_years')->nullable();
            $table->string('property_condition')->nullable();
            $table->string('lift')->nullable();
            $table->string('other_details')->nullable();
            $table->string('development')->nullable();
            $table->string('unit_type')->nullable();
            $table->string('occupancy')->nullable();
            $table->string('boundary_walls')->nullable();
            $table->string('cladding')->nullable();
            $table->string('doors')->nullable();
            $table->string('yard_coverings')->nullable();
            $table->string('windows')->nullable();
            $table->string('facilities')->nullable();
            $table->string('structures')->nullable();
            $table->longText('internally')->nullable();
            $table->string('finishing')->nullable();
            $table->mediumText('air_conditioning')->nullable();
            $table->mediumText('view')->nullable();
            $table->unsignedInteger('property_id');
            $table->text('other_1')->nullable()->comment('Specification including Construction type');
            $table->text('other_2')->nullable()->comment('Additional Features/Alterations (Include impact on Market value)');
            $table->text('other_3')->nullable()->comment('Main services');
            $table->text('other_4')->nullable()->comment('Nature and source of the information relied upon');
            $table->text('other_5')->nullable()->comment('Extent of investigation');
            $table->text('other_6')->nullable()->comment('Previous Involvement Declaration');
            $table->text('other_7')->nullable()->comment('Surveyor Declaration');
            $table->text('other_8')->nullable()->comment('Micro Location');
            $table->text('other_9')->nullable()->comment('Access & Surroundings');
            $table->text('other_10')->nullable()->comment('Access & Surroundings');
            $table->text('surroundings')->nullable();
            $table->boolean('water_marked_images')->nullable();
            $table->string('include_photos')->nullable();
            $table->string('photos_page_title')->nullable();
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_valuation_inspections');
    }
}
