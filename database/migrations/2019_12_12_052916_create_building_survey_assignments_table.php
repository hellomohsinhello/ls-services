<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingSurveyAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_survey_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->date('due_date')->nullable();
            $table->unsignedInteger('manager_id')->nullable();
            $table->unsignedInteger('client_contact_id')->nullable();
            $table->string('client_ref')->nullable();
            $table->string('ref_no')->nullable();
            $table->string('report_lang')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_received')->nullable();
            $table->string('progress_status')->nullable();
            $table->dateTime('draft_issued_at')->nullable();
            $table->dateTime('final_issued_at')->nullable();
            $table->dateTime('contract_executed_at')->nullable();
            $table->string('service')->nullable();
            $table->unsignedInteger('price')->nullable();
            $table->date('target_date')->nullable();
            $table->unsignedInteger('payment_percentage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_survey_assignments');
    }
}
