<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomPermitToWorkFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_permit_to_work_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('sub_contractors')->nullable();
            $table->text('approvals')->nullable();
            $table->string('work_permit_number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('site_supervisor_name')->nullable();
            $table->string('site_supervisor_contact_number')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('type_of_application')->nullable();
            $table->text('design_approvals')->nullable();
            $table->mediumText('electricity_comment')->nullable();
            $table->mediumText('noisy_non_noisy_works_comment')->nullable();
            $table->mediumText('insurance_comment')->nullable();
            $table->unsignedBigInteger('task_id')->nullable();
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_permit_to_work_forms');
    }
}
