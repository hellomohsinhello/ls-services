<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maps_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('locationable_id');
            $table->string('locationable_type');
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('position_lat')->nullable();
            $table->string('position_lng')->nullable();
            $table->integer('zoom')->nullable();
            $table->mediumText('polygon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maps_locations');
    }
}
