<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuationBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_valuation_breakdowns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('floors_units')->nullable();
            $table->string('type')->nullable();
            $table->unsignedInteger('no_of_units')->nullable();
            $table->float('area')->nullable();
            $table->float('rent_unit_or_rent_area')->nullable();
            $table->string('operational_costs')->nullable();
            $table->float('deduction')->nullable();
            $table->float('total_deduction')->nullable();
            $table->string('description')->nullable();
            $table->float('value')->nullable();
            $table->string('breakdown_type')->nullable();
            $table->unsignedInteger('property_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_valuation_breakdowns');
    }
}
