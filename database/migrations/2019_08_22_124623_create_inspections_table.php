<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('property_id');
            $table->dateTime('inspection_date')->nullable();
            $table->string('inspection_tel')->nullable();
            $table->string('property_type')->nullable();
            $table->string('inspection_name')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('gps')->nullable();
            $table->string('address_new')->nullable();
            $table->string('address_old')->nullable();
            $table->text('internally')->nullable();
            $table->text('location_map')->nullable();
            $table->text('surroundings')->nullable();
            $table->string('location')->nullable();
            $table->string('road_surface')->nullable();
            $table->string('leveling')->nullable();
            $table->string('parking')->nullable();
            $table->string('boundaries')->nullable();
            $table->string('property_age')->nullable();
            $table->string('property_life')->nullable();
            $table->string('other_details')->nullable();
            $table->string('occupancy')->nullable();
            $table->string('no_of_units')->nullable();
            $table->string('property_condition')->nullable();
            $table->string('development')->nullable();
            $table->string('unit_type')->nullable();
            $table->string('property_status')->nullable();
            $table->string('lift')->nullable();
            $table->string('boundary_wall')->nullable();
            $table->string('yard_covering')->nullable();
            $table->string('structure')->nullable();
            $table->string('cladding')->nullable();
            $table->string('windows')->nullable();
            $table->string('doors')->nullable();
            $table->string('facilities')->nullable();
            $table->string('finishing')->nullable();
            $table->string('air-conditioning')->nullable();
            $table->string('view')->nullable();
            $table->text('specification')->nullable();
            $table->text('additional_features')->nullable();
            $table->text('main_services')->nullable();
            $table->text('nature')->nullable();
            $table->text('extent_of_investigation')->nullable();
            $table->text('previous_involvement_declaration')->nullable();
            $table->text('surveyor_declaration')->nullable();
            $table->text('micro_location')->nullable();
            $table->text('access_surroundings')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspections');
    }
}
