<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomHotWorkPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_hot_work_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name')->nullable();
            $table->string('project')->nullable();
            $table->string('location')->nullable();
            $table->string('permit_receiver')->nullable();
            $table->string('contact_mobile_number')->nullable();
            $table->string('permit_holder')->nullable();
            $table->string('name_of_operative')->nullable();
            $table->string('employee_number')->nullable();
            $table->timestamp('date_and_time')->nullable();
            $table->timestamp('date_and_time_end')->nullable();
            $table->timestamp('s2_date_and_time')->nullable();
            $table->timestamp('s2_date_and_time_end')->nullable();
            $table->string('q1')->nullable();
            $table->string('q2')->nullable();
            $table->string('q3')->nullable();
            $table->string('q4')->nullable();
            $table->string('q5')->nullable();
            $table->string('q6')->nullable();
            $table->string('q7')->nullable();
            $table->string('q8')->nullable();
            $table->string('q9')->nullable();
            $table->string('q10')->nullable();
            $table->string('q11')->nullable();
            $table->string('q12')->nullable();
            $table->string('q13')->nullable();
            $table->string('q14')->nullable();
            $table->string('q15')->nullable();
            $table->string('q16')->nullable();
            $table->string('q17')->nullable();
            $table->string('q18')->nullable();
            $table->string('q19')->nullable();
            $table->string('q20')->nullable();
            $table->mediumText('disposalmetods')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_hot_work_permits');
    }
}
