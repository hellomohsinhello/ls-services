<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomExitPassPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_exit_pass_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('b_date_of_exit')->nullable();
            $table->string('co_company')->nullable();
            $table->string('co_address')->nullable();
            $table->string('c_primary_contact_name')->nullable();
            $table->string('c_title')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_city')->nullable();
            $table->string('c_contact_number')->nullable();
            $table->string('c_email')->nullable();
            $table->longText('materials')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_exit_pass_permits');
    }
}
