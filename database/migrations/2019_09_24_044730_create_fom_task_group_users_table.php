<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomTaskGroupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_task_group_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('task_group_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('project_id')->nullable();
            $table->string('type')->nullable();
            $table->boolean('downloaded')->nullable();
            $table->timestamps();

//            $table->unique(['task_group_id','user_id','project_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_task_group_users');
    }
}
