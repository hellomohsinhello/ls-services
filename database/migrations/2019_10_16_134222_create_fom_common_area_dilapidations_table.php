<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomCommonAreaDilapidationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_common_area_dilapidations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_manager')->nullable();
            $table->date('inspection_date')->nullable();
            $table->date('report_issue_date')->nullable();
            $table->longText('items')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_common_area_dilapidations');
    }
}
