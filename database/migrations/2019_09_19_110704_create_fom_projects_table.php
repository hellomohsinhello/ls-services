<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('fom_projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('units')->nullable();
            $table->unsignedInteger('owner_id')->nullable();
            $table->unsignedInteger('tenant_id')->nullable();
            $table->unsignedInteger('contractor_id')->nullable();
            $table->unsignedInteger('building_id')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->string('oa_email')->nullable();
            $table->string('building_management_email')->nullable();
            $table->double('area')->nullable();
            $table->unsignedInteger('levels')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_projects');
    }
}
