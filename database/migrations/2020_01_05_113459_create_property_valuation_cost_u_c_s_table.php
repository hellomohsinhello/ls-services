<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuationCostUCSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_valuation_cost_u_c_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->float('area_length')->nullable();
            $table->float('complete_percent')->nullable();
            $table->float('rate_area')->nullable();
            $table->string('cost_as_is')->nullable();
            $table->string('cost_as_completed')->nullable();
            $table->boolean('apply_profit')->nullable();
            $table->string('type')->nullable();
            $table->unsignedInteger('property_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_valuation_cost_u_c_s');
    }
}
