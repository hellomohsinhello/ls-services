<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomFirstFixInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_first_fix_inspections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('t_name_of_tenant')->nullable();
            $table->string('t_shop_unit_no')->nullable();
            $table->string('t_doc_ref_no')->nullable();
            $table->string('t_file_no')->nullable();
            $table->string('t_unit')->nullable();
            $table->string('c_contact_person')->nullable();
            $table->string('c_office_tel_no')->nullable();
            $table->string('c_title_position')->nullable();
            $table->string('c_mobile_no')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_email')->nullable();
            $table->string('f_contact_person')->nullable();
            $table->string('f_office_tel_no')->nullable();
            $table->string('f_title_position')->nullable();
            $table->string('f_mobile_no')->nullable();
            $table->string('question_1')->nullable();
            $table->string('question_1_remarks')->nullable();
            $table->string('question_2')->nullable();
            $table->string('question_2_remarks')->nullable();
            $table->string('question_3')->nullable();
            $table->string('question_3_remarks')->nullable();
            $table->string('question_4')->nullable();
            $table->string('question_4_remarks')->nullable();
            $table->string('question_5')->nullable();
            $table->string('question_5_remarks')->nullable();
            $table->string('question_6')->nullable();
            $table->string('question_6_remarks')->nullable();
            $table->string('question_7')->nullable();
            $table->string('question_7_remarks')->nullable();
            $table->string('question_8')->nullable();
            $table->string('question_8_remarks')->nullable();
            $table->string('question_9')->nullable();
            $table->string('question_9_remarks')->nullable();
            $table->string('question_10')->nullable();
            $table->string('question_10_remarks')->nullable();
            $table->string('question_11')->nullable();
            $table->string('question_11_remarks')->nullable();
            $table->string('question_12')->nullable();
            $table->string('question_12_remarks')->nullable();
            $table->string('question_13')->nullable();
            $table->string('question_13_remarks')->nullable();
            $table->string('question_14')->nullable();
            $table->string('question_14_remarks')->nullable();
            $table->string('question_15')->nullable();
            $table->string('question_15_remarks')->nullable();
            $table->string('question_16')->nullable();
            $table->string('question_16_remarks')->nullable();
            $table->string('question_17')->nullable();
            $table->string('question_17_remarks')->nullable();
            $table->string('question_18')->nullable();
            $table->string('question_18_remarks')->nullable();
            $table->string('question_19')->nullable();
            $table->string('question_19_remarks')->nullable();
            $table->string('question_20')->nullable();
            $table->string('question_20_remarks')->nullable();
            $table->string('question_21')->nullable();
            $table->string('question_21_remarks')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_first_fix_inspections');
    }
}
