<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingSurveyPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('building_survey_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            //building survey || Common
            $table->unsignedInteger('template_id')->nullable();
            $table->unsignedInteger('area_id');
            $table->unsignedInteger('surveyor_id')->nullable();
            $table->string('name');
            $table->string('plot_number');
            $table->string('property_number');
            $table->string('property_type')->nullable();
            $table->unsignedInteger('assignment_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('building_survey_properties');
    }
}
