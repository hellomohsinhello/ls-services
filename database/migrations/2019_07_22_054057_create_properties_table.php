<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            //building survey || Common
            $table->unsignedInteger('template_id')->nullable();
            $table->unsignedInteger('area_id')->nullable();
            $table->unsignedInteger('valuer_id')->nullable();
            $table->string('name');
            $table->string('plot_number');
            $table->string('property_number');
            $table->unsignedInteger('inspection_type_id')->nullable();
            $table->unsignedInteger('property_type_id')->nullable();
            $table->unsignedInteger('assignment_id')->nullable();
            $table->unsignedInteger('inspector_id')->nullable();
            $table->date('instruction_date')->nullable();
            $table->string('current_owner')->nullable();
            $table->string('applicant_name')->nullable();
            $table->integer('bua')->nullable();
            $table->string('bua_unit')->nullable();
            $table->integer('area')->nullable();
            $table->string('area_unit')->nullable();
            $table->unsignedInteger('client_contact_id')->nullable();
            $table->timestamp('inspection_datetime')->nullable();
            $table->string('ref_number')->nullable();


            $table->date('valuation_date')->nullable();
            $table->mediumText('purpose')->nullable();
            $table->mediumText('basis')->nullable();
            $table->mediumText('interests')->nullable();
            $table->string('tenure_status')->nullable();
            $table->string('developer')->nullable();
            $table->mediumText('methods')->nullable();
            $table->integer('round_by')->nullable();
            $table->integer('forced_sale_value')->nullable();
            $table->string('primary_market_value')->nullable();
            $table->boolean('fs_instead_sa')->nullable();
            $table->text('valuation_rationale_1')->nullable();
            $table->text('market_research')->nullable();
            $table->boolean('detailed_method')->nullable();
            $table->string('adj_name_1')->nullable();
            $table->string('adj_name_2')->nullable();
            $table->string('adj_name_3')->nullable();
            $table->string('area_for_calculation')->nullable();
            $table->boolean('use_decimal_for_rate_area')->nullable();
            $table->float('cost_method_dev_profit')->nullable();
            $table->float('cost_method_uc_dev_profit_as_is')->nullable();
            $table->float('cost_method_uc_dev_profit_as_comp')->nullable();
            $table->float('income_method_capitalisation_rate')->nullable();
            $table->text('valuation_rationale_2')->nullable();
            $table->string('primary_market_value_sub_option')->nullable();
            $table->string('marketing_period')->nullable();
            $table->text('assumptions')->nullable();
            $table->text('special_assumptions')->nullable();
            $table->boolean('report_cosigned_by_valuer')->nullable();
            $table->boolean('approved_municipality_plans')->nullable();
            $table->text('market_commentary')->nullable();
            $table->string('appendix_title')->nullable();
            $table->string('client_contact_phone')->nullable();
            $table->unsignedInteger('cover_page_photo_index')->nullable();
            $table->text('appendix_content')->nullable();


            $table->unsignedBigInteger('price_per_square_foot')->nullable();
            $table->unsignedBigInteger('total_construction_cost')->nullable();
            $table->unsignedBigInteger('existing_market_value')->nullable();
            $table->unsignedBigInteger('estimated_market_value')->nullable();
            $table->string('passing_rent')->nullable();
            $table->text('other_remarks')->nullable();

            $table->dateTime('inspection_date_time')->nullable();
            $table->string('inspection_tel')->nullable();
            $table->string('gps')->nullable();
            $table->mediumText('address_old')->nullable();
            $table->mediumText('address_new')->nullable();
            $table->string('location')->nullable();
            $table->string('parking')->nullable();
            $table->string('road_surface')->nullable();
            $table->string('boundaries')->nullable();
            $table->string('leveling')->nullable();
            $table->string('age_years')->nullable();
            $table->string('no_of_units')->nullable();
            $table->string('property_status')->nullable();
            $table->string('property_use')->nullable();
            $table->string('life_years')->nullable();
            $table->string('property_condition')->nullable();
            $table->string('lift')->nullable();
            $table->string('other_details')->nullable();
            $table->string('development')->nullable();
            $table->string('unit_type')->nullable();
            $table->string('occupancy')->nullable();
            $table->string('boundary_walls')->nullable();
            $table->string('cladding')->nullable();
            $table->string('doors')->nullable();
            $table->string('yard_coverings')->nullable();
            $table->string('windows')->nullable();
            $table->string('facilities')->nullable();
            $table->string('structures')->nullable();
            $table->longText('internally')->nullable();
            $table->string('finishing')->nullable();
            $table->mediumText('air_conditioning')->nullable();
            $table->mediumText('view')->nullable();
            $table->text('other_1')->nullable()->comment('Specification including Construction type');
            $table->text('other_2')->nullable()->comment('Additional Features/Alterations (Include impact on Market value)');
            $table->text('other_3')->nullable()->comment('Main services');
            $table->text('other_4')->nullable()->comment('Nature and source of the information relied upon');
            $table->text('other_5')->nullable()->comment('Extent of investigation');
            $table->text('other_6')->nullable()->comment('Previous Involvement Declaration');
            $table->text('other_7')->nullable()->comment('Surveyor Declaration');
            $table->text('other_8')->nullable()->comment('Micro Location');
            $table->text('other_9')->nullable()->comment('Access & Surroundings');
            $table->text('other_10')->nullable()->comment('Property Description');
            $table->text('other_11')->nullable()->comment('Nature and source of the information relied upon');
            $table->text('other_12')->nullable()->comment('Accommodation');
            $table->text('other_13')->nullable()->comment('Access/Views');
            $table->text('other_14')->nullable()->comment('Condition of Property');
            $table->text('other_15')->nullable()->comment('Easements, Rights of Way');
            $table->text('other_16')->nullable()->comment('Quality of surrounding property');
            $table->text('other_17')->nullable()->comment('Main Services');
            $table->text('surroundings')->nullable();
            $table->boolean('water_marked_images')->nullable();
            $table->string('include_photos')->nullable();
            $table->string('photos_page_title')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
