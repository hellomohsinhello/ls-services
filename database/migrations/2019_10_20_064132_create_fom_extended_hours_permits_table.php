<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomExtendedHoursPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_extended_hours_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company');
            $table->date('date');
            $table->time('time');
            $table->time('time_end');
            $table->string('location_of_work');
            $table->string('description_of_work');
            $table->string('equipment_needed');
            $table->string('supervisor_name');
            $table->string('supervisor_contact_number');
            $table->string('safety_name');
            $table->string('safety_contact_number');
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_extended_hours_permits');
    }
}
