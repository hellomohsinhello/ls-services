<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFomWasteDisposalPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fom_waste_disposal_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('date_and_time')->nullable();
            $table->string('permit_no')->nullable();
            $table->string('co_company')->nullable();
            $table->string('co_address')->nullable();
            $table->string('c_primary_contact_name')->nullable();
            $table->string('c_title')->nullable();
            $table->string('c_address')->nullable();
            $table->string('c_city')->nullable();
            $table->string('c_contact_number')->nullable();
            $table->string('c_email')->nullable();
            $table->string('w_generated_from')->nullable();
            $table->string('w_by_whom')->nullable();
            $table->string('w_describe_material')->nullable();
            $table->string('w_quantity')->nullable();
            $table->string('w_how_stored')->nullable();
            $table->string('w_disposal_method')->nullable();
            $table->mediumText('w_disposal_location')->nullable();
            $table->mediumText('w_comments')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->string('status')->nullable();
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fom_waste_disposal_permits');
    }
}
