<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuationValuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('property_valuation_valuations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('valuation_date')->nullable();
            $table->mediumText('purpose')->nullable();
            $table->mediumText('basis')->nullable();
            $table->mediumText('interests')->nullable();
            $table->string('tenure_status')->nullable();
            $table->string('developer')->nullable();
            $table->mediumText('methods')->nullable();
            $table->unsignedInteger('property_id');
            $table->integer('round_by')->nullable();
            $table->integer('forced_sale_value')->nullable();
            $table->string('primary_market_value')->nullable();
            $table->boolean('fs_instead_sa')->nullable();
            $table->text('valuation_rationale_1')->nullable();
            ;
            $table->text('market_research')->nullable();
            $table->boolean('detailed_method')->nullable();
            $table->string('adj_name_1')->nullable();
            $table->string('adj_name_2')->nullable();
            $table->string('adj_name_3')->nullable();
            $table->string('area_for_calculation')->nullable();
            $table->boolean('use_decimal_for_rate_area')->nullable();
            $table->float('cost_method_dev_profit')->nullable();
            $table->float('cost_method_uc_dev_profit_as_is')->nullable();
            $table->float('cost_method_uc_dev_profit_as_comp')->nullable();
            $table->float('income_method_capitalisation_rate')->nullable();
            $table->text('valuation_rationale_2')->nullable();
            $table->string('primary_market_value_sub_option')->nullable();
            $table->string('marketing_period')->nullable();
            $table->text('assumptions')->nullable();
            $table->text('special_assumptions')->nullable();
            $table->boolean('report_cosigned_by_valuer')->nullable();
            $table->boolean('approved_municipality_plans')->nullable();
            $table->text('market_commentary')->nullable();
            $table->string('appendix_title')->nullable();
            $table->text('appendix_content')->nullable();

            $table->unsignedBigInteger('total_construction_cost')->nullable();
            $table->unsignedBigInteger('existing_market_value')->nullable();
            $table->unsignedBigInteger('estimated_market_value')->nullable();
            $table->string('passing_rent')->nullable();
            $table->text('other_remarks')->nullable();
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_valuation_valuations');
    }
}
