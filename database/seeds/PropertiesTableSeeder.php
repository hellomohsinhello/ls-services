<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\PropertyType::truncate();
        \App\Models\PropertyUse::truncate();
        \App\Models\Property::truncate();

        $types = [
            'Villa',
            'Apartment',
            'Land',
            'Compound',
            'Tower',
            'Warehouse',
            'Mall - Shopping Center',
            'Building',
            'Office Units',
            'Office Unit',
            'Townhouse',
            'Industrial Facility',
            'Retail Unit',
            'Labour Camp',
            'Lands + Buildings',
            'School',
            'Temporary Structures',
            'Residential Villa'
        ];

        foreach ($types as $type){
            \App\Models\PropertyType::create([
                'title' => $type
            ]);
        }


        $uses  = [
            'Residential',
            'Commercial',
            'Mixed Use',
            'Residential/Commecial',
            'Investment',
            'Agricultural',
            'Industrial',
            'Office'
        ];

        foreach ($uses as $use){
            \App\Models\PropertyUse::create([
                'title' => $use
            ]);
        }

        factory(\App\Models\Property::class,100)->create();

        $assignments = \App\Models\Assignment::all();

        foreach ($assignments as $assignment){
            update_properties_refs($assignment);
        }
    }
}
