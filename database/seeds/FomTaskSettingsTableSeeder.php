<?php

use Illuminate\Database\Seeder;

class FomTaskSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\FomTaskGroupSettings::truncate();
        \App\Models\FomTaskSetting::truncate();

        $taskGroups = [
            [
                'title' => 'UNIT OCCUPANT AND CONTRACTOR DOCUMENTS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,
                'sender_owner' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'step' => 0,
                'tasks' => [
                    [
                        'title' => 'Appointment Letter',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Owner/Tenants Passport & Emirates I.D. copies',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Title Deeds/Lease Agreement',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Contractors Trade License',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Tax Registration Number',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'FIT-OUT PACKAGE',
                'incoming' => false,
                'timescale' => 5,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,

                'step' => 0,
                'tasks' => [
                    [
                        'title' => 'Fit-out package files',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD_ONCE
                    ],
                    [
                        'title' => 'As built drawings',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'FIT OUT COMMENCEMENT DOCUMENTS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,
                'sender_owner' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'step' => 0,
                'tasks' => [
                    [
                        'title' => 'Land Sterling payment',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Damage Deposit Receipt',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Undated security cheque',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Signed and stamped Compliance Agreement',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Project Plan',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Completed Communication Schedule',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Tenants Trade License',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Contractors Passport & Emirates I.D. copies',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Owner’s PP copy',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'NOC from the Owner/Tenant to the Contractor for works to proceed',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],

                ]
            ],
            [
                'title' => 'DETAILED DESIGN SUBMISSION',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 1,
                'tasks' => [
                    [
                        'title' => 'Drawing package (soft copy) as per list provided in kick off meeting including drawing schedule and cover page.',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'DETAILED DESIGN REVIEW REPORT',
                'incoming' => false,
                'timescale' => 5,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,

                'step' => 1,
                'tasks' => [
                    [
                        'title' => 'Soft copy response with stamp and signature',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'NOC REQUEST DOCUMENTS',
                'incoming' => true, //incoming
                'timescale' => 5,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 1,
                'tasks' => [
                    [
                        'title' => 'Copy of request of NOC’s from the Owner/Tenant to Land Sterling',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Completed checklist',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of SPA (1st page) and/or Title Deed',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of Unit Owner’s Passport copy',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of authorisation letter from Unit Owner',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of Contractors Insurance',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of Owners Trade License (if applicable)',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of Tenancy Contract (if Leased)',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of Drawing Package',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copy of request of NOC from the Owner/Tenant to Land Sterling',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Damage Deposit Receipt',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'NOC CONFIRMATION',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_oa' => true,
                'receiver_building_management' => true,


                'step' => 1,
                'tasks' => [
                    [
                        'title' => 'Issue confirmation e-mail alert',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'NOC SUBMISSION',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_oa' => true,
                'receiver_building_management' => true,

                'step' => 1,
                'tasks' => [
                    [
                        'title' => 'NOC Documents',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'NOC ISSUANCE',
                'incoming' => true, //incoming
                'timescale' => 10,

                'sender_oa' => true,
                'sender_developer' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,
                'receiver_fom_manager' => true,

                'step' => 2,
                'tasks' => [
                    [
                        'title' => 'DDA/DEWA NOC’s',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'AUTHORITY APPROVALS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 3,
                'tasks' => [
                    [
                        'title' => 'Authority Approvals',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'PERMIT TO WORK DOCUMENTS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'receiver_contractor' => true,

                'sender_fom_manager' => true,

                'step' => 3,
                'tasks' => [
                    [
                        'title' => 'Permit to Work Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'permit-to-work-form'
                    ],
                    [
                        'title' => 'Letter for Owner/Tenant confirming that ALL works will be executed in accordance with Local Law and Dubai Municipality requirements',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => '2,000 Temporary Power Receipt showing payment into OA’s account',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'PERMIT APPROVAL',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,
                'receiver_oa' => true,
                'receiver_building_management' => true,
                'receiver_security' => true,

                'step' => 3,
                'tasks' => [
                    [
                        'title' => 'Permit to Work Form',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'HSE INDUCTION FORM DETAILS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'H&S Induction/I.D. Card Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'h-s-induction-id-card-form'
                    ],
                ]
            ],
            [
                'title' => 'HSE INDUCTION REQUEST DOCUMENTS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'Copies of all Sub-Contractor Trade Licenses (if applicable)',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Copies of all workers Emirates I.D. & Passports inc. Visa Stamp',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Health and Safety Management Plan, Method Statement and Risk Assessments  (Project Specific)',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'I.D. Card fee to be paid on induction date or commencement of work',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'HSE INDUCTION SCHEDULER',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'Issue e-mail.',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'HSE INDUCTION COMPLETION DETAILS',
                'incoming' => false,
                'timescale' => 0,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'Attendance Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'attendance-form'
                    ],
                    [
                        'title' => 'Provide Induction Presentation',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ]
                ]
            ],
            [
                'title' => 'ID CARD PICKUP SCHEDULER',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'Receive payment from Contractor and provide a receipt',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'ID CARD RENEWAL REQUEST',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 4,
                'tasks' => [
                    [
                        'title' => 'H&S Induction/I.D. Card Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'h-s-induction-id-card-form'
                    ],
                ]
            ],
            [
                'title' => 'CERTIFICATE OF CONFORMITY',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 5,
                'tasks' => [
                    [
                        'title' => 'Common Area Dilapidation Report',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'common-area-dilapidation-report'
                    ],
                    [
                        'title' => 'Certificate of Conformity Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type'=>'certificate-of-conformity-form'
                    ],
                    [
                        'title' => 'Delivery and Access Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'delivery-and-access-permit'
                    ],
                    [
                        'title' => 'Exit Pass Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'exit-pass-permit'
                    ],
                    [
                        'title' => 'Waste Disposal Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'waste-disposal-permit'
                    ],
                ]
            ],
            [
                'title' => 'MOBILIZATION APPROVAL',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,

                'step' => 5,
                'tasks' => [
                    [
                        'title' => 'Issue Mobilization Permit for COC set up and common area protection only',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                    [
                        'title' => 'Common area protection specification',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                    [
                        'title' => 'Issue e-mail and provide security list of workers and start date',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'INSPECTION SCHEDULER',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 5,
                'tasks' => [
                    [
                        'title' => 'Common area protection as per requirement',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'common-area-protection-requirement'
                    ],
                    [
                        'title' => 'Other relevant COC requirements',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'certificate-of-conformity-form'
                    ],
                ]
            ],
            [
                'title' => 'INSPECTION APPROVAL',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,

                'step' => 5,
                'tasks' => [
                    [
                        'title' => 'Completed COC Form',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'MAJOR WORK PERMIT REQUEST (MANDATORY)',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Approved Job specific HSE Plan, Method Statement and Risk Assessments',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Inducted Employees List and documents if required ( secure from safety team with seal)',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ]
                ]
            ],
            [
                'title' => 'MATERIAL DELIVERY REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Material Delivery Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'material-delivery-permit'
                    ],
                    [
                        'title' => 'LS to send notification to building management and security of the approved permit',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'MATERIAL EXIT AND WASTE DISPOSAL REQUEST',
                'incoming' => true, //incoming
                'timescale' => 1,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Exit Pass Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'exit-pass-permit'
                    ],
                    [
                        'title' => 'Waste Disposal Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'waste-disposal-permit'
                    ],
                ]
            ],
            [
                'title' => 'HOT WORKS PERMIT REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Hot work permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'hot-work-permit'
                    ],
                ]
            ],
            [
                'title' => 'NOISY/EXTENDED WORKS REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Send notice to LS for the intended noisy works',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                    [
                        'title' => 'Extended Hours Work Permit  (if required)',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'extended-hours-permit'
                    ],
                ]
            ],
            [
                'title' => 'SAFETY INSPECTION REQUEST',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Compliance Inspection',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'compliance-inspection'
                    ],
                    [
                        'title' => 'Send email communication to Contractor for action and rectification of the comments',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'BREACH REPORT AND DISCIPLINARY ACTION',
                'incoming' => false,
                'timescale' => 2,

                'sender_contractor' => true,
                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'HSE Breach Report',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'hse-breach-report'
                    ],
                   /* [
                        'title' => 'Disposition and Closure Report',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'hse-breach-report'
                    ],*/
                    [
                        'title' => 'Contractor to revert back with its action taken on Disciplinary Action Issued ',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'disciplinary-action-issued'
                    ],
                ]
            ],
            [
                'title' => 'Penalties imposed by H & S of LS per Compliance Agreement requirement or parameters ',
                'incoming' => true, //incoming
                'timescale' => 3,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Receipt showing payment directly to OA’s account ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'Incident notification issued including near misses',
                'incoming' => false,
                'timescale' => 2,

                'sender_contractor' => true,
                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_fom_manager' => true,
                'receiver_oa' => true,
                'receiver_building_management' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Incident Notification',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'incident-notification'
                    ],
                ]
            ],
            [
                'title' => 'TESTING AND COMMISSIONING REQUEST (MANDATORY)',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Certificate of Calibration',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'MEP Services Witnessing',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Pressure Test Report for Water Line',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Pressure Test Report for Sprinkler Line',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Pressure Test Report for chilled water line',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Laboratory Analysis Report for chilled water',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'SERVICE CONNECTION REQUEST (MANDATORY)',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Submit Calibration Certificate',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'MEP Services Witnessing',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'mep-services-witnessing'
                    ],
                ]
            ],
            [
                'title' => 'WORK PERMIT RENEWAL REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Access work permit form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'access-work-permit-form'
                    ],
                    [
                        'title' => 'Submit new work program.',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD,
                        'form_type' => 'submit-new-work-program'
                    ],
                    [
                        'title' => '2,000 Temporary Power Receipt showing payment into OA’s account every month or part thereof.',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD,
                        'form_type' => 'temporary-power-receipt-showing-payment'
                    ],
                ]
            ],
            [
                'title' => 'ID CARD RENEWAL REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Payment of access card  to LS site team',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'LS to issue a receipt',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'REQUEST FOR EXTENDED WORK HOURS',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Extended hours permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'extended-hours-permit'
                    ],
                    [
                        'title' => 'Payment to LS to be made prior to implementation of extended hours',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD,
                        'form_type' => 'payment-of-extended-hours'
                    ],
                ]
            ],
            [
                'title' => 'CEILING CLOSURE REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'MEP Services Witnessing – Testing and Commissioning',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'mep-services-witnessing'
                    ],
                    [
                        'title' => 'Take photos prior to closing',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Provide all certification relevant for the item ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'First fix inspection checklist',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'first-fix-inspection-checklist'
                    ],
                ]
            ],
            [
                'title' => 'UTILITY ACCESS REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Access Work Permit Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'access-work-permit-form'
                    ],
                ]
            ],
            [
                'title' => 'VEHICLE ACCESS/PARKING REQUEST',
                'incoming' => true, //incoming
                'timescale' => 1,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'vehicle parking registration and permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'vehicle-parking-registration-and-permit'
                    ],
                ]
            ],
            [
                'title' => 'OWNERS MATERIAL DELIVERY REQUEST',
                'incoming' => true, //incoming
                'timescale' => 1,

                'sender_contractor' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,
                'receiver_oa' => true,
                'receiver_building_management' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'After receipt of Completion Certification ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Complete  Clarity Material Deliveries Permit ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'SNAG WORK REQUEST',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Snag Work Permit',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'permit-to-work-form'
                    ],
                    [
                        'title' => 'Valid Access ID',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'REQUEST FOR INFORMATION',
                'incoming' => true, //incoming
                'timescale' => 5,

                'receiver_contractor' => true,

                'sender_fom_manager' => true,

                'step' => 6,
                'tasks' => [
                    [
                        'title' => 'Submit RFI to Land Sterling ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'COMPLETION & HANDOVER REQUEST',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Exhibit 5 required documentation',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'Arrange for LS final inspection.',
                'incoming' => true, //incoming
                'timescale' => 2,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Exhibit 5 documents complete and available at the time of inspection in both hard and soft copy',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Valid Permit to work ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Valid Insurance',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Confirmation by building nominated contractors that fire fighting ZCV is opened and fire alarm interface complete',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'All Temporary power payments are up to date',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'COMPLETION CERTIFICATE APPROVAL',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_contractor' => true,
                'receiver_tenant' => true,
                'receiver_oa' => true,
                'receiver_building_management' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Completion Certificate',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Moving In Form',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'ISSUE HANDOVER FILE TO OA INCL. COMPLETION CERTIFICATE',
                'incoming' => false,
                'timescale' => 2,

                'sender_fom_manager' => true,

                'receiver_oa' => true,
                'receiver_building_management' => true,

                'oa' => true,
                'building_management' => true,
                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Handover file ',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Completion Certificate',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'SNAGGING WORKS',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,

                'receiver_fom_manager' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Permit to Work Form',
                        'type' => \App\Models\FomTask::TYPE_FILL_UP_FORM,
                        'form_type' => 'permit-to-work-form'
                    ],
                ]
            ],
            [
                'title' => 'Unit occupation',
                'incoming' => true, //incoming
                'timescale' => 0,

                'sender_contractor' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Submit moving in form to OA',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
            [
                'title' => 'SECURITY DEPOSIT REFUND REQUEST',
                'incoming' => true,  //incoming
                'timescale' => 0,

                'sender_contractor' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Request refund by email to Land Sterling',
                        'type' => \App\Models\FomTask::TYPE_NOTIFICATION,
                        'notification_message' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi aperiam deserunt doloremque ea eos esse excepturi exercitationem, explicabo iusto molestias nihil perspiciatis placeat quibusdam, repellat saepe similique sit suscipit vel.'
                    ],
                ]
            ],
            [
                'title' => 'Request for refund of security deposit from OA',
                'incoming' => false,
                'timescale' => 20,

                'sender_contractor' => true,
                'sender_tenant' => true,

                'receiver_fom_manager' => true,

                'oa' => true,
                'building_management' => true,
                'step' => 7,
                'tasks' => [
                    [
                        'title' => 'Signed rectification / damage report',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                    [
                        'title' => 'Completed Refund Form',
                        'type' => \App\Models\FomTask::TYPE_FILE_UPLOAD
                    ],
                ]
            ],
        ];

        foreach ($taskGroups as $group){

            $groupSetting = \App\Models\FomTaskGroupSettings::create([
                'title' => strtoupper($group['title']),
                'timescale' => 5,

                'incoming' => $group['incoming'],

                'receiver_contractor' => $group['receiver_contractor'] ?? false,
                'receiver_owner' => $group['receiver_owner'] ?? false,
                'receiver_tenant' => $group['receiver_tenant'] ?? false,
                'receiver_fom_manager' => $group['receiver_fom_manager'] ?? false,

                'sender_contractor' => $group['sender_contractor'] ?? false,
                'sender_owner' => $group['sender_owner'] ?? false,
                'sender_tenant' => $group['sender_tenant'] ?? false,
                'sender_fom_manager' => $group['sender_fom_manager'] ?? false,

                'oa' => $group['oa'] ?? false,
                'building_management' => $group['building_management'] ?? false,

                'step' => $group['step'],
            ]);

            foreach ($group['tasks'] as $task){
                \App\Models\FomTaskSetting::create([
                    'title' => ucfirst($task['title']),
                    'group_setting_id' => $groupSetting->id,
                    'type' => $task['type'],
                    'form_type' => $task['form_type'] ?? '',
                    'notification_message' => $task['notification_message'] ?? ''
                ]);
            }

        }

    }
}
