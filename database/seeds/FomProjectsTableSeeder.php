<?php

use Illuminate\Database\Seeder;

class FomProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');

        \App\Models\FomBuilding::truncate();
        \App\Models\FomProject::truncate();
        \App\Models\FomTask::truncate();
        \App\Models\FomTaskGroupUser::truncate();
        \App\Models\FomTaskGroup::truncate();

        \App\Models\FomBuilding::create([
            'name' => 'Marina Plaza',
        ]);
        \App\Models\FomBuilding::create([
            'name' => 'Opus',
        ]);
        \App\Models\FomBuilding::create([
            'name' => 'Opus 2',
        ]);
        \App\Models\FomBuilding::create([
            'name' => 'Marina Plaza 2',
        ]);


        factory(\App\Models\FomProject::class,50)->create();

        $projects = \App\Models\FomProject::all();
        $tasksGroupsSettings = \App\Models\FomTaskGroupSettings::all();

        $fomManagers = \App\Laravue\Models\User::whereHas("roles", function($q){ $q->where("name", \App\Laravue\Acl::ROLE_FOM_MANAGER); })->get();

        foreach ($projects as $project){

            foreach ($tasksGroupsSettings as $tasksGroupsSetting){

                $DBtasksGroup = \App\Models\FomTaskGroup::create([
                    'project_id' => $project->id,
                    'task_group_setting_id' => $tasksGroupsSetting->id,
                    'due_date' => null,
                    'assigned' => false,
                ]);

                if ($tasksGroupsSetting->receiver_contractor){


                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->contractor->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->contractor->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]);
                    }


                }
                if ($tasksGroupsSetting->receiver_owner){

                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->owner->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->owner->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]);
                    }


                }
                if ($tasksGroupsSetting->receiver_tenant){

                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->tenant->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->tenant->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]);
                    }
                }
                if ($tasksGroupsSetting->receiver_fom_manager){
                    foreach ($fomManagers as $fomManager){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $fomManager->id,
                            'project_id' => $project->id,
                            'type' => 'receiver',
                        ]);
                    }
                }
                if ($tasksGroupsSetting->sender_contractor){

                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->contractor->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->contractor->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]);
                    }
                }
                if ($tasksGroupsSetting->sender_owner){

                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->owner->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->owner->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]);
                    }

                }
                if ($tasksGroupsSetting->sender_tenant){

                    if (! \App\Models\FomTaskGroupUser::where(
                        [
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->tenant->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]
                    )->exists()){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->tenant->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]);
                    }

                }
                if ($tasksGroupsSetting->sender_fom_manager){
                    foreach ($fomManagers as $fomManager){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $fomManager->id,
                            'project_id' => $project->id,
                            'type' => 'sender',
                        ]);
                    }
                }
/*
                if ($tasksGroupsSetting->incoming){

                    foreach ($fomManagers as $fomManager){
                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $fomManager->id,
                            'project_id' => $project->id,
                        ]);
                    }

                }
                else{

                    if ($tasksGroupsSetting->contractor){

                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->contractor->id,
                            'project_id' => $project->id,
                        ]);

                    }

                    if ($tasksGroupsSetting->owner){

                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->owner->id,
                            'project_id' => $project->id,
                        ]);

                    }

                    if ($tasksGroupsSetting->contractor){

                        \App\Models\FomTaskGroupUser::create([
                            'task_group_id' => $DBtasksGroup->id,
                            'user_id' => $project->tenant->id,
                            'project_id' => $project->id,
                        ]);

                    }

                }*/

                foreach ($tasksGroupsSetting->tasks as $taskSetting){
                    \App\Models\FomTask::create([
                        'project_id' => $project->id,
                        'task_setting_id' => $taskSetting->id,
                        'task_group_id' => $DBtasksGroup->id,
                        'status' => 'Pending',
                        'notification_message' => $taskSetting->notification_message,
                    ]);
                }
            }

        }

    }
}
