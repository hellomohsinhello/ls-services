<?php

use Illuminate\Database\Seeder;

class InspectionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\InspectionType::truncate();

        $services = \App\Models\Service::all();

        $BSinspection_types = [
            ['title' => 'Civil'],
            ['title' => 'MEP'],
        ];

        $VALinspection_types = [
            ['title' => 'Drive-By'],
            ['title' => 'Desktop'],
            ['title' => 'Full Inspection'],
        ];

        foreach ($services as $service){

            if ($service->code == 'BS'){
                foreach ($BSinspection_types as $type){
                    factory(\App\Models\InspectionType::class)->create([
                        'title' => $type['title'],
                        'service_id' => $service->id,
                    ]);
                }
            }



            if ($service->code == 'VAL'){
                foreach ($VALinspection_types as $type){
                    factory(\App\Models\InspectionType::class)->create([
                        'title' => $type['title'],
                        'service_id' => $service->id,
                    ]);
                }
            }
        }
    }
}
