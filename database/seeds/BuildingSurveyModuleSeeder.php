<?php

use Illuminate\Database\Seeder;

class BuildingSurveyModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//      clear building survey data.
        \App\Models\BuildingSurveyAssignment::truncate();
        \App\Models\BuildingSurveyProperty::truncate();

//      add assignments
        factory(\App\Models\BuildingSurveyAssignment::class,15)->create();

//      add properties
        foreach (\App\Models\BuildingSurveyAssignment::all() as $assignment){
            factory(\App\Models\BuildingSurveyProperty::class,rand(5,15))->create([
                'assignment_id' => $assignment->id
            ]);

            //        set assignment team

            $users = \App\Laravue\Models\User::all()->random(rand(1,3));

            $assignment->team()->attach($users);

        }




    }
}
