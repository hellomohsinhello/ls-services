<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Service::truncate();

        \App\Models\Service::create([
            'title' => 'Building Survey',
            'code' => 'BS',
        ]);
        \App\Models\Service::create([
            'title' => 'Property Valuation',
            'code' => 'VAL',
        ]);
    }
}
