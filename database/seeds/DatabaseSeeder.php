<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Activitylog\Models\Activity::truncate();

        $this->call(ElementsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(StatusTableSeeder::class);
//        $this->call(ClientsTableSeeder::class);
        $this->call(AssignmentsSeeder::class);
        $this->call(TemplateTableSeeder::class);

//        building survey module seeder
        $this->call(BuildingSurveyModuleSeeder::class);
        $this->call(DefectsTableSeeder::class);

        $this->call(InspectionTypeTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);


        //fom
//        $this->call(FomTaskSettingsTableSeeder::class); //working
//        $this->call(FomProjectsTableSeeder::class); //working

    }
}
