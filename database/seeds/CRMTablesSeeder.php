<?php

use Illuminate\Database\Seeder;
use \App\Models\Lead;

class CRMTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Lead::truncate();

        factory(Lead::class,100)->create();
    }
}
