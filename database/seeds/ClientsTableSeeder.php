<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client_types = [
            'Bank',
            'Individual',
            'Company',
            'Others',
        ];

        foreach ($client_types as $client_type){
            \App\Models\ClientType::create([
                'title' => $client_type
            ]);
        }

        factory(\App\Models\Client::class,10)->create();
        factory(\App\Models\ClientContact::class,10)->create();
    }
}
