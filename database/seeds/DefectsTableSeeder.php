<?php

use Illuminate\Database\Seeder;

class DefectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\Defect::truncate();


        $property = \App\Models\BuildingSurveyProperty::first();


        factory(\App\Models\Defect::class,rand(50,100))->create([
            'property_id' => $property->id
        ]);

        $defects = \App\Models\Defect::all();

        dump($defects->count());

        foreach ($defects as $defect){
            $defect->addMediaFromUrl('https://picsum.photos/300')
                ->toMediaCollection('image');
        }

    }
}
