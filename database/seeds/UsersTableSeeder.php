<?php

use Illuminate\Database\Seeder;
use App\Laravue\Acl;
use App\Laravue\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Laravue\Models\User::truncate();

        $admin = \App\Laravue\Models\User::create([
            'firstname' => 'Land',
            'lastname' => 'Sterling',
            'email' => 'admin@landsterling.com',
            'password' => Hash::make('secret')
        ]);
        $contractor = \App\Laravue\Models\User::create([
            'firstname' => 'Contractor',
            'lastname' => 'Contractor',
            'email' => 'contractor@gmail.com',
            'password' => Hash::make('secret')
        ]);
        $tenant = \App\Laravue\Models\User::create([
            'firstname' => 'tenant',
            'lastname' => 'tenant',
            'email' => 'tenant@gmail.com',
            'password' => Hash::make('secret')
        ]);
        $owner = \App\Laravue\Models\User::create([
            'firstname' => 'Owner',
            'lastname' => 'Owner',
            'email' => 'owner@gmail.com',
            'password' => Hash::make('secret')
        ]);

        $fomHod = \App\Laravue\Models\User::create([
            'firstname' => 'Head',
            'lastname' => 'Fit Out Management',
            'email' => 'fomhead@gmail.com',
            'password' => Hash::make('secret')
        ]);
        $fomManager = \App\Laravue\Models\User::create([
            'firstname' => 'FOM',
            'lastname' => 'Manager',
            'email' => 'fommanager@gmail.com',
            'password' => Hash::make('secret')
        ]);

        $bsHod = \App\Laravue\Models\User::create([
            'firstname' => 'Head',
            'lastname' => 'Building Consultancy',
            'email' => 'bshead@gmail.com',
            'password' => Hash::make('secret')
        ]);
        $bsManager = \App\Laravue\Models\User::create([
            'firstname' => 'BS',
            'lastname' => 'Manager',
            'email' => 'bsmanager@gmail.com',
            'password' => Hash::make('secret')
        ]);



        $adminRole = Role::findByName(\App\Laravue\Acl::ROLE_ADMIN);

        $fomHodRole = Role::findByName(\App\Laravue\Acl::ROLE_FOM_HEAD);
        $fomManagerRole = Role::findByName(\App\Laravue\Acl::ROLE_FOM_MANAGER);

        $bsHodRole = Role::findByName(\App\Laravue\Acl::ROLE_BS_HEAD);
        $bsManagerRole = Role::findByName(\App\Laravue\Acl::ROLE_BS_MANAGER);

        $userRole = Role::findByName(\App\Laravue\Acl::ROLE_USER);

//        $fomManagerRole->givePermissionTo(Acl::PERMISSION_USER_MANAGE_PROFILE);
//        $fomManagerRole->givePermissionTo(Acl::PERMISSION_USER_MANAGE);
//        $fomManagerRole->givePermissionTo(Acl::PERMISSION_VIEW_MENU_ADMINISTRATOR);

        $admin->syncRoles($adminRole);

        $bsHod->syncRoles($bsHodRole);
        $bsManager->syncRoles($bsManagerRole);

        $fomHod->syncRoles($fomHodRole);
        $fomManager->syncRoles($fomManagerRole);

        $contractor->syncRoles($userRole);
        $tenant->syncRoles($userRole);
        $owner->syncRoles($userRole);

        $users = factory(\App\Laravue\Models\User::class,10)->create();

        foreach ($users as $user){

            $roleName = \App\Laravue\Faker::randomInArray([
                Acl::ROLE_USER,
            ]);

            $role = Role::findByName($roleName);
            $user->syncRoles($role);

        }

    }
}
