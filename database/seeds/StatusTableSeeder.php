<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Status::truncate();

        $statuses = [
            ['title' => 'New','color'=>'#F44336'],
            ['title' => 'Assigned','color'=>'#E91E63'],
            ['title' => 'Pending Inspection','color'=>'#9C27B0'],
            ['title' => 'Pending Valuation','color'=>'#9C27B0'],
            ['title' => 'Completed','color'=>'#9C27B0'],
            ['title' => 'Cancelled','color'=>'#FFC107'],
            ['title' => 'On Hold','color'=>'#CDDC39']
        ];


        foreach (\App\Models\Service::all() as $service){
            foreach ($statuses as $status){
                \App\Models\Status::create([
                    'title' => $status['title'],
                    'color' => $status['color'],
                    'statusable_id' => $service->id,
                    'statusable_type' => get_class($service),
                ]);
            }
        }
    }
}
