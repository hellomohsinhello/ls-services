<?php

use Illuminate\Database\Seeder;

class TemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = \App\Models\Service::all();

        $BStemplate = [
            [
                'title' => 'Normal Defect Template',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,
                    'description' => 1,
                    'recommendation' => 1,
                    'liable_parties' => 1,
                    'status/priority' => 1,
                    'defect' => 1,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'priority' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]

            ],
            [
                'title' => 'Aldar Defect Template',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'component' => 1,
                    'poac' => 1,
                    'rec' => 1,
                    'rating' => 1,
                    'priority' => 1,
                    'unit' => 1,
                    'quantity' => 1,
                    'total' => 1,
                    'category' => 1,
                    'element' => 1,
                    'defect' => 1,
                    'description' => 0,
                    'recommendation' => 0,
                    'liable_parties' => 0,
                    'status/priority' => 0,
                    'rate' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'Normal Template with desnagging',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,
                    'description' => 1,
                    'recommendation' => 1,
                    'liable_parties' => 1,
                    'defect' => 1,
                    'priority' => 1,
                    'rectified_image' => 1,
                    'unit' => 1,
                    'quantity' => 1,
                    'rate' => 1,
                    'total' => 1,
                    'status' => 1,


                    'status/priority' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'Jems & Jewel Template',
                'settings' => [
                    'number' => 1,
                    'element' => 1,
                    'contravention' => 1,
                    'risks' => 1,
                    'rectification' => 1,
                    'priority' => 1,
                    'defect' => 1,


                    'location' => 0,
                    'category' => 0,
                    'description' => 0,
                    'recommendation' => 0,
                    'liable_parties' => 0,
                    'status/priority' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'legislation' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'Desnagging Template',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,
                    'description' => 1,
                    'status/priority' => 1,
                    'defect' => 1,

                    'recommendation' => 0,
                    'liable_parties' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'priority' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' =>0
                ]
            ],
            /*[
                'title' => 'Nakheel Template - Villas',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,


                    'description' => 1,
                    'recommendation' => 1,
                    'liable_parties' => 1,
                    'status/priority' => 1,
                    'defect' => 1,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'priority' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 1,
                    'risks' => 1,
                    'legislation' => 1,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],*/
            [
                'title' => 'Burj Khalifa Template',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,
                    'description' => 1,
                    'status' => 1,
                    'rating' => 1,
                    'defect' => 1,

                    'recommendation' => 0,
                    'liable_parties' => 0,
                    'status/priority' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'priority' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'BCA Template',
                'settings' => [
                    'number' => 0,
                    'location' => 0,
                    'category' => 0,
                    'element' => 0,
                    'description' => 0,
                    'recommendation' => 0,
                    'priority' => 0,
                    'defect' => 0,

                    'liable_parties' => 0,
                    'status/priority' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'FOA Template',
                'settings' => [
                    'number' => 1,
                    'location' => 1,
                    'category' => 1,
                    'element' => 1,
                    'description' => 1,
                    'recommendation' => 1,
                    'status/priority' => 1,
                    'defect' => 1,

                    'liable_parties' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'priority' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'status' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                    'nrm_code' => 0,
                    'conditions' => 0,
                    'audio' => 0
                ]
            ],
            [
                'title' => 'KAFD Template',
                'settings' => [
                    'number' => 0,
                    'location' => 0,
                    'category' => 0,
                    'element' => 0,
                    'nrm_code' => 0,
                    'description' => 0,
                    'recommendation' => 0,
                    'conditions' => 0,
                    'status' => 0,
                    'defect' => 0,
                    'unit' => 0,
                    'quantity' => 0,
                    'audio' => 0,

                    'liable_parties' => 0,
                    'component' => 0,
                    'poac' => 0,
                    'rec' => 0,
                    'rating' => 0,
                    'priority' => 0,
                    'rate' => 0,
                    'total' => 0,
                    'rectified_image' => 0,
                    'contravention' => 0,
                    'risks' => 0,
                    'legislation' => 0,
                    'rectification' => 0,
                ]
            ],
        ];

        $VALtemplate = [
            ['title' => 'Single Unit / Residential Template'],
            ['title' => 'Commercial Valuation Template'],
            ['title' => 'Portfolio Template'],
            ['title' => 'Abu Dhabi Finance Template'],
            ['title' => 'New Valuation Template'],
            ['title' => 'Portfolio Multi-page Template']
        ];

        foreach ($services as $service){

            if ($service->code == 'BS'){
                foreach ($BStemplate as $type){
                    factory(\App\Models\Template::class)->create([
                        'title' => $type['title'],
                        'service_id' => $service->id,
                        'settings' => json_encode($type['settings']),
                    ]);
                }
            }

            if ($service->code == 'VAL'){
                foreach ($VALtemplate as $type){
                    factory(\App\Models\Template::class)->create([
                        'title' => $type['title'],
                        'service_id' => $service->id,
                    ]);
                }
            }
        }
    }
}
