<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FomTaskFileDownloaded extends Mailable
{
    use Queueable, SerializesModels;

    public $task;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task,$user)
    {
        $this->task = $task;
        $this->user = $user;
        $this->subject = "New Notification from Land Sterling!";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.fom-task-file-downloaded');
    }
}
