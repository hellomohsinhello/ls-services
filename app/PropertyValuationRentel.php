<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PropertyValuationRentel extends Model
{
    protected $guarded = [];

    protected $casts = [
        'date' => 'date'
    ];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value);
    }
}
