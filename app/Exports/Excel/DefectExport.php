<?php

namespace App\Exports\Excel;


use App\Models\Defect;
use App\Models\Property;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DefectExport implements FromView
{

    protected $property;
    protected $defects;

    public function __construct($property, $defects)
    {
        $this->property = $property;
        $this->defects = $defects;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('exports.excel.defects', [
            'property' => $this->property,
            'defects' => $this->defects,
        ]);
    }
}
