<?php

namespace App\Console\Commands;

use App\Events\FomTaskStatusUpdate;
use App\Laravue\Models\User;
use App\Notifications\FomTaskStatusApproved;
use Illuminate\Console\Command;

class SendNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::first();

        $user->notify(new FomTaskStatusApproved());

//        return event(new FomTaskStatusUpdate());
    }
}
