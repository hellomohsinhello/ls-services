<?php

namespace App\Providers;

use App\Models\FomProject;
use App\Models\Property;
use App\Services\WordDoc;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        Schema::defaultStringLength(191);

        /*Relation::morphMap([
            'employee' => \App\Models\Employee::class,
            'client' => \App\Models\Client::class,
            'client_contact' => \App\Models\ClientContact::class,
        ]);*/
        Relation::morphMap([
            'fom_project' => FomProject::class,
            'properties' => Property::class,
        ]);

        $this->app->bind('App\Services\WordDoc', function () {
            return new WordDoc();
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
