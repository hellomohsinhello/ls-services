<?php

namespace App\Services;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class WordDoc{

    public $phpWord;
    public $table;
    public $section;


    public function __construct()
    {
        $this->phpWord = new PhpWord();
        $this->phpWord->setDefaultFontSize(12);
        $this->phpWord->getCompatibility()->setOoxmlVersion(15);

    }

    public function addSection()
    {
        $sectionStyle = ['orientation' => 'landscape', 'marginLeft' => 800, 'marginRight' => 800, 'marginTop' => 1800, 'marginBottom' => 600];

        $this->section = $this->phpWord->addSection($sectionStyle);

    }

    public function addTable()
    {
        $styleTable = ['borderSize' => 4, 'borderColor' => '000000', 'width'=>14500,    'cellMarginRight'  => 0,   'cellMarginLeft'   => 0,  'cellMarginBottom'   => 0, 'cellMarginTop'   => 0];

        $this->phpWord->addTableStyle('NORMAL TEMPLATE', $styleTable);

        $this->table = $this->section->addTable('NORMAL TEMPLATE');

    }

    public function addTR()
    {
        $this->table->addRow();
    }

    public function addTH($width, $text)
    {

        $cellRowSpanH = ['vMerge' => 'restart', 'valign' => 'center', 'bgColor' => '0c4789','color'=>'FFFFFF'];
        $cellHTextStyleP = ['color'=>'FFFFFF', 'size'=>10, 'bold'=>false,'name'=>'Century Gothic','width'=>1000,'lineHeight'=> 1.0];
        $cellHCentered = ['align' => 'center','lineHeight'=> 1.0, 'spaceAfter'=>0, 'spaceBefore'=>0];

        $this->table->addCell($width,  $cellRowSpanH)->addText(htmlspecialchars($text), $cellHTextStyleP, $cellHCentered);

    }

    public function addTD($width, $text)
    {

        $cellRowSpan = ['vMerge' => 'restart', 'valign' => 'center', 'bgColor' => 'FFFFFF','color'=>'000000'];
        $cellNTextStyle = ['color'=>'000000', 'size'=>10, 'bold'=>false,'name'=>'Century Gothic','lineHeight'=> 1.0];
        $cellHCentered = ['align' => 'center','lineHeight'=> 1.0, 'spaceAfter'=>0, 'spaceBefore'=>0];

        $this->table->addCell($width, $cellRowSpan)->addText(htmlspecialchars($text), $cellNTextStyle, $cellHCentered);

    }

    public function addTDIMG($width, $path)
    {
        $cellHCentered = ['align' => 'center','lineHeight'=> 1.0, 'spaceAfter'=>0, 'spaceBefore'=>0];

        $imagecell = $this->table->addCell($width, $cellHCentered);
        $imagecell->addImage($path,['align'=> 'center','width'=> 100,'wrappingStyle' => 'inline',] );
    }

    public function pageBreak()
    {
        $this->section->addPageBreak();
    }

    public function saveFile($name)
    {
        $file = storage_path('docs/'.$name.".docx");
        $xmlWriter = IOFactory::createWriter($this->phpWord, 'Word2007');
        $xmlWriter->save($file);

        return $file;
    }
}
