<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FomTaskGroupAssignedNotification extends Notification
{
    use Queueable;
    public $group;
    public $user;
    public $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($group)
    {
        //
        $this->group = $group;
        $this->user = auth()->user();
        $this->url =  url("?#/fit-out-management/projects/{$this->group->project->id}/stages?group={$this->group->id}&step={$this->group->step}");
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line("Group **{$this->group->setting->title}** of tasks **assigned** to you by **{$this->user->fullname}**")
            ->line("**Due Date** {$this->group->due_date->toDayDateTimeString()}")
            ->action('Go to dashboard', $this->url)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => '',
            'message' => "Group <b>{$this->group->setting->title}</b> of tasks <b>assigned</b> to you by <b>{$this->user->fullname}</b> and <b>Due Date</b> is {$this->group->due_date->toDayDateTimeString()}. <a href='$this->url'>Click Here</a>",
            'action' =>$this->url,
            'by_user_avatar' =>$this->user->avatar
      ];
    }
}
