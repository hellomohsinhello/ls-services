<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FomTaskStatusUpdatedNotification extends Notification
{
    use Queueable;

    public $fomTask;
    public $user;
    public $url;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($fomTask)
    {
        $this->fomTask = $fomTask;
        $this->user = auth()->user();
        $this->url =  url("?#/fit-out-management/projects/{$this->fomTask->project->id}/stages?group={$this->fomTask->task_group->id}&step={$this->fomTask->task_group->step}" );

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line("Task **{$this->fomTask->setting->title}** is **{$this->fomTask->status}** by **{$this->user->fullname}**")
            ->action('Go to dashboard', $this->url)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => "Task {$this->fomTask->status}",
            'message' => "Task <b>{$this->fomTask->setting->title}</b> is <b>{$this->fomTask->status}</b> by <b>{$this->user->fullname}</b> <a href='$this->url'>Click Here</a>",
            'action' =>$this->url,
            'by_user_avatar' =>$this->user->avatar
       ];
    }

}
