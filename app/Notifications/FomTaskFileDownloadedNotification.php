<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FomTaskFileDownloadedNotification extends Notification
{
    use Queueable;
    public $task;
    public $user;
    public $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task,$user)
    {
        //
        $this->task = $task;
        $this->user = $user;
        $this->url =   url("?#/fit-out-management/projects/{$this->task->project->id}/stages?group={$this->task->task_group->id}&step={$this->task->task_group->step}" );
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("The **{$this->task->setting->title}** in **{$this->task->task_group->setting->title}**  has been downloaded by **{$this->user->fullname}**")
                    ->action('Go to dashboard', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => '',
            'message' => "File downloaded by <b>{$this->user->fullname}</b> from <b>{$this->task->setting->title}</b> in <b>{$this->task->task_group->setting->title}</b>. <a href='$this->url'>Click Here</a>",
            'action' =>$this->url,
            'by_user_avatar' =>$this->user->avatar
       ];
    }
}
