<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BuildingSurveyAssignmentStatusUpdateNotification extends Notification
{
    use Queueable;
    private $assignment;
    private $status;
    private $user;
    private $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($assignment,$status)
    {
        //
        $this->assignment = $assignment;
        $this->status = $status;
        $this->user = auth()->user();
        $this->url =  url("?#/building-survey/assignments" );
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => '',
            'message' => "Building Inspection assignment <b>{$this->assignment->title}</b> is <b>{$this->status}</b> by <b>{$this->user->fullname}</b>. <a href='$this->url'>Click Here</a>",
            'action' => $this->url,
            'by_user_avatar' =>$this->user->avatar
        ];
    }
}
