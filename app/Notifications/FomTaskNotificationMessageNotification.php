<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FomTaskNotificationMessageNotification extends Notification
{
    use Queueable;
    private $task;
    private $url;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($task)
    {
        $this->task = $task;
        $this->user = auth()->user();
        $this->url = url("?#/fit-out-management/projects/{$task->project->id}/stages?group={$task->task_group->id}&step={$task->task_group->step}");
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('You have new notification.')
            ->line(request('comment'))
            ->action('Go to dashboard', $this->url)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => '',
            'message' => "<b>{{ $this->user->fullname }}</b> posted new comment. <a href='$this->url'>Click Here</a>",
            'action' =>$this->url,
            'by_user_avatar' =>$this->user->avatar
      ];
    }
}
