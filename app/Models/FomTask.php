<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class FomTask extends Model implements HasMedia
{
    use HasMediaTrait;
    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'fom task';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $casts = [
        'comments' => 'array'
    ];


    const TYPE_FILE_UPLOAD = 'file upload';
    const TYPE_FILE_UPLOAD_ONCE = 'file upload once';
    const TYPE_NOTIFICATION = 'notification';
    const TYPE_FILL_UP_FORM = 'fill-up form';


    protected $appends = ['file_url','file_download'];

    protected $guarded = [];

    public function setting()
    {
        return $this->belongsTo(FomTaskSetting::class,'task_setting_id');
    }

    public function task_group_setting()
    {
        $this->belongsTo(FomTaskGroupSettings::class,'task_group_setting_id');
    }

    public function project()
    {
        return $this->belongsTo(FomProject::class,'project_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class,'users_to_notify_for_tasks','task_id','user_id');
    }

    public function getFileUrlAttribute()
    {
        return $this->getFirstMedia('file')? $this->getFirstMedia('file')->getFullUrl():null;
    }

    public function getFileDownloadAttribute()
    {
        return $this->getFirstMedia('file')? route('fom-tasks-file-download',[
            $this->id , auth()->id()
        ]):null;
    }

    public function downloader()
    {
        return $this->hasManyThrough(MediaDownload::class,Media::class,'model_id')
            ->where('model_type', static::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('file')
            ->singleFile();
    }

    public function task_group()
    {
        return $this->belongsTo(FomTaskGroup::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function permit_to_works()
    {
        return $this->hasMany(FomPermitToWorkForm::class,'task_id');
    }

    public function id_card_forms()
    {
        return $this->hasMany(FomIDCardForm::class,'task_id');
    }

    public function attendance_forms()
    {
        return $this->hasMany(FomAttendanceForm::class,'task_id');
    }

    public function common_area_dilapidations()
    {
        return $this->hasMany(FomCommonAreaDilapidation::class,'task_id');
    }

    public function certificate_of_conformities()
    {
        return $this->hasMany(FomCertificateOfConformity::class,'task_id');
    }

    public function delivery_access_permits()
    {
        return $this->hasMany(FomDeliveryAccessPermit::class,'task_id');
    }

    public function exit_pass_permits()
    {
        return $this->hasMany(FomExitPassPermit::class,'task_id');
    }

    public function waste_disposal_permits()
    {
        return $this->hasMany(FomWasteDisposalPermit::class,'task_id');
    }

    public function material_delivery_permits()
    {
        return $this->hasMany(FomMaterialDeliveryPermit::class,'task_id');
    }

    public function hot_work_permits()
    {
        return $this->hasMany(FomHotWorkPermit::class,'task_id');
    }

    public function extended_hours_permits()
    {
        return $this->hasMany(FomExtendedHoursPermit::class,'task_id');
    }

    public function hse_breach_reports()
    {
        return $this->hasMany(FomHseBreachReport::class,'task_id');
    }

    public function disciplinary_action_issues()
    {
        return $this->hasMany(FomDisciplinaryActionIssued::class,'task_id');
    }

    public function incident_notifications()
    {
        return $this->hasMany(FomIncidentNotification::class,'task_id');
    }

    public function mep_services_permits()
    {
        return $this->hasMany(FomMepServicesPermit::class,'task_id');
    }

    public function access_work_permits()
    {
        return $this->hasMany(FomAccessWorkPermit::class,'task_id');
    }

    public function first_fix_inspections()
    {
        return $this->hasMany(FomFirstFixInspection::class,'task_id');
    }

    public function vehicle_parking_registers()
    {
        return $this->hasMany(FomVehicleParkingRegister::class,'task_id');
    }

    public function hse_weekly_inspections()
    {
        return $this->hasMany(FomHseWeeklyInspection::class,'task_id');
    }

    public function material_delivery_permit()
    {
        return $this->hasMany(FomMaterialDeliveryPermit::class,'task_id');
    }

}
