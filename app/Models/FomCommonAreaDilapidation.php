<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomCommonAreaDilapidation extends Model
{
    protected $guarded = [];
    protected $appends = ['download_url'];

    public function getItemsAttribute($val)
    {
        return json_decode($val);
    }

    public function getDownloadUrlAttribute()
    {
        return route('commonAreaDilapidation.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
