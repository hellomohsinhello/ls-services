<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomDeliveryAccessPermit extends Model
{
    protected $appends = ['download_url'];
    protected $guarded = [];

    public function getDownloadUrlAttribute()
    {
        return route('deliveryAccessPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
