<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomHseBreachReport extends Model
{
    protected $guarded = [];

    public function getDispositionsAttribute($val)
    {
        return json_decode($val);
    }

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('hseBreachReport.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
