<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function statuses()
    {
        return $this->morphMany(Status::class, 'statusable');
    }

    public function templates()
    {
        return $this->hasMany(Template::class);
    }

    public function inspections()
    {
        return $this->hasMany(InspectionType::class);
    }

}
