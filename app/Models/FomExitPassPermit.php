<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomExitPassPermit extends Model
{
    protected $appends = ['download_url'];
    protected $guarded = [];

    public function getMaterialsAttribute($val)
    {
        return json_decode($val);
    }

    public function getDownloadUrlAttribute()
    {
        return route('exitPassPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
