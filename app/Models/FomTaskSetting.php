<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomTaskSetting extends Model
{
    public function group()
    {
        $this->belongsTo(FomTaskGroupSettings::class,'group_setting_id');
    }
}
