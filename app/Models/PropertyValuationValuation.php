<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyValuationValuation extends Model
{

    protected $casts = [
        'interests' => 'json' ,
        'purpose' => 'json' ,
        'basis' => 'json',
        'methods' => 'json',
        'valuation_date' => 'date'
    ];

    protected $guarded = [];

    public function setPurposeAttribute($value)
    {
        $this->attributes['purpose'] =  json_encode($value);
    }

    public function setBasisAttribute($value)
    {
        $this->attributes['basis'] =  json_encode($value);
    }

    public function setInterestsAttribute($value)
    {
        $this->attributes['interests'] =  json_encode($value);
    }

    public function setMethodsAttribute($value)
    {
        $this->attributes['methods'] =  json_encode($value);
    }

    public function getInterestsTextAttribute($value)
    {
        $text = '';
        foreach ($this->interests as $interest){
            $text .= $interest . ', ';
        }
        return $text;
    }

    public function getPurposeTextAttribute($value)
    {
        $text = '';
        foreach ($this->purpose as $item){
            $text .= $item . ', ';
        }
        return $text;
    }

}
