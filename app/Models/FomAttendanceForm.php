<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomAttendanceForm extends Model
{
    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'Fit-Out Attendance Form';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];


    protected $guarded = [];
    protected $appends = ['download_url'];

    public function getAttendancesAttribute($val)
    {
        return json_decode($val);
    }

    public function getDownloadUrlAttribute()
    {
        return route('attendanceForm.download',$this->id);
    }

    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
