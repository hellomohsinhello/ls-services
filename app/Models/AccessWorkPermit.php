<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessWorkPermit extends Model
{

    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
