<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomBuilding extends Model
{
    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'Fit-Out Building';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];


    protected $guarded = [];

    public function projects()
    {
        return $this->hasMany(FomProject::class,'building_id');
    }
}
