<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class MapsLocation extends Model implements HasMedia
{
    use HasMediaTrait;

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('maker')
            ->singleFile();
        $this
            ->addMediaCollection('polygon')
            ->singleFile();
    }

    protected $guarded = [];

    protected $appends = ['marker_img_url','polygon_img_url'];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float',
        'position_lat' => 'float',
        'position_lng' => 'float',
        'polygon' => 'json',
    ];

    public function setPolygonAttribute($val)
    {
        $this->attributes['polygon'] = json_encode($val);
    }

    public function getMarkerImgUrlAttribute()
    {
        return optional($this->getFirstMedia('maker'))->getFullUrl();
    }

    public function getPolygonImgUrlAttribute()
    {
        return optional($this->getFirstMedia('polygon'))->getFullUrl();
    }



    public function getMarkerImgPathAttribute()
    {
        return optional($this->getFirstMedia('maker'))->getPath();
    }

    public function getPolygonImgPathAttribute()
    {
        return optional($this->getFirstMedia('polygon'))->getPath();
    }


    public function markerImgUrl()
    {

//        https://maps.googleapis.com/maps/api/staticmap?maptype=hybrid&size=450x400&key=AIzaSyCtihf0Jsmc9CNaGf2TeLksScvQzQgvyZ0&zoom=13&markers=color:C11818%7C25.20944,55.38904

        return url('https://maps.googleapis.com/maps/api/staticmap') . '?' . http_build_query([
                'maptype' => "hybrid",
                'size' => "500x400",
                'center' => "{$this->lat},{$this->lng}",
                'zoom' => $this->zoom - 2,
                'key' => env('GOOGLE_MAPS'),
                'markers' => "color:C11818|{$this->lat},{$this->lng}",
            ]);
    }

    public function polygonImgUrl()
    {

        $path = '';

        if (isset($this->polygon[0])){

            foreach ($this->polygon as $item){
                $path .= "|{$item['lat']},{$item['lng']}";
            }

            $path .= "|{$this->polygon[0]['lat']},{$this->polygon[0]['lng']}";
        }

        return url('https://maps.googleapis.com/maps/api/staticmap') . '?' . http_build_query([
                'maptype' => "hybrid",
                'size' => "450x400",
                'zoom' => $this->zoom,
                'key' => env('GOOGLE_MAPS'),
                'path' => "color:0xFFFF33FF|weight:4|fillcolor:white{$path}",
            ]);


//        return "https://maps.googleapis.com/maps/api/staticmap?maptype=hybrid&size=450x400&key=AIzaSyCtihf0Jsmc9CNaGf2TeLksScvQzQgvyZ0&zoom={$this->zoom}&path=color:0xFFFF33FF|weight:4|fillcolor:white{$path}";
    }



    public function locationable()
    {
        return $this->morphTo();
    }
}
