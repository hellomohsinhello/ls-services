<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomIncidentNotification extends Model
{
    protected $guarded = [];


    public function getNearMissesAttribute($val)
    {
        return json_decode($val);
    }


    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('incidentNotification.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
