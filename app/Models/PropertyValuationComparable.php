<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyValuationComparable extends Model
{
    protected $guarded = [];

    protected $casts = [
        'date' => 'date'
    ];
}
