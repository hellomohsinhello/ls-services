<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class FomTaskGroupSettings extends Model
{

//    protected $appends = ['date'];

    public function tasks()
    {
        return $this->hasMany(FomTaskSetting::class,'group_setting_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class,'fom_project_groups_tasks_users','tasks_group_id','user_id')
            ->withPivot('project_id');
    }

    public function isIncoming()
    {
        return $this->incoming;
    }

    public function isOutGoing()
    {
        return ! $this->isIncoming();
    }

}
