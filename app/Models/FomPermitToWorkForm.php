<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomPermitToWorkForm extends Model
{

    protected $appends = ['download_url'];

    protected $guarded = [];

    public function getApprovalsAttribute($val)
    {
        return json_decode($val);
    }
    public function getSubContractorsAttribute($val)
    {
        return json_decode($val);
    }
    public function getDesignApprovalsAttribute($val)
    {
        return json_decode($val);
    }

    public function getDownloadUrlAttribute()
    {
        return route('permitToWorkForm.download',$this->id);
    }

    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
