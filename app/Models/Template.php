<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function getSettingsAttribute($value)
    {
        return json_decode($value);
    }
}
