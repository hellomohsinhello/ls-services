<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomAccessWorkPermit extends Model
{

    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'Fit-Out Access Work Permit';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $guarded = [];

    public function getAreasAttribute($val)
    {
        return json_decode($val);
    }

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('accessWorkPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
