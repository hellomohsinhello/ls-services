<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomHotWorkPermit extends Model
{

    protected $guarded = [];

    public function getDisposalmetodsAttribute($val)
    {
        return json_decode($val);
    }

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('hotWorkPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
