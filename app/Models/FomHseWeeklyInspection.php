<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomHseWeeklyInspection extends Model
{
    protected $guarded = [];

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('hseWeeklyInspection.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
