<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomTaskGroup extends Model
{

    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'fom task group';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $appends = ['step','step_t'];

//    protected $dates = ['due_date'];
    protected $guarded = [];

    public function setting()
    {
        return $this->belongsTo(FomTaskGroupSettings::class,'task_group_setting_id');
    }

    public function project()
    {
        return $this->belongsTo(FomProject::class,'project_id');
    }

    public function tasks()
    {
        return $this->hasMany(FomTask::class,'task_group_id');
    }

    public function receivers()
    {
        return $this->belongsToMany(User::class,'fom_task_group_users','task_group_id','user_id')
            ->withPivot(['project_id','type'])
            ->where('type','receiver')
            ->withTimestamps();
    }

    public function senders()
    {
        return $this->belongsToMany(User::class,'fom_task_group_users','task_group_id','user_id')
            ->withPivot(['project_id','type'])
            ->where('type','sender')
            ->withTimestamps();
    }

    public function assign_alert_users()
    {
        if ($this->isIncoming()){
            return $this->senders()->get();
        }

        return $this->receivers()->get();
    }


    public function users()
    {
        return $this->belongsToMany(User::class,'fom_task_group_users','task_group_id','user_id')
            ->withPivot(['project_id','type'])
            ->withTimestamps();
    }

    public function getDueDateAttribute($value)
    {
        if ($value){
            return $value;
        }

        return now()->addDays($this->setting->timescale);
    }

    public function getStepAttribute()
    {
        return $this->setting->step;
    }



    public function getStepTAttribute()
    {
        $steps = ['one','two','three','four','five','six','seven','eight','nine'];

        return $steps[$this->setting->step];
    }

    public function isAssigned()
    {
        return $this->assigned;
    }

    public function notAssigned()
    {
        return ! $this->isAssigned();
    }

    public function isIncoming()
    {
        return $this->setting->isIncoming();
    }

    public function isOutGoing()
    {
        return $this->setting->isOutGoing();
    }

}
