<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $appends = ['display'];

    protected $with = ['province.country'];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function getDisplayAttribute()
    {
        return "{$this->name} > {$this->province->name} > {$this->province->country->name}";
    }

    public function getDisplayCommaAttribute()
    {
        return "{$this->name}, {$this->province->name}, {$this->province->country->name}";
    }
}
