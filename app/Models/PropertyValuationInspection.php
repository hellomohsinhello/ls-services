<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyValuationInspection extends Model
{
    protected $casts = [
        'boundary_walls' => 'json' ,
        'cladding' => 'json',
        'doors' => 'json',
        'yard_coverings' => 'json',
        'windows' => 'json',
        'facilities' => 'json',
        'structures' => 'json',
        'internally' => 'json',
        'finishing' => 'json',
        'air_conditioning' => 'json',
        'surroundings' => 'json',
        'view' => 'json',
        'inspection_date_time' => 'datetime',
    ];

    protected $guarded = [];


    public function setBoundaryWallsAttribute($value)
    {
        $this->attributes['boundary_walls'] =  json_encode($value);
    }

    public function setSurroundingsAttribute($value)
    {
        $this->attributes['surroundings'] =  json_encode($value);
    }

    public function setCladdingAttribute($value)
    {
        $this->attributes['cladding'] =  json_encode($value);
    }

    public function setDoorsAttribute($value)
    {
        $this->attributes['doors'] =  json_encode($value);
    }

    public function setYardCoveringsAttribute($value)
    {
        $this->attributes['yard_coverings'] =  json_encode($value);
    }

    public function setWindowsAttribute($value)
    {
        $this->attributes['windows'] =  json_encode($value);
    }
    public function setFacilitiesAttribute($value)
    {
        $this->attributes['facilities'] =  json_encode($value);
    }
    public function setStructuresAttribute($value)
    {
        $this->attributes['structures'] =  json_encode($value);
    }
    public function setInternallyAttribute($value)
    {
        $this->attributes['internally'] =  json_encode($value);
    }
    public function setFinishingAttribute($value)
    {
        $this->attributes['finishing'] =  json_encode($value);
    }
    public function setAirConditioningAttribute($value)
    {
        $this->attributes['air_conditioning'] =  json_encode($value);
    }
    public function setViewAttribute($value)
    {
        $this->attributes['view'] =  json_encode($value);
    }

}
