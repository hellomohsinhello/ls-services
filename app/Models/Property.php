<?php

namespace App\Models;

use App\Laravue\Models\User;
use App\PropertyValuationRentel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Property extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $casts = [
        'instruction_date' => 'date',
        'inspection_datetime' => 'datetime',
        'interests' => 'json' ,
        'purpose' => 'json' ,
        'basis' => 'json',
        'methods' => 'json',
        'valuation_date' => 'date',
        'boundary_walls' => 'json' ,
        'cladding' => 'json',
        'doors' => 'json',
        'yard_coverings' => 'json',
        'windows' => 'json',
        'facilities' => 'json',
        'structures' => 'json',
        'internally' => 'json',
        'finishing' => 'json',
        'air_conditioning' => 'json',
        'surroundings' => 'json',
        'view' => 'json',
        'inspection_date_time' => 'datetime',
        'comments' => 'array'
    ];

    public function setPurposeAttribute($value)
    {
        $this->attributes['purpose'] =  json_encode($value);
    }
    public function setBasisAttribute($value)
    {
        $this->attributes['basis'] =  json_encode($value);
    }
    public function setInterestsAttribute($value)
    {
        $this->attributes['interests'] =  json_encode($value);
    }
    public function setMethodsAttribute($value)
    {
        $this->attributes['methods'] =  json_encode($value);
    }
    public function maps_location()
    {
        return $this->morphOne(MapsLocation::class,'locationable');
    }
    public function getInterestsTextAttribute($value)
    {
        $text = '';
        if (is_null($this->interests)){
            return '';
        }
        foreach ($this->interests as $interest){
            $text .= $interest . ', ';
        }
        return $text;
    }
    public function getBasisTextAttribute($value)
    {
        $text = '';
        if (is_null($this->basis)){
            return '';
        }
        foreach ($this->basis as $interest){
            $text .= $interest . ', ';
        }
        return $text;
    }
    public function getPurposeTextAttribute($value)
    {
        $text = '';
        foreach ($this->purpose as $item){
            $text .= $item . ', ';
        }
        return $text;
    }
    public function setBoundaryWallsAttribute($value)
    {
        $this->attributes['boundary_walls'] =  json_encode($value);
    }
    public function setSurroundingsAttribute($value)
    {
        $this->attributes['surroundings'] =  json_encode($value);
    }
    public function setCladdingAttribute($value)
    {
        $this->attributes['cladding'] =  json_encode($value);
    }
    public function setDoorsAttribute($value)
    {
        $this->attributes['doors'] =  json_encode($value);
    }
    public function setYardCoveringsAttribute($value)
    {
        $this->attributes['yard_coverings'] =  json_encode($value);
    }
    public function setWindowsAttribute($value)
    {
        $this->attributes['windows'] =  json_encode($value);
    }
    public function setFacilitiesAttribute($value)
    {
        $this->attributes['facilities'] =  json_encode($value);
    }
    public function setStructuresAttribute($value)
    {
        $this->attributes['structures'] =  json_encode($value);
    }
    public function setInternallyAttribute($value)
    {
        $this->attributes['internally'] =  json_encode($value);
    }
    public function setFinishingAttribute($value)
    {
        $this->attributes['finishing'] =  json_encode($value);
    }
    public function setAirConditioningAttribute($value)
    {
        $this->attributes['air_conditioning'] =  json_encode($value);
    }
    public function setViewAttribute($value)
    {
        $this->attributes['view'] =  json_encode($value);
    }

    protected $guarded = [];

    protected $withCount = ['defects'];

    protected $appends = ['area_num','photos','appendices','documents','ref_no'];
    public function getRefNoAttribute()
    {

        return 'ok';

        $alphabates = [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            'AA',
            'AB',
            'AC',
            'AD',
            'AE',
            'AF',
            'AG',
            'AH',
            'AI',
            'AJ',
            'AK',
            'AL',
            'AM',
            'AN',
            'AO',
            'AP',
            'AQ',
            'AR',
            'AS',
            'AT',
            'AU',
            'AV',
            'AW',
            'AX',
            'AY',
            'AZ',
            'BA',
            'BB',
            'BC',
            'BD',
            'BE',
            'BF',
            'BG',
            'BH',
            'BI',
            'BJ',
            'BK',
            'BL',
            'BM',
            'BN',
            'BO',
            'BP',
            'BQ',
            'BR',
            'BS',
            'BT',
            'BU',
            'BV',
            'BW',
            'BX',
            'BY',
            'BZ',
            'CA',
            'CB',
            'CC',
            'CD',
            'CE',
            'CF',
            'CG',
            'CH',
            'CI',
            'CJ',
            'CK',
            'CL',
            'CM',
            'CN',
            'CO',
            'CP',
            'CQ',
            'CR',
            'CS',
            'CT',
            'CU',
            'CV',
            'CW',
            'CX',
            'CY',
            'CZ',
            'DA',
            'DB',
            'DC',
            'DD',
            'DE',
            'DF',
            'DG',
            'DH',
            'DI',
            'DJ',
            'DK',
            'DL',
            'DM',
            'DN',
            'DO',
            'DP',
            'DQ',
            'DR',
            'DS',
            'DT',
            'DU',
            'DV',
            'DW',
            'DX',
            'DY',
            'DZ',
            'EA',
            'EB',
            'EC',
            'ED',
            'EE',
            'EF',
            'EG',
            'EH',
            'EI',
            'EJ',
            'EK',
            'EL',
            'EM',
            'EN',
            'EO',
            'EP',
            'EQ',
            'ER',
            'ES',
            'ET',
            'EU',
            'EV',
            'EW',
            'EX',
            'EY',
            'EZ',
        ];

        $i = 0;
        foreach ($properties as $property){
            $property->ref_number = $this->assignment->ref_no.$alphabates[$i++];
        }

    }
    public function registerMediaConversions(\Spatie\MediaLibrary\Models\Media $media = null)
    {
        $this->addMediaConversion('watermark')
            ->watermark(public_path('reports-assets/watermark.png'))
            ->watermarkPosition(Manipulations::POSITION_CENTER)
            ->watermarkOpacity(50)
            ->performOnCollections('photos');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('photos');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    public function template()
    {
        return $this->belongsTo(Template::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function community()
    {
        return $this->belongsTo(Area::class,'area_id');
    }
    public function valuer()
    {
        return $this->belongsTo(User::class);
    }
    public function inspector()
    {
        return $this->belongsTo(User::class);
    }
    public function type()
    {
        return $this->belongsTo(InspectionType::class,'inspection_type_id');
    }
    public function property_type()
    {
        return $this->belongsTo(PropertyType::class,'property_type_id');
    }
    public function defects()
    {
        return $this->hasMany(Defect::class);
    }
    public function getAreaNumAttribute()
    {
        return $this->area;
    }
    public function getPhotosAttribute()
    {
        return $this->getMedia('photos');
    }
    public function getCoverPhotoPathAttribute()
    {
        $photos = $this->getMedia('photos');

        if (count($photos) >= $this->cover_page_photo_index){
            return $photos[$this->cover_page_photo_index]->getPath();
        }

        return $this->getFirstMediaPath('photos');

    }
    public function getAppendicesAttribute()
    {
        return $this->getMedia('appendices');
    }
    public function getDocumentsAttribute()
    {
        return $this->getMedia('documents');
    }

    /*public function getAddressOldGetAttribute($val)
    {
        $area = optional($this->community)->display_comma;

        return is_null($this->address_old) ? "Plot# {$this->plot_number}, Property# {$this->property_number}, {$area}" : $this->address_old;
    }*/

    /*public function inspection()
    {
        return $this->hasOne(PropertyValuationInspection::class);
    }

    public function valuation()
    {
        return $this->hasOne(PropertyValuationValuation::class);
    }*/

    public function comparables()
    {
        return $this->hasMany(PropertyValuationComparable::class);
    }
    public function costs()
    {
        return $this->hasMany(PropertyValuationCost::class);
    }
    public function costs_uc()
    {
        return $this->hasMany(PropertyValuationCostUC::class);
    }
    public function breakdowns()
    {
        return $this->hasMany(PropertyValuationBreakdown::class);
    }
    public function rentels()
    {
        return $this->hasMany(PropertyValuationRentel::class);
    }
    public function getPhotosPageTitleAttribute($val)
    {
        return ($val == '')? 'Photos': $val;
    }
    public function getIncludePhotosCountAttribute($val)
    {
        return (is_null($this->include_photos) or $this->include_photos === 'all')? $this->photos->count() : $this->include_photos;
    }


}
