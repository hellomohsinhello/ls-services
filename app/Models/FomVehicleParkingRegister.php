<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomVehicleParkingRegister extends Model
{
    protected $guarded = [];

    public function getVehiclesAttribute($val)
    {
        return json_decode($val);
    }

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('vehicleParkingRegister.download',$this->id);
    }

    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
