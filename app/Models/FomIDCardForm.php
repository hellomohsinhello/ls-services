<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomIDCardForm extends Model
{
    protected $guarded = [];
    protected $appends = ['download_url'];

    public function getCardsAttribute($val)
    {
        return json_decode($val);
    }

    public function getDownloadUrlAttribute()
    {
        return route('IDCardForm.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
