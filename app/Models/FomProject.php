<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomProject extends Model
{

    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'fom project';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $guarded = [];

    protected $appends = ['steps'];

    /*public function getUnitsAttribute($value)
    {
        return json_decode($value);
    }*/

    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id');
    }

    public function tenant()
    {
        return $this->belongsTo(User::class,'tenant_id');
    }

    public function contractor()
    {
        return $this->belongsTo(User::class,'contractor_id');
    }

    public function building()
    {
        return $this->belongsTo(FomBuilding::class,'building_id');
    }

    public function getStepsAttribute()
    {

        return $this->task_groups()
                ->where('assigned', true)
                ->with('setting')
                ->orderBy('id', 'desc')
                ->first()->setting->step ?? 0;


        /*return optional(optional($this->task_groups()->where('assigned', true)->with('setting')->orderBy('id', 'desc')->first())->setting)->step ?? 0;*/

        /*$id = $this->task_groups()->where('assigned',true)->max('id');

        if (is_null($id)){
            return 0;
        }

        return FomTaskGroup::find($id)->setting->step;*/

    }

    public function tasks()
    {
        return $this->hasMany(FomTask::class,'project_id');
    }

    public function task_groups()
    {
        return $this->hasMany(FomTaskGroup::class,'project_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
