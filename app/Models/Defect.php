<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Defect extends Model implements HasMedia
{
    use HasMediaTrait;

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile();
    }

    protected $appends = ['image_url'];

    protected $guarded = [];

    public function getImageUrlAttribute()
    {
        return optional($this->getFirstMedia('image'))->getFullUrl();
    }

    public function getImagePathAttribute()
    {
        return  optional($this->getFirstMediaPath('image'));
    }

    public function getRatingAttribute($value)
    {
        return json_decode($value);
    }

    public function element()
    {
        return $this->belongsTo(Element::class);
    }

    /*public function setRatingAttribute($value)
    {
        $this->attributes['rating'] = $value ?? json_encode([
            'ss_requirements' => 0,
            'po_condition' => 0,
            'fs_performance' => 0,
            'ee_performance' => 0,

        ]);
    }*/
}
