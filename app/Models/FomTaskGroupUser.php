<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class FomTaskGroupUser extends Model
{

    use LogsActivity;

    protected static $logUnguarded = true;

    protected static $logOnlyDirty = true;

    protected static $logName = 'fom task group users';

    protected static $submitEmptyLogs = false;

    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
