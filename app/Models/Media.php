<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

class Media extends BaseMedia
{
    protected $appends = ['full_url','title'];

    public function downloader()
    {
        $this->hasMany(User::class);
    }

    public function getFullUrlAttribute()
    {
        return $this->getFullUrl();
    }

    public function getPathAttribute()
    {
        return $this->getPath();
    }

    public function getTitleAttribute()
    {
        return $this->hasCustomProperty('title') ? $this->getCustomProperty('title') : '';
    }

    public function setTitleAttribute($value)
    {
        return $this->setCustomProperty('title',$value);
    }
}
