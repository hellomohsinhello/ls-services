<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomMaterialDeliveryPermit extends Model
{
    protected $guarded = [];

    public function getVDeliveryDaysAttribute($val)
    {
        return json_decode($val);
    }

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('materialDeliveryPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
