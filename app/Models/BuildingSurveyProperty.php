<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;

class BuildingSurveyProperty extends Model
{

    protected $withCount = ['defects'];

    protected $guarded = [];

    public function assignment()
    {
        return $this->belongsTo(BuildingSurveyAssignment::class,'assignment_id');
    }

    public function template()
    {
        return $this->belongsTo(Template::class,'template_id');
    }

    public function surveyor()
    {
        return $this->belongsTo(User::class,'surveyor_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class,'area_id');
    }

    public function defects()
    {
        return $this->hasMany(Defect::class,'property_id');
    }
}
