<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomWasteDisposalPermit extends Model
{


    protected $guarded = [];

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('wasteDisposalPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
