<?php

namespace App\Models;

use App\Laravue\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BuildingSurveyAssignment extends Model
{
    protected $guarded = [];

    protected $appends = ['due_date_data','status_color'];

    protected $withCount = ['properties','defects'];

    public function properties()
    {
        return $this->hasMany(BuildingSurveyProperty::class,'assignment_id');
    }

    public function team()
    {
        return $this->belongsToMany(User::class,'building_survey_teams','assignment_id','user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class,'manager_id');
    }

    public function client_contact()
    {
        return $this->belongsTo(User::class,'client_contact_id');
    }

    public function getStatusColorAttribute()
    {
        $color = '';
        switch ($this->status){
            case 'New':
                $color = '#13ce66';
                break;
            case 'Assigned':
                $color = '#E91E63';
                break;
            case 'Pending Inspection':
                $color = '#9C27B0';
                break;
            case 'Completed':
                $color = '#9C27B1';
                break;
            case 'Cancelled':
                $color = '#FFC107';
                break;
            case 'On Hold':
                $color = '#CDDC39';
                break;
            case 'Finalized':
                $color = '#CDDC36';
                break;
            case 'Approved':
                $color = '#CDDC37';
                break;
        }

        return $color;
    }

    public function getDueDateDataAttribute()
    {
        $days_left = now()->diffInDays($this->due_date,false);

        if ($days_left == 0 or $days_left == 1){
            $type = 'warning';
        }elseif ($days_left > 2 and $days_left <= 7){
            $type = 'success';
        }elseif ($days_left >= 8){
            $type = 'info';
        }else{
            $type = 'danger';
        }


        return [
            'type' => $type,
            'text' => str_plural('Day', $days_left) .'('. $days_left .')',
            'tooltip' => 'Due Date: '.$this->due_date,
            'date' => Carbon::parse($this->due_date)->format('d-m-Y'),
        ];
    }

    public function defects()
    {
        return $this->hasManyThrough(Defect::class,BuildingSurveyProperty::class,'assignment_id','property_id');
    }
}
