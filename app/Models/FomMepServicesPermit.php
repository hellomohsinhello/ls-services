<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomMepServicesPermit extends Model
{
    protected $guarded = [];

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('mepServicesPermit.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
