<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FomDisciplinaryActionIssued extends Model
{
    protected $guarded = [];

    protected $appends = ['download_url'];

    public function getDownloadUrlAttribute()
    {
        return route('disciplinaryActionIssued.download',$this->id);
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
