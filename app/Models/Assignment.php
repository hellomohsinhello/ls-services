<?php

namespace App\Models;

use App\Laravue\Models\User;
use Illuminate\Database\Eloquent\Model;
use function Sodium\crypto_box_publickey_from_secretkey;

class Assignment extends Model
{

    protected $appends = ['remaining_days','due_date_data', 'status_color'];

    protected $withCount = ['properties'];

    protected $guarded = [];

    public function getRemainingDaysAttribute()
    {
        return now()->diffInDays($this->due_date);
    }

    public function getDueDateDataAttribute()
    {
        $days_left = now()->diffInDays($this->due_date,false);

        if ($days_left == 0 or $days_left == 1){
            $type = 'warning';
        }elseif ($days_left > 2 and $days_left <= 7){
            $type = 'success';
        }elseif ($days_left >= 8){
            $type = 'info';
        }else{
            $type = 'danger';
        }


        return [
            'type' => $type,
            'text' => str_plural('Day', $days_left) .'('. $days_left .')',
            'tooltip' => 'Due Date: '.$this->due_date,
        ];
    }


    public function contact()
    {
        return $this->belongsTo(User::class,'client_contact_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class,'manager_id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function portfolio()
    {
        return $this->hasOne(Portfolio::class);
    }


    public function getStatusColorAttribute()
    {
        $color = '';
        switch ($this->status){
            case 'New':
                $color = '#13ce66';
                break;
            case 'Assigned':
                $color = '#E91E63';
                break;
            case 'Pending Inspection':
                $color = '#9C27B0';
                break;
            case 'Completed':
                $color = '#9C27B1';
                break;
            case 'Cancelled':
                $color = '#FFC107';
                break;
            case 'On Hold':
                $color = '#CDDC39';
                break;
            case 'Finalized':
                $color = '#CDDC36';
                break;
            case 'Approved':
                $color = '#CDDC37';
                break;
        }

        return $color;
    }
}
