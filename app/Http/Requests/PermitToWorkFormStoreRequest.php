<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermitToWorkFormStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_contractors' =>'required',
            'approvals' => 'required',
//            'work_permit_number' => 'required',
//            'owner_name' => 'required',
//            'tenant_name' => 'required',
            'company_name' => 'required',
//            'contractor_name' => 'required',
            'site_supervisor_name' => 'required',
            'site_supervisor_contact_number' => 'required',
//            'work_permit_validity' => 'required',
            'type_of_application' => 'required',
            'design_approvals' => 'required',
//            'electricity_comment' => 'required',
//            'noisy_non_noisy_works_comment' => 'required',
//            'insurance_comment' => 'required',
        ];
    }
}
