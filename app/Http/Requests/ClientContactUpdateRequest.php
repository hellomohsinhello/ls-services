<?php

namespace App\Http\Requests;

use App\Models\ClientContact;
use Illuminate\Foundation\Http\FormRequest;

class ClientContactUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salutation' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'designation' => 'required',
            'client' => 'required',
            'email' => "required|email|unique:client_contacts,email,".request('id'),
            'area' => 'required',
            'password' => 'nullable|confirmed',
        ];
    }
}
