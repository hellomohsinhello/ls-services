<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Monolog\Handler\IFTTTHandler;

class AssignmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request('is_portfolio')){
            return [
                'title' => 'required',
                'due_date' => 'required|date',
                'manager' => 'required',
                'service' => 'required',
                'client_ref' => 'required',
                'report_lang' => 'required',
                'client_contact' => 'required',
                'hide_valuation' => 'required',
                'template_type' => 'required',
                'report_title' => 'required',
                'applicant_name' => 'required',
                'introduction' => 'required',
                'brief_description' => 'required',
                'address' => 'required',
                'purpose' => 'required',
                'tenure_status' => 'required',
                'valuation_date' => 'required|date',
                'final_notes' => 'required',
                'instruction' => 'required',
                'methodology' => 'required',
                'valuation_basis' => 'required',
                'forced_sale' => 'required',
                'is_page_signature' => 'required',
                'valuation_approach' => 'required',
                'valuation_calculation_summary' => 'required',
                'principal_risks' => 'required',
                'is_page_signature' => 'required',
                'general_market_commentary' => 'required',
                'special_assumptions_appendices_b' => 'required',
                'general_market_commentary_2' => 'required'
            ];
        }

        return [
            'title' => 'required',
            'due_date' => 'required|date',
            'manager' => 'required',
            'service' => 'required',
            'client_ref' => 'required',
            'report_lang' => 'required',
            'client_contact' => 'required',
        ];
    }
}
