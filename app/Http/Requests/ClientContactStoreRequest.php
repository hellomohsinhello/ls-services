<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientContactStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salutation' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'designation' => 'required',
            'designation_ar' => 'nullable',
            'password' => 'required|confirmed',
            'phone_number' => 'required',
            'mobile_number' => 'required',
            'email' => 'required|email|unique:client_contacts,email',
            'area' => 'required',
            'client' => 'required',
        ];
    }
}
