<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'client_type' => 'required|integer',
//            'email' => "required|email|unique:clients,email,".request('id'),
            'fullname' => 'required',
//            'area' => 'required',
//            'lastname' => 'required',
//            'password' => 'nullable|confirmed',
        ];
    }
}
