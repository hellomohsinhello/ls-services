<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FomProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'area' => 'required',
            'contractor' => 'required',
            'owner' => 'required',
            'tenant' => 'required',
            'building' => 'required',
            'type' => 'required',
            'units' => 'required',
            'levels' => 'required',
            'oa_email' => 'required|email',
            'building_management_email' => 'required|email',
        ];
    }
}
