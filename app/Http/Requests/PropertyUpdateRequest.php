<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'template' => 'required',
            'area' => 'required',
            'inspector' => 'required',
            'name' => 'required',
            'plot_number' => 'required',
            'property_number' => 'required',
            'inspection_type' => 'required',
            'property_type' => 'required'
        ];
    }

}
