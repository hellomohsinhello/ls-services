<?php

namespace App\Http\Resources;

use App\Laravue\Acl;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $permisions = array_map(function ($permission) {
            return $permission['name'];
        }, $this->getAllPermissions()->toArray());


//        can view fit out management menu.
        if ($this->hasFomProjects() or $this->hasAnyRole([
                Acl::ROLE_ADMIN,
                Acl::ROLE_FOM_HEAD,
                Acl::ROLE_FOM_MANAGER,
//                Acl::ROLE_FOM_ADMIN,
            ])){

            $permission = 'view menu fit-out-management';
            array_push($permisions,$permission);
        }


//        can view building inspection
        if ($this->hasBsProjects() or $this->hasAnyRole([
                Acl::ROLE_ADMIN,
                Acl::ROLE_BS_HEAD,
                Acl::ROLE_BS_MANAGER,
                Acl::ROLE_BS_ADMIN,
            ])){

            $permission = 'view menu building inspection';
            array_push($permisions,$permission);
        }

        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'name' => $this->firstname,
            'email' => $this->email,
            'client_id' => $this->client_id,
            'roles' => array_map(function ($role) {
                return $role['name'];
            }, $this->roles->toArray()),
            'role_ids' => array_map(function ($role) {
                return $role['id'];
            }, $this->roles->toArray()),
            'userable' => $this->userable,
            'permissions' => $permisions,
            'avatar' => $this->avatar,
            'signature' => $this->signature,
            'qualification' => $this->qualification,
            'professional_membership_number' => $this->professional_membership_number,
            'position' => $this->position,
            'userable_type' => $this->userable_type,
        ];
    }
}
