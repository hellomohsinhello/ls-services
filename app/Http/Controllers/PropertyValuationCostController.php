<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\PropertyValuationCost;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationCostController extends Controller
{


    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationCost::query();
        $property = Arr::get($searchParams, 'property_id', '');
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','asc')
                ->with([])->paginate($limit)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cost = PropertyValuationCost::create([
            'property_id' => \request('property_id'),
            'description' => \request('description'),
            'type' => \request('type'),
            'area_length' =>  \request('area_length') ?? 0,
            'rate_area' => 0,
            'age' => 0,
            'life_expectancy' => 0,
            'apply_profit' => 0,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationCost  $propertyValuationCost
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationCost $propertyValuationCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationCost  $propertyValuationCost
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationCost $propertyValuationCost)
    {
        //
    }

    public function update()
    {


        $costs = \request('costs');

        foreach ($costs as $cost){

            $DBcost = PropertyValuationCost::find($cost['id']);

            $DBcost->description = $cost['description'];
            $DBcost->area_length = $cost['area_length'];
            $DBcost->rate_area = $cost['rate_area'];
            $DBcost->age = $cost['age'];
            $DBcost->life_expectancy = $cost['life_expectancy'];
            $DBcost->apply_profit = $cost['apply_profit'];
            $DBcost->total_cost = $cost['total_cost'];


            $DBcost->save();

        }
    }


    public function destroy(PropertyValuationCost $propertyValuationCost)
    {
        $propertyValuationCost->delete();
    }
}
