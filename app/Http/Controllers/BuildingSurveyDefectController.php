<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Models\Defect;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BuildingSurveyDefectController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Defect::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
//        $keyword = Arr::get($searchParams, 'keyword', '');
        $property_id = Arr::get($searchParams, 'property_id', '');
        $sorted = Arr::get($searchParams, 'sorted', '');

        if (!empty($property_id)) {
            $resourceQuery->where('property_id',  $property_id);
        }

        if (!empty($sorted)) {
            $resourceQuery->orderBy($sorted);
        }

        /*if (!empty($keyword)) {
            $resourceQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('plot_number', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('property_number', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('property_type', 'LIKE', '%' . $keyword . '%');
        }*/

        return new CommonResourceCollection($resourceQuery->paginate($limit));
    }


    public function update(Defect $building_survey_defect)
    {
        $building_survey_defect->number = \request('number');
        $building_survey_defect->location = \request('location');
        $building_survey_defect->element_id = \request('element');
        $building_survey_defect->inspection_type_id = \request('inspection_type');
        $building_survey_defect->description = \request('description');
        $building_survey_defect->recommendation = \request('recommendation');
        $building_survey_defect->liable_parties = \request('liable_parties');
        $building_survey_defect->status = \request('status');
        $building_survey_defect->unit = \request('unit');
        $building_survey_defect->quantity = \request('quantity');
        $building_survey_defect->rate = \request('rate');
        $building_survey_defect->nrm_code = \request('nrm_code');
        $building_survey_defect->conditions = \request('conditions');
        $building_survey_defect->component = \request('component');
        $building_survey_defect->poac = \request('poac');
        $building_survey_defect->rec = \request('rec');
        $building_survey_defect->rating = json_encode(\request('rating'));
        $building_survey_defect->save();

        return new CommonSingleResource($building_survey_defect);
    }

    public function sort()
    {
        $defects = collect(\request('defects'))->pluck('id','number');

        foreach ($defects as $key => $value){
            DB::table('defects')->where('id',$value)->update(['number'=> $key]);
        }
    }
}
