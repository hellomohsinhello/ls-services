<?php

namespace App\Http\Controllers;

use App\Http\Requests\WasteDisposalPermitStoreRequest;
use App\Http\Resources\FomWasteDisposalPermitSingleResource;
use App\Http\Resources\WasteDisposalPermitCollection;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomTask;
use App\Models\FomWasteDisposalPermit;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomWasteDisposalPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new WasteDisposalPermitCollection($fomTask->waste_disposal_permits);
    }

    public function store(WasteDisposalPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomWasteDisposalPermit::create([
            'date_and_time' => \request('date_and_time'),
            'permit_no' => \request('permit_no'),
            'co_company' => \request('co_company'),
            'co_address' => \request('co_address'),
            'c_primary_contact_name' => \request('c_primary_contact_name'),
            'c_title' => \request('c_title'),
            'c_address' => \request('c_address'),
            'c_city' => \request('c_city'),
            'c_contact_number' => \request('c_contact_number'),
            'c_email' => \request('c_email'),
            'w_generated_from' => \request('w_generated_from'),
            'w_by_whom' => \request('w_by_whom'),
            'w_describe_material' => \request('w_describe_material'),
            'w_quantity' => \request('w_quantity'),
            'w_how_stored' => \request('w_how_stored'),
            'w_disposal_method' => \request('w_disposal_method'),
            'w_disposal_location' => \request('w_disposal_location'),
            'w_comments' => \request('w_comments'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomWasteDisposalPermitSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomWasteDisposalPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomWasteDisposalPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.waste-disposal-permit',compact('form'));

        return $pdf->stream('waste-disposal-permit.pdf');

    }
}
