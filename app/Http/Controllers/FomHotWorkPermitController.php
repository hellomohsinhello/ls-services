<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomHotWorkPermitStoreRequest;
use App\Http\Resources\FomHotWorkPermitCollection;
use App\Http\Resources\FomHotWorkPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomHotWorkPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;


class FomHotWorkPermitController extends Controller
{

    public function index(FomTask $fomTask)
    {
        return new FomHotWorkPermitCollection($fomTask->hot_work_permits);
    }

    public function store(FomHotWorkPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomHotWorkPermit::create([
            'company_name' => \request('company_name'),
            'project' => \request('project'),
            'location' => \request('location'),
            'permit_receiver' => \request('permit_receiver'),
            'contact_mobile_number' => \request('contact_mobile_number'),
            'permit_holder' => \request('permit_holder'),
            'name_of_operative' => \request('name_of_operative'),
            'employee_number' => \request('employee_number'),
            'date_and_time' => \request('date_and_time.0'),
            'date_and_time_end' => \request('date_and_time.1'),
            's2_date_and_time' => \request('s2_date_and_time.0'),
            's2_date_and_time_end' => \request('s2_date_and_time.1'),
            'q1' => \request('q1'),
            'q2' => \request('q2'),
            'q3' => \request('q3'),
            'q4' => \request('q4'),
            'q5' => \request('q5'),
            'q6' => \request('q6'),
            'q7' => \request('q7'),
            'q8' => \request('q8'),
            'q9' => \request('q9'),
            'q10' => \request('q10'),
            'q11' => \request('q11'),
            'q12' => \request('q12'),
            'q13' => \request('q13'),
            'q14' => \request('q14'),
            'q15' => \request('q15'),
            'q16' => \request('q16'),
            'q17' => \request('q17'),
            'q18' => \request('q18'),
            'q19' => \request('q19'),
            'q20' => \request('q20'),
            'disposalmetods' => json_encode(\request('disposalmetods')),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomHotWorkPermitSingleResource($fomPermitToWorkForm);

    }

    public function statusUpdate(FomHotWorkPermit $permit)
    {

        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();

    }

    public function downlaod(FomHotWorkPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.hot-work-permit',compact('form'));

        return $pdf->stream('hot-work-permit.pdf');

    }

}
