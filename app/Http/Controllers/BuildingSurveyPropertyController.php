<?php

namespace App\Http\Controllers;

use App\Http\Requests\BuildingSurveyPropertyStore;
use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Laravue\Acl;
use App\Models\BuildingSurveyAssignment;
use App\Models\BuildingSurveyProperty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BuildingSurveyPropertyController extends Controller
{
    public function index(Request $request)
    {

//        user can access properties if the user has admin access or user is member of assignment team.

        if (!(auth()->user()->hasAnyRole([
                Acl::ROLE_ADMIN,
                Acl::ROLE_BS_HEAD,
                Acl::ROLE_BS_MANAGER
            ]) or BuildingSurveyAssignment::find(\request('assignment_id'))->team()->where('user_id',auth()->id())->exists())){

            abort(403);

        }


        $searchParams = $request->all();
        $resourceQuery = BuildingSurveyProperty::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $assignment_id = Arr::get($searchParams, 'assignment_id', '');

        if (!empty($assignment_id)) {
            $resourceQuery->where('assignment_id',  $assignment_id);
        }

        if (!empty($keyword)) {
            $resourceQuery->where('name', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('plot_number', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('property_number', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('property_type', 'LIKE', '%' . $keyword . '%');
        }

        return new CommonResourceCollection($resourceQuery->with(['template','surveyor','area'])->paginate($limit));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(BuildingSurveyPropertyStore $request)
    {
        $property = BuildingSurveyProperty::create([
            'template_id' => \request('template'),
            'area_id' => \request('area'),
            'surveyor_id' => \request('surveyor'),
            'name' => \request('name'),
            'plot_number' => \request('plot_number'),
            'property_number' => \request('property_number'),
            'property_type' => \request('property_type'),
            'assignment_id' => \request('assignment'),
        ]);

        return new CommonSingleResource($property);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BuildingSurveyProperty  $buildingSurveyProperty
     * @return \Illuminate\Http\Response
     */
    public function show(BuildingSurveyProperty $buildingSurveyProperty)
    {

        return new CommonSingleResource($buildingSurveyProperty->load(['template']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BuildingSurveyProperty  $buildingSurveyProperty
     * @return \Illuminate\Http\Response
     */
    public function edit(BuildingSurveyProperty $buildingSurveyProperty)
    {
        //
    }


    public function update(BuildingSurveyPropertyStore $request, BuildingSurveyProperty $buildingSurveyProperty)
    {
        $buildingSurveyProperty->template_id = \request('template');
        $buildingSurveyProperty->area_id = \request('area');
        $buildingSurveyProperty->surveyor_id = \request('surveyor');
        $buildingSurveyProperty->name = \request('name');
        $buildingSurveyProperty->plot_number = \request('plot_number');
        $buildingSurveyProperty->property_number = \request('property_number');
        $buildingSurveyProperty->property_type = \request('property_type');
        $buildingSurveyProperty->assignment_id = \request('assignment');
        $buildingSurveyProperty->save();

        return new CommonSingleResource($buildingSurveyProperty);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BuildingSurveyProperty  $buildingSurveyProperty
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuildingSurveyProperty $buildingSurveyProperty)
    {
        //
    }
}
