<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomDeliveryAccessPermitStoreRequest;
use App\Http\Resources\FomDeliveryAccessPermitCollection;
use App\Http\Resources\FomDeliveryAccessPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomDeliveryAccessPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;


class FomDeliveryAccessPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomDeliveryAccessPermitCollection($fomTask->delivery_access_permits);
    }

    public function store(FomDeliveryAccessPermitStoreRequest $request)
    {

        $resource = FomDeliveryAccessPermit::create([
            'co_company' => \request('co_company'),
            'co_ls_permit_no' => \request('co_ls_permit_no'),
            'co_address' => \request('co_address'),
            'c_primary_contact_name' => \request('c_primary_contact_name'),
            'c_title' => \request('c_title'),
            'c_address' => \request('c_address'),
            'c_city' => \request('c_city'),
            'c_fax_no' => \request('c_fax_no'),
            'c_phone_no' => \request('c_phone_no'),
            'c_email' => \request('c_email'),
            'd_make_model' => \request('d_make_model'),
            'd_vehicle_height' => \request('d_vehicle_height'),
            'd_vehicle_registration' => \request('d_vehicle_registration'),
            'd_vehicle_weight' => \request('d_vehicle_weight'),
            'd_drivers_name' => \request('d_drivers_name'),
            'd_drivers_license_number' => \request('d_drivers_license_number'),
            'd_insurance_validity_period' => \request('d_insurance_validity_period'),
            'd_license_plate_number' => \request('d_license_plate_number'),
            'v_delivery_date' => \request('v_delivery_date'),
            'v_unit' => \request('v_unit'),
            'v_description_of_material' => \request('v_description_of_material'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);


//        Mail::to($resource->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($resource->task));

        Notification::send($resource->task->task_group->receivers,new FomFillUpFormFilledNotification($resource->task));

        return new FomDeliveryAccessPermitSingleResource($resource);
    }

    public function statusUpdate(FomDeliveryAccessPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomDeliveryAccessPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.delivery-and-access-permit',compact('form'));

        return $pdf->stream('delivery-and-access-permit.pdf');

    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
