<?php

namespace App\Http\Controllers;

use App\Exports\Excel\DefectExport;
use App\Http\Requests\PropertyStoreRequest;
use App\Http\Requests\PropertyUpdateRequest;
use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\DefectResourceCollection;
use App\Http\Resources\PropertyResource;
use App\Http\Resources\PropertyResourceSingle;
use App\Models\BuildingSurveyProperty;
use App\Models\Defect;
use App\Models\Property;
use App\Models\Service;
use App\Services\WordDoc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Monolog\Handler\IFTTTHandler;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;

class PropertyController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Property::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $assignment_id = Arr::get($searchParams, 'assignment_id', '');

        if (!empty($assignment_id)) {
            $resourceQuery->where('assignment_id', $assignment_id);
        }
        if (!empty($keyword)) {
            $resourceQuery->where('name', 'like',"%$keyword%");
        }

        return new CommonResourceCollection($resourceQuery->with(['valuer','inspector','community','type','template'])->paginate($limit));

    }

    public function getDefects(Request $request, Property $property)
    {
//       $searchParams = $request->all();
        /*$resourceQuery = Defect::query();
       $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
       $keyword = Arr::get($searchParams, 'keyword', '');
       $property_id = Arr::get($searchParams, 'property_id', '');

       if (!empty($assignment_id)) {
           $resourceQuery->where('assignment_id', $property_id);
       }
       if (!empty($property_id)) {
           $resourceQuery->where('property_id', $property_id);
       }*/

        $searchParams = $request->all();

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        $defects = $property->defects();

        return new DefectResourceCollection($defects->orderBy('number')->get());

    }

    public function update(PropertyUpdateRequest $request, Property $property)
    {

        $property->template_id = \request('template');
        $property->area_id = \request('area');
        $property->valuer_id = \request('valuer');
        $property->name = \request('name');
        $property->plot_number = \request('plot_number');
        $property->client_contact_phone = \request('client_contact_phone');
        $property->property_number = \request('property_number');
        $property->inspection_type_id = \request('inspection_type');
        $property->property_type_id = \request('property_type');
        $property->inspector_id = \request('inspector');
        $property->instruction_date = Carbon::parse(\request('instruction_date'));
        $property->current_owner = \request('current_owner');
        $property->applicant_name = \request('applicant_name');
        $property->bua = \request('bua');
        $property->bua_unit = \request('bua_unit');
        $property->area = \request('area_num');
        $property->area_unit = \request('area_unit');
        $property->client_contact_id = \request('client_contact');
        $property->client_contact_person = \request('client_contact_person');
        $property->inspection_datetime = Carbon::parse(\request('inspection_datetime'));
        $property->save();

        update_properties_refs($property->assignment);

        return new PropertyResource($property);
    }

    public function show(Property $property)
    {
        return new PropertyResource($property->load(['template','comments']));
    }

    public function getReport(BuildingSurveyProperty $property)
    {

        $property = BuildingSurveyProperty::find(\request('property'));


        //filter results
        switch (\request('type')){
            case ('civil'):
                $defects = $property->defects()->where('inspection_type_id',1)->orderBy('number')->get();
                break;
            case ('developer-civil'):
                $defects = $property->defects()->where('inspection_type_id',1)->where('liable_parties','Developer')->orderBy('number')->get();
                break;
            case ('oa-fm-civil'):
                $defects = $property->defects()->where('inspection_type_id',1)->where('liable_parties','OA/FM')->orderBy('number')->get();
                break;
            case ('mep'):
                $defects = $property->defects()->where('inspection_type_id',2)->orderBy('number')->get();
                break;
            case ('developer-mep'):
                $defects = $property->defects()->where('inspection_type_id',2)->where('liable_parties','Developer')->orderBy('number')->get();
                break;
            case ('oa-fm-mep'):
                $defects = $property->defects()->where('inspection_type_id',2)->where('liable_parties','OA/FM')->orderBy('number')->get();
                break;
            default:
                $defects = $property->defects()->orderBy('number')->get();
                break;

        }


        //send web view report
        if (request()->get('format') == 'web-view'){
            return view('exports.excel.defects',[
                'property' => $property,
                'defects' => $defects,
            ]);
        }

        //send excel report
        if (request()->get('format') == 'ms-excel'){
            return Excel::download(new DefectExport($property,$defects),"ads.xlsx");
        }

        //send word report
        if (request()->get('format') == 'ms-word'){

            $settings = $property->template->settings;

            $document = new WordDoc();
            $document->addSection();

            $loop = 0;
            foreach ($defects as $defect){

                if ($loop % 4 == 0){

                    if ($loop % 4 == 0 && $loop != 0){
                        $document->pageBreak();
                    }
                    $document->addTable();
                    $document->addTR();

                    $document->addTH(500,'REF');
                    if($settings->location) $document->addTH(1300,'LOCATION');
                    if($settings->element) $document->addTH(1400,'ELEMENT');
                    if($settings->description) $document->addTH(2800,'DESCRIPTION');
                    if($settings->recommendation) $document->addTH(2800,'RECOMMENDATIONS');
                    if($settings->liable_parties) $document->addTH(1400,'LIABLE PARTIES');
                    if($settings->status) $document->addTH(1000,'STATUS');
                    if($settings->component) $document->addTH(1400,'COMPONENT');
                    if($settings->poac) $document->addTH(2800,'POAC');
                    if($settings->rec) $document->addTH(2800,'REC');
                    if($settings->unit) $document->addTH(1000,'UNIT');
                    if($settings->priority) $document->addTH(1000,'PRIORITY');
                    if($settings->quantity) $document->addTH(1000,'QTY');
                    if($settings->rate) $document->addTH(1000,'RATE');
                    if($settings->rate && $settings->quantity) $document->addTH(1000,'TOTAL');
                    if($settings->nrm_code) $document->addTH(1400,'NRM CODE');
                    if($settings->conditions) $document->addTH(1400,'CONDITIONS');
                    $document->addTH(2000,'DEFECT IMAGE');
                }

                $document->addTR();
                $document->addTD(500, $loop+1);
                if($settings->location) $document->addTD(1300, $defect->location);
                if($settings->element) $document->addTD(1400, optional($defect->element)->name);
                if($settings->description) $document->addTD(2800, $defect->description);
                if($settings->recommendation) $document->addTD(2800, $defect->recommendation);
                if($settings->liable_parties) $document->addTD(1400, $defect->liable_parties);
                if($settings->status) $document->addTD(1000, $defect->status);
                if($settings->component) $document->addTD(1400, $defect->component);
                if($settings->poac) $document->addTD(2800, $defect->poac);
                if($settings->rec) $document->addTD(2800, $defect->rec);
                if($settings->unit) $document->addTD(1000, $defect->unit);
                if($settings->priority) $document->addTD(1000, $defect->priority);
                if($settings->quantity) $document->addTD(1000, $defect->quantity);
                if($settings->rate) $document->addTD(1000, $defect->rate);
                if($settings->rate && $settings->quantity) $document->addTD(1000, $defect->rate * $defect->quantity);
                if($settings->nrm_code) $document->addTD(1400, $defect->nrm_code);
                if($settings->conditions) $document->addTD(1400, $defect->conditions);
                $document->addTDIMG(2000, $defect->image_path);

                $loop++;
            }
            $file = $document->saveFile($property->name);

            return response()->download($file);
        }

    }

    public function store(PropertyStoreRequest $request)
    {

        $property = Property::create([
            'template_id' => \request('template'),
            'area_id' => \request('area'),
            'valuer_id' => \request('valuer'),
            'name' => \request('name'),
            'plot_number' => \request('plot_number'),
            'client_contact_phone' => \request('client_contact_phone'),
            'client_contact_person' => \request('client_contact_person'),
            'property_number' => \request('property_number'),
            'inspection_type_id' => \request('inspection_type'),
            'property_type_id' => \request('property_type'),
            'assignment_id' => \request('assignment'),
            'inspector_id' => \request('inspector'),
            'inspection_datetime' => Carbon::parse(\request('instruction_date')),
            'current_owner' => \request('current_owner'),
            'applicant_name' => \request('applicant_name'),
            'bua' => \request('bua'),
            'bua_unit' => \request('bua_unit'),
            'area' => \request('area_num'),
            'area_unit' => \request('area_unit'),
            'client_contact_id' => \request('client_contact'),
            'other_3' => 'Electricity, Water, Telephone and Drainage',
            'other_15' => 'According to our knowledge, there are no Easements, Rights of Way on the subject land.',
            'other_4' => 'The valuer has relied upon information provided by the Client and/or the Client’s legal representative or other professional advisers relating to tenure, tenancies, rights of way, restrictive covenants and other relevant matters. The valuer has assumed that the information provided by the client is accurate.',
            'other_5' => 'The subject property was given a visual examination. These inspections and investigations were limited by the Valuer’s reasonable professional judgement to areas which were appropriate. The inspection was limited to those areas clearly visible at time of inspection, therefore no comment is made on any inaccessible or unexposed areas. The valuer has not arranged for testing of services (e.g. electrical).',
            'other_7' => 'Land Sterling confirms that the appointed Valuer has the necessary local knowledge, skill, and understanding to undertake the valuation completely. Land Sterling confirms that the Surveyor is in a position to provide an objective and unbiased valuation and is competent to undertake the valuation assignment.',
            'other_6' => 'Unknown',

        ]);

        update_properties_refs($property->assignment);

        return new PropertyResourceSingle($property);
    }

}
