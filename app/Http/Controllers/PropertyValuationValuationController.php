<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Models\Property;
use App\Models\PropertyValuationValuation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class PropertyValuationValuationController extends Controller
{

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationValuation::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $property = Arr::get($searchParams, 'property_id', '');

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        if (\request()->has('first')) {

            $resource = $resourceQuery->firstOrCreate(['property_id' => $property ]);

            return new CommonSingleResource(PropertyValuationValuation::find($resource->id));

        }else{

            return new CommonResourceCollection(
                $resourceQuery
                    ->orderBy('created_at','desc')
                    ->with([])->paginate($limit)
            );

        }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationValuation  $propertyValuationValuation
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationValuation $propertyValuationValuation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationValuation  $propertyValuationValuation
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationValuation $propertyValuationValuation)
    {
        //
    }

    public function update(Request $request, Property $propertyValuationValuation)
    {
        $attribute = \request('attribute');
        $value = \request('value');
        $is_date = \request('is_date');

        /*$request->request->add([$attribute => $value]);

        $validator = Validator::make($request->all(), [
            'inspection_tel' => 'required|max:2',
        ])->validate();*/

        if ($is_date){
            $propertyValuationValuation->{$attribute} = Carbon::parse($value);
        }else{
            $propertyValuationValuation->{$attribute} = $value;
        }

        $propertyValuationValuation->save();

        return [
            $attribute,
            $value,
            $is_date,
        ];


        /*$propertyValuationValuation->valuation_date = \request('valuation.valuation_date') ? Carbon::parse(\request('valuation.valuation_date')) : null;
        $propertyValuationValuation->purpose = \request('valuation.purpose');
        $propertyValuationValuation->developer = \request('valuation.developer');
        $propertyValuationValuation->basis = \request('valuation.basis');
        $propertyValuationValuation->interests = \request('valuation.interests');
        $propertyValuationValuation->tenure_status = \request('valuation.tenure_status');
        $propertyValuationValuation->methods = \request('valuation.methods');
        $propertyValuationValuation->round_by = \request('valuation.round_by');
        $propertyValuationValuation->primary_market_value = \request('valuation.primary_market_value');
        $propertyValuationValuation->forced_sale_value = \request('valuation.forced_sale_value');
        $propertyValuationValuation->fs_instead_sa = \request('valuation.fs_instead_sa');
        $propertyValuationValuation->valuation_rationale_1 = \request('valuation.valuation_rationale_1');
        $propertyValuationValuation->market_research = \request('valuation.market_research');
        $propertyValuationValuation->total_construction_cost = \request('valuation.total_construction_cost');
        $propertyValuationValuation->existing_market_value = \request('valuation.existing_market_value');
        $propertyValuationValuation->passing_rent = \request('valuation.passing_rent');
        $propertyValuationValuation->estimated_market_value = \request('valuation.estimated_market_value');
        $propertyValuationValuation->other_remarks = \request('valuation.other_remarks');
        $propertyValuationValuation->detailed_method = \request('valuation.detailed_method');
        $propertyValuationValuation->adj_name_1 = \request('valuation.adj_name_1');
        $propertyValuationValuation->adj_name_2 = \request('valuation.adj_name_2');
        $propertyValuationValuation->adj_name_3 = \request('valuation.adj_name_3');
        $propertyValuationValuation->adj_name_3 = \request('valuation.adj_name_3');
        $propertyValuationValuation->area_for_calculation = \request('valuation.area_for_calculation');
        $propertyValuationValuation->use_decimal_for_rate_area = \request('valuation.use_decimal_for_rate_area');
        $propertyValuationValuation->cost_method_dev_profit = \request('valuation.cost_method_dev_profit');
        $propertyValuationValuation->cost_method_uc_dev_profit_as_is = \request('valuation.cost_method_uc_dev_profit_as_is');
        $propertyValuationValuation->cost_method_uc_dev_profit_as_comp = \request('valuation.cost_method_uc_dev_profit_as_comp');
        $propertyValuationValuation->income_method_capitalisation_rate = \request('valuation.income_method_capitalisation_rate');
        $propertyValuationValuation->valuation_rationale_2 = \request('valuation.valuation_rationale_2');
        $propertyValuationValuation->primary_market_value_sub_option = \request('valuation.primary_market_value_sub_option');
        $propertyValuationValuation->marketing_period = \request('valuation.marketing_period');
        $propertyValuationValuation->assumptions = \request('valuation.assumptions');
        $propertyValuationValuation->special_assumptions = \request('valuation.special_assumptions');
        $propertyValuationValuation->report_cosigned_by_valuer = \request('valuation.report_cosigned_by_valuer');
        $propertyValuationValuation->approved_municipality_plans = \request('valuation.approved_municipality_plans');
        $propertyValuationValuation->market_commentary = \request('valuation.market_commentary');
        $propertyValuationValuation->appendix_title = \request('valuation.appendix_title');
        $propertyValuationValuation->appendix_content = \request('valuation.appendix_content');



        $propertyValuationValuation->save();

        return new CommonSingleResource($propertyValuationValuation);*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropertyValuationValuation  $propertyValuationValuation
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyValuationValuation $propertyValuationValuation)
    {
        //
    }
}
