<?php

namespace App\Http\Controllers;

use App\Http\Requests\DefectStoreRequest;
use App\Http\Requests\DefectUpdateRequest;
use App\Http\Resources\DefectResource;
use App\Models\BuildingSurveyProperty;
use App\Models\Defect;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DefectController extends Controller
{
    public function update(DefectUpdateRequest $request, Defect $defect)
    {

        $defect->number = \request('number');
        $defect->location = \request('location');
        $defect->element_id = \request('element');
        $defect->inspection_type_id = \request('inspection_type');
        $defect->description = \request('description');
        $defect->recommendation = \request('recommendation');
        $defect->liable_parties = \request('liable_parties');
        $defect->status = \request('status');
        $defect->unit = \request('unit');
        $defect->quantity = \request('quantity');
        $defect->rate = \request('rate');
        $defect->nrm_code = \request('nrm_code');
        $defect->conditions = \request('conditions');
        $defect->component = \request('component');
        $defect->poac = \request('poac');
        $defect->rec = \request('rec');
        $defect->rating = json_encode(\request('rating'));
        $defect->save();

        return new DefectResource($defect);
    }


    public function create(DefectStoreRequest $request)
    {
        $property = BuildingSurveyProperty::find(\request('property'));

       /* if ($property->inspection_type_id != 1 or $property->inspection_type_id != 2){
            $property->inspection_type_id = 1;
            $property->save();
        }*/

        $defect = new Defect();

        $defect->property_id = \request('property');
//        $defect->inspection_type_id = $property->inspection_type_id;
        $defect->rating = json_encode([
            'ss_requirements' => 0,
            'po_condition' => 0,
            'fs_performance' => 0,
            'ee_performance' => 0,

        ]);

        $defect->save();

        $defect->addMediaFromRequest('file')->toMediaCollection('image');

        return new DefectResource($defect);
    }

    public function destroy(Defect $defect)
    {
        $defect->delete();
    }

    public function sort()
    {
        $defects = collect(\request('defects'))->pluck('id','number');

        foreach ($defects as $key => $value){
            DB::table('defects')->where('id',$value)->update(['number'=> $key]);
        }
    }
}
