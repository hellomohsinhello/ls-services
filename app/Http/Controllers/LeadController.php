<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LeadController extends Controller
{

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Lead::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $type = Arr::get($searchParams, 'type', '');

        if (!empty($keyword)) {
            $resourceQuery->where('type', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('source', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('client_name', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('community', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('floor_breakdown', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('ls_ref', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('request_service', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('property_name', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('contact_name', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('mobile', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('telephone', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('email', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($type)) {
            $resourceQuery->where('type',$type);
        }

        return new CommonResourceCollection($resourceQuery->orderBy('created_at','desc')->paginate($limit));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }
}
