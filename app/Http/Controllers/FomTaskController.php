<?php

namespace App\Http\Controllers;

use App\Laravue\Models\User;
use App\Models\FomTask;
use App\Models\MediaDownload;
use Illuminate\Support\Facades\Notification;
use App\Notifications\FomTaskFileDownloadedNotification;

class FomTaskController extends Controller
{
    public function downloadFile(FomTask $fomTask,User $user)
    {

        $media = $fomTask->getFirstMedia('file');

        $download = MediaDownload::firstOrCreate([
            'user_id' => $user->id,
            'media_id' => $media->id,
            'downloaded' => true
        ]);


        Notification::send($fomTask->task_group->users, new FomTaskFileDownloadedNotification($fomTask,$user));

        return response()->download($media->getPath(), $media->file_name);
    }
}
