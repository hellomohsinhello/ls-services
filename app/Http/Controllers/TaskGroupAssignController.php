<?php

namespace App\Http\Controllers;

use App\Mail\FomTaskGroupAssigned;
use App\Models\FomTaskGroup;
use App\Notifications\FomTaskGroupAssignedNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class TaskGroupAssignController extends Controller
{
    public function update()
    {
        $fomTaskGroup = FomTaskGroup::find(\request('id'));
        $fomTaskGroup->due_date = Carbon::parse(\request()->get('due_date'));
        $fomTaskGroup->assigned = \request()->get('assigned');
        $fomTaskGroup->save();

        if (\request('notifications') and $fomTaskGroup->isAssigned()){
            Notification::send($fomTaskGroup->assign_alert_users(), new FomTaskGroupAssignedNotification($fomTaskGroup));
        }

        if (\request('notifications') and $fomTaskGroup->isAssigned() and $fomTaskGroup->oa){
            Mail::to($fomTaskGroup->project->oa_email)->send(new FomTaskGroupAssigned($fomTaskGroup));
        }

        if (\request('notifications') and $fomTaskGroup->isAssigned() and $fomTaskGroup->building_management){
            Mail::to($fomTaskGroup->project->building_management_email)->send(new FomTaskGroupAssigned($fomTaskGroup));
        }

        return $fomTaskGroup->load('setting');
    }
}







