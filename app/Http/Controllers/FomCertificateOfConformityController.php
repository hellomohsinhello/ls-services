<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomCertificateOfConformityStoreRequest;
use App\Http\Resources\FomCertificateOfConformityCollection;
use App\Http\Resources\FomCertificateOfConformitySingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomCertificateOfConformity;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomCertificateOfConformityController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomCertificateOfConformityCollection($fomTask->certificate_of_conformities);
    }

    public function store(FomCertificateOfConformityStoreRequest $request)
    {

        $fomPermitToWorkForm = FomCertificateOfConformity::create([
            'date_of_submission' => \request('date_of_submission'),
            'name_of_tenant' => \request('name_of_tenant'),
            'tower' => \request('tower'),
            'file_no' => \request('file_no'),
            't_contact_person' => \request('t_contact_person'),
            't_office_tel_no' => \request('t_office_tel_no'),
            't_title_position' => \request('t_title_position'),
            't_mobile_no' => \request('t_mobile_no'),
            't_address' => \request('t_address'),
            't_email' => \request('t_email'),
            'c_title_position' => \request('c_title_position'),
            'c_mobile_no' => \request('c_mobile_no'),
            'c_address' => \request('c_address'),
            'c_email' => \request('c_email'),
            'f_contact_person' => \request('f_contact_person'),
            'f_office_tel_no' => \request('f_office_tel_no'),
            'f_title_position' => \request('f_title_position'),
            'f_mobile_no' => \request('f_mobile_no'),
            'question_1' => \request('question_1'),
            'question_2' => \request('question_2'),
            'question_3' => \request('question_3'),
            'question_4' => \request('question_4'),
            'question_5' => \request('question_5'),
            'question_6' => \request('question_6'),
            'question_7' => \request('question_7'),
            'question_8' => \request('question_8'),
            'question_9' => \request('question_9'),
            'question_10' => \request('question_10'),
            'question_11' => \request('question_11'),
            'question_12' => \request('question_12'),
            'question_13' => \request('question_13'),
            'question_14' => \request('question_14'),
            'question_15' => \request('question_15'),
            'question_16' => \request('question_16'),
            'question_17' => \request('question_17'),
            'question_18' => \request('question_18'),
            'question_19' => \request('question_19'),
            'question_20' => \request('question_20'),
            'question_21' => \request('question_21'),
            'question_22' => \request('question_22'),
            'question_23' => \request('question_23'),
            'question_24' => \request('question_24'),
            'question_25' => \request('question_25'),
            'question_26' => \request('question_26'),
            'question_27' => \request('question_27'),
            'question_28' => \request('question_28'),
            'question_29' => \request('question_29'),
            'question_30' => \request('question_30'),
            'a_contracotrs' => \request('a_contracotrs'),
            'a_titleposition' => \request('a_titleposition'),
            'cc_name' => \request('cc_name'),
            'cc_title_position' => \request('cc_title_position'),
            'cc_additional_comments' => \request('cc_additional_comments'),
            'cc_name_of_inspector' => \request('cc_name_of_inspector'),
            'cc_inspection_date' => \request('cc_inspection_date'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);


//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));
        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomCertificateOfConformitySingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomCertificateOfConformity $dilapidation)
    {
        $dilapidation->status = \request('status');
        $dilapidation->comment = \request('comment');
        $dilapidation->save();
    }

    public function downlaod(FomCertificateOfConformity $form)
    {

        $pdf = PDF::loadView('fom.fuf.certificate-of-conformity-form',compact('form'));

        return $pdf->stream('certificate-of-conformity-form.pdf');
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
