<?php

namespace App\Http\Controllers;

use App\Laravue\Acl;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use App\Models\Area;
use App\Models\BuildingSurveyAssignment;
use App\Models\Client;
use App\Models\InspectionType;
use App\Models\PropertyType;
use App\Models\Template;
use Illuminate\Http\Request;

class ResourceSelectionController extends Controller
{


    public function get($resource)
    {
        $query = $this->getModelQuery($resource);

        return $query->get();
    }

    public function getModelQuery($resource)
    {

        $query = null;

        switch ($resource){
            case 'users':
                $query = User::query();
                break;

            case 'building-survey-assignment-team':
                $assignment = BuildingSurveyAssignment::find(\request('assignment'));
                $query = $assignment->team();
                break;

            case 'building-survey-templates':
                $query = Template::where('service_id',1);
                break;

            case 'property-valuations-templates':
                $query = Template::where('service_id',2);
                break;

            case 'client-contact-users':
                $query = User::role(Acl::ROLE_CLIENT_CONTACT);
                break;

            case 'inspection-types':
                $query = InspectionType::where('service_id', \request('service'));
                break;

            case 'properties-types':
                $query = PropertyType::query();
                break;

            case 'areas':
                $query =  Area::query();
                break;

            case 'roles':
                $query =  Role::query();
                break;

            case 'clients':
                $query =  Client::query();
                break;
        }

        return $query;

    }

}
