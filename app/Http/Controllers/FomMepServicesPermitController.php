<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomMepServicesPermitStoreRequest;
use App\Http\Resources\FomMepServicesPermitCollection;
use App\Http\Resources\FomMepServicesPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomMepServicesPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomMepServicesPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomMepServicesPermitCollection($fomTask->mep_services_permits);
    }

    public function store(FomMepServicesPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomMepServicesPermit::create([
            'location_and_details' => \request('location_and_details'),
            'contractor_name' => \request('contractor_name'),
            'contractor_contact_no' => \request('contractor_contact_no'),
            'site_supervisor_name' => \request('site_supervisor_name'),
            'site_supervisor_contact_no' => \request('site_supervisor_contact_no'),
            'issuance_date' => \request('issuance_date'),
            'validity_date' => \request('validity_date'),
            'witnessing_request_1' => \request('witnessing_request_1'),
            'witnessing_request_2' => \request('witnessing_request_2'),
            'witnessing_request_3' => \request('witnessing_request_3'),
            'witnessing_request_4' => \request('witnessing_request_4'),
            'witnessing_request_5' => \request('witnessing_request_5'),
            'witnessing_request_6' => \request('witnessing_request_6'),
            'witnessing_request_7' => \request('witnessing_request_7'),
            'witnessing_request_8' => \request('witnessing_request_8'),
            'witnessing_request_9' => \request('witnessing_request_9'),
            'building_systems_request_1' => \request('building_systems_request_1'),
            'building_systems_request_2' => \request('building_systems_request_2'),
            'building_systems_request_3' => \request('building_systems_request_3'),
            'building_systems_request_4' => \request('building_systems_request_4'),
            'building_systems_request_5' => \request('building_systems_request_5'),
            'building_systems_request_6' => \request('building_systems_request_6'),
            'building_systems_request_7' => \request('building_systems_request_7'),
            'building_systems_request_8' => \request('building_systems_request_8'),
            'building_systems_request_9' => \request('building_systems_request_9'),
            'building_systems_request_10' => \request('building_systems_request_10'),
            'building_systems_request_11' => \request('building_systems_request_11'),
            'building_systems_request_13' => \request('building_systems_request_13'),
            'building_systems_request_14' => \request('building_systems_request_14'),
            'bm_ls_comments' => \request('bm_ls_comments'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomMepServicesPermitSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomMepServicesPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomMepServicesPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.mep-services-permit',compact('form'));

        return $pdf->stream('mep-services-permit.pdf');

    }
}
