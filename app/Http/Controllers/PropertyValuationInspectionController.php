<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Models\PropertyValuationInspection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationInspectionController extends Controller
{

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationInspection::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $property = Arr::get($searchParams, 'property_id', '');

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        if (\request()->has('first')) {

            $resource = $resourceQuery->firstOrCreate(['property_id' => $property ]);

            return new CommonSingleResource(PropertyValuationInspection::find($resource->id));

        }else{

            return new CommonResourceCollection(
                $resourceQuery
                    ->orderBy('created_at','desc')
                    ->with([])->paginate($limit)
            );
        }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationInspection  $propertyValuationInspection
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationInspection $propertyValuationInspection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationInspection  $propertyValuationInspection
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationInspection $propertyValuationInspection)
    {
        //
    }


    public function update(Request $request, PropertyValuationInspection $propertyValuationInspection)
    {
        $propertyValuationInspection->Inspection_date_time = \request('inspection.inspection_date_time') ? Carbon::parse(\request('inspection.inspection_date_time')) : null;
        $propertyValuationInspection->inspection_tel = \request('inspection.inspection_tel');
        $propertyValuationInspection->gps = \request('inspection.gps');
        $propertyValuationInspection->address_old = \request('inspection.address_old');
        $propertyValuationInspection->address_new = \request('inspection.address_new');
        $propertyValuationInspection->location = \request('inspection.location');
        $propertyValuationInspection->parking = \request('inspection.parking');
        $propertyValuationInspection->road_surface = \request('inspection.road_surface');
        $propertyValuationInspection->boundaries = \request('inspection.boundaries');
        $propertyValuationInspection->leveling = \request('inspection.leveling');
        $propertyValuationInspection->age_years = \request('inspection.age_years');
        $propertyValuationInspection->no_of_units = \request('inspection.no_of_units');
        $propertyValuationInspection->property_status = \request('inspection.property_status');
        $propertyValuationInspection->life_years = \request('inspection.life_years');
        $propertyValuationInspection->property_condition = \request('inspection.property_condition');
        $propertyValuationInspection->lift = \request('inspection.lift');
        $propertyValuationInspection->other_details = \request('inspection.other_details');
        $propertyValuationInspection->development = \request('inspection.development');
        $propertyValuationInspection->unit_type = \request('inspection.unit_type');
        $propertyValuationInspection->occupancy = \request('inspection.occupancy');
        $propertyValuationInspection->boundary_walls = \request('inspection.boundary_walls');
        $propertyValuationInspection->cladding = \request('inspection.cladding');
        $propertyValuationInspection->doors = \request('inspection.doors');
        $propertyValuationInspection->yard_coverings = \request('inspection.yard_coverings');
        $propertyValuationInspection->windows = \request('inspection.windows');
        $propertyValuationInspection->facilities = \request('inspection.facilities');
        $propertyValuationInspection->structures = \request('inspection.structures');
        $propertyValuationInspection->internally = \request('inspection.internally');
        $propertyValuationInspection->finishing = \request('inspection.finishing');
        $propertyValuationInspection->air_conditioning = \request('inspection.air_conditioning');
        $propertyValuationInspection->view = \request('inspection.view');
        $propertyValuationInspection->internally = \request('internally');
        $propertyValuationInspection->other_1 = \request('inspection.other_1');
        $propertyValuationInspection->other_2 = \request('inspection.other_2');
        $propertyValuationInspection->other_3 = \request('inspection.other_3');
        $propertyValuationInspection->other_4 = \request('inspection.other_4');
        $propertyValuationInspection->other_5 = \request('inspection.other_5');
        $propertyValuationInspection->other_6 = \request('inspection.other_6');
        $propertyValuationInspection->other_7 = \request('inspection.other_7');
        $propertyValuationInspection->other_8 = \request('inspection.other_8');
        $propertyValuationInspection->other_9 = \request('inspection.other_9');
        $propertyValuationInspection->other_10 = \request('inspection.other_10');
        $propertyValuationInspection->water_marked_images = \request('inspection.water_marked_images');
        $propertyValuationInspection->include_photos = \request('inspection.include_photos');
        $propertyValuationInspection->photos_page_title = \request('inspection.photos_page_title');
        $propertyValuationInspection->surroundings = \request('inspection.surroundings');

        $propertyValuationInspection->save();

        return new CommonSingleResource($propertyValuationInspection);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropertyValuationInspection  $propertyValuationInspection
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyValuationInspection $propertyValuationInspection)
    {
        //
    }
}
