<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Mail\FomTaskNotificationMessage;
use App\Models\Comment;
use App\Models\FomProject;
use App\Models\FomTask;
use App\Notifications\FomTaskNotificationMessageNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class CommentController extends Controller
{
    public function getByProject(FomProject $fomProject)
    {
        return $fomProject->load('comments');
    }

    public function store(Request $request, CommentStoreRequest $commentStoreRequest)
    {

        switch ($request->commentable_type){
            case 'fom_project':
                $model = FomProject::findOrFail($request->commentable_id);
                break;
            case 'fom_task':
                $model = FomTask::findOrFail($request->commentable_id);
                break;
            case 'fom_task_notified':
                $model = FomTask::findOrFail($request->commentable_id);
                $receivers =  $model->task_group->receivers->filter(function ($user) {
                    return !$user->isMe();
                });
                Notification::send($receivers, new FomTaskNotificationMessageNotification($model));
                break;
        }

        $comment = new Comment();
        $comment->commenter()->associate(auth()->user());
        $comment->commentable()->associate($model);
        $comment->comment = $commentStoreRequest->comment;
        $comment->save();

        return $comment;
    }

    public function destroy(Comment $comment)
    {

        $comment->delete();

        return [];
    }

}
