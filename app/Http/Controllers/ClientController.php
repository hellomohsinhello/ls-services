<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Http\Resources\ClientContactResource;
use App\Http\Resources\ClientResource;
use App\Http\Resources\ClientSingleResource;
use App\Http\Resources\ClientTypeResource;
use App\Laravue\Acl;
use App\Models\Client;
use App\Models\ClientType;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $userQuery = Client::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $userQuery->where('fullname', 'LIKE', '%' . $keyword . '%');
//            $userQuery->OrWhere('lastname', 'LIKE', '%' . $keyword . '%');
//            $userQuery->OrWhere('email', 'LIKE', '%' . $keyword . '%');
        }

        /*if (!empty($keyword)) {
            $userQuery->whereHas('type', function($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', '%' . $keyword . '%');
            });
        }*/

        return new ClientResource($userQuery->paginate($limit));
    }

    public function types(Request $request)
    {

        $types = ClientType::all();

        return new ClientTypeResource($types);
    }

    public function store(ClientStoreRequest $request)
    {
        $client = Client::create([
            'fullname' => \request('fullname'),
//            'lastname' => \request('lastname'),
//            'email' => \request('email'),
//            'client_type_id' => \request('client_type'),
//            'area_id' => \request('area'),
//            'password' => Hash::make(\request('password')),
        ]);

        return new ClientSingleResource($client);
    }

    public function update(ClientUpdateRequest $request, Client $client)
    {

        $client->fullname = \request('fullname');
//        $client->lastname = \request('lastname');
//        $client->email = \request('email');
//        $client->client_type_id = \request('client_type');
//        $client->area_id = \request('area');

        /*if ($request->has('password')){
            $client->password = Hash::make(\request('password'));
        }*/

        $client->save();

        return new ClientSingleResource($client);

    }

    public function clientContacts(Client $client)
    {
        $contacts = $client->contacts();

        return new ClientContactResource($contacts->paginate(15));
    }

}
