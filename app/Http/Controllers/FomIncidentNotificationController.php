<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomDisciplinaryActionIssuedStoreRequest;
use App\Http\Resources\FomIncidentNotificationCollection;
use App\Http\Resources\FomIncidentNotificationSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomIncidentNotification;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomIncidentNotificationController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomIncidentNotificationCollection($fomTask->incident_notifications);
    }

    public function store(FomDisciplinaryActionIssuedStoreRequest $request)
    {

        $fomPermitToWorkForm = FomIncidentNotification::create([
            'related' => \request('related'),
            'if_not_why' => \request('if_not_why'),
            'near_miss' => \request('near_miss'),
            'affected' => \request('affected'),
            'near_misses' => json_encode(\request('near_misses')),
            'date_of_occurrence' => \request('date_of_occurrence'),
            'accident_description' => \request('accident_description'),
            'action' => \request('action'),
            'reporting_party_details' => \request('reporting_party_details'),
            'notification_to_company' => \request('notification_to_company'),
            'government_authorities' => \request('government_authorities'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));
        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomIncidentNotificationSingleResource($fomPermitToWorkForm);

    }

    public function statusUpdate(FomIncidentNotification $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomIncidentNotification $form)
    {

        $pdf = PDF::loadView('fom.fuf.incident-notification',compact('form'));

        return $pdf->stream('incident-notification.pdf');

    }
}
