<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Models\MapsLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MapsLocationController extends Controller
{



    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = MapsLocation::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $locationable_id = Arr::get($searchParams, 'locationable_id', '');
        $locationable_type = Arr::get($searchParams, 'locationable_type', '');

        if (!empty($locationable_id)) {
            $resourceQuery->where('locationable_id',  $locationable_id);
        }

        if (!empty($locationable_type)) {
            $resourceQuery->where('locationable_type',  $locationable_type);
        }

        if (\request()->has('first')) {

            $resource = $resourceQuery->firstOrCreate([
                'locationable_id' => $locationable_id,
                'locationable_type' => $locationable_type,
            ]);

            return new CommonSingleResource(MapsLocation::find($resource->id));

        }else{

            return new CommonResourceCollection(
                $resourceQuery
                    ->orderBy('created_at','desc')
                    ->with([])->paginate($limit)
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MapsLocation  $mapsLocation
     * @return \Illuminate\Http\Response
     */
    public function show(MapsLocation $mapsLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MapsLocation  $mapsLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(MapsLocation $mapsLocation)
    {
        //
    }


    public function update(Request $request, MapsLocation $mapsLocation)
    {
        $mapsLocation->lat = \request('lat');
        $mapsLocation->lng = \request('lng');
        $mapsLocation->position_lat = \request('lat');
        $mapsLocation->position_lng = \request('lng');
        $mapsLocation->zoom = \request('zoom');
        $mapsLocation->polygon = \request('paths');
        $mapsLocation->save();


        $mapsLocation
            ->addMediaFromUrl($mapsLocation->markerImgUrl())
            ->toMediaCollection('maker');

        $mapsLocation
            ->addMediaFromUrl($mapsLocation->polygonImgUrl())
            ->toMediaCollection('polygon');

        return new CommonSingleResource($mapsLocation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MapsLocation  $mapsLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(MapsLocation $mapsLocation)
    {
        //
    }
}
