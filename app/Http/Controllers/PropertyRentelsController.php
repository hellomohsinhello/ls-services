<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\PropertyValuationRentel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyRentelsController extends Controller
{
    public function index(Request $request)
    {

        $searchParams = $request->all();
        $resourceQuery = PropertyValuationRentel::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        $property_id = Arr::get($searchParams, 'property_id', '');

        if (!empty($property_id)) {
            $resourceQuery->where('property_id',$property_id);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','desc')
                ->with([])->paginate($limit)
        );
    }

    public function store()
    {
        $rentels = \request('rentels');
        $property = \request('property');

        foreach ($rentels as $rentel){
            PropertyValuationRentel::create([
                'property_id' => $property,
                'type' => $rentel['type'],
                'size' => $rentel['size'],
                'market_rent' => $rentel['market_rent'],
                'rent_yearly' => $rentel['rent_yearly'],
                'transaction_type' => $rentel['transaction_type'],
                'source' => $rentel['source'],
                'date' => $rentel['date'],
                'details' => $rentel['details'],
                'neighbourhood_ar' => $rentel['neighbourhood_ar'],
                'details_ar' => $rentel['details_ar'],
                'source_details' => $rentel['source_details'],
                'unit_type' => $rentel['unit_type'],
                'report_id' => $rentel['report_id'],
                'sort_order' => $rentel['sort_order'],
                'neighbourhood' => $rentel['neighbourhood'],
                'transaction_type_ar' => $rentel['transaction_type_ar'],
                'building_type_ar' => $rentel['building_type_ar'],
                'rental_index_id' => $rentel['rental_index_id'],
                'unit_type_ar' => $rentel['unit_type_ar'],
                'rate_area' => $rentel['rate_area'],
            ]);

        }

        return $rentels;
    }

    public function update()
    {


        $rentels = \request('rentels');

        foreach ($rentels as $rentel){
            $DBrentel = PropertyValuationRentel::find($rentel['id']);

            $DBrentel->type = $rentel['type'];
            $DBrentel->size = $rentel['size'];
            $DBrentel->market_rent = $rentel['market_rent'];
            $DBrentel->rent_yearly = $rentel['rent_yearly'];
            $DBrentel->transaction_type = $rentel['transaction_type'];
            $DBrentel->source = $rentel['source'];
            $DBrentel->date = Carbon::parse($rentel['date'])->format('d-m-Y');
            $DBrentel->details = $rentel['details'];
            $DBrentel->neighbourhood_ar = $rentel['neighbourhood_ar'];
            $DBrentel->details_ar = $rentel['details_ar'];
            $DBrentel->source_details = $rentel['source_details'];
            $DBrentel->unit_type = $rentel['unit_type'];
            $DBrentel->report_id = $rentel['report_id'];
            $DBrentel->sort_order = $rentel['sort_order'];
            $DBrentel->neighbourhood = $rentel['neighbourhood'];
            $DBrentel->transaction_type_ar = $rentel['transaction_type_ar'];
            $DBrentel->building_type_ar = $rentel['building_type_ar'];
            $DBrentel->rental_index_id = $rentel['rental_index_id'];
            $DBrentel->unit_type_ar = $rentel['unit_type_ar'];
            $DBrentel->rate_area = $rentel['rate_area'];
            $DBrentel->save();
        }
    }

    public function destroy(PropertyValuationRentel $propertyRentel)
    {
        $propertyRentel->delete();
    }
}
