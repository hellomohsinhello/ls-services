<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientContactStoreRequest;
use App\Http\Requests\ClientContactUpdateRequest;
use App\Http\Resources\ClientContactResource;
use App\Http\Resources\ClientContactSingleResource;
use App\Http\Resources\ClientResource;
use App\Models\Client;
use App\Models\ClientContact;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class ClientContactController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = ClientContact::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('firstname', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->OrWhere('lastname', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->OrWhere('email', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->OrWhere('designation', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->OrWhere('mobile_number', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->OrWhere('phone_number', 'LIKE', '%' . $keyword . '%');
        }

        return new ClientContactResource($resourceQuery->with(['client','area','province'])->paginate($limit));
    }

    public function store(ClientContactStoreRequest $request)
    {

        $contact = ClientContact::create([
            'salutation' => \request('salutation'),
            'firstname' => \request('firstname'),
            'lastname' => \request('lastname'),
            'email' => \request('email'),
            'designation' => \request('designation'),
            'designation_ar' => \request('designation_ar'),
            'mobile_number' => \request('mobile_number'),
            'phone_number' => \request('phone_number'),
            'client_id' => \request('client'),
            'area_id' => \request('area'),
            'password' => Hash::make(\request('password')),
        ]);

        unset($contact->password);

        return new ClientContactSingleResource($contact);

    }


    public function update(ClientContactUpdateRequest $request, ClientContact $client_contact)
    {

        $client_contact->salutation = \request('salutation');
        $client_contact->firstname = \request('firstname');
        $client_contact->lastname = \request('lastname');
        $client_contact->email = \request('email');
        $client_contact->designation = \request('designation');
        $client_contact->designation_ar = \request('designation_ar');
        $client_contact->mobile_number = \request('mobile_number');
        $client_contact->phone_number = \request('phone_number');
        $client_contact->client_id = \request('client');
        $client_contact->area_id = \request('area');

        if ($request->has('password')){
            $client_contact->password = Hash::make(\request('password'));
        }

        $client_contact->save();

        return new ClientContactSingleResource($client_contact);

    }
}
