<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\PropertyValuationBreakdown;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationBreakdownController extends Controller
{

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationBreakdown::query();
        $property = Arr::get($searchParams, 'property_id', '');
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','asc')
                ->with([])->paginate($limit)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        return PropertyValuationBreakdown::create([
            'property_id' => \request('property_id'),
            'breakdown_type' => \request('breakdown_type'),
            'floors_units' => '',
            'type' => '',
            'no_of_units' => '',
            'area' => 0,
            'rent_unit_or_rent_area' => 0,
            'operational_costs' => '',
            'deduction' => 0,
            'total_deduction' => 0,
            'description' => '',
            'value' => 0,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationBreakdown  $propertyValuationBreakdown
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationBreakdown $propertyValuationBreakdown)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationBreakdown  $propertyValuationBreakdown
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationBreakdown $propertyValuationBreakdown)
    {
        //
    }

    public function update()
    {


        $breakdowns = \request('breakdowns');

        foreach ($breakdowns as $breakdown){

            $DBbreakdown = PropertyValuationBreakdown::find($breakdown['id']);

            $DBbreakdown->floors_units = $breakdown['floors_units'];
            $DBbreakdown->type = $breakdown['type'];
            $DBbreakdown->no_of_units = $breakdown['no_of_units'];
            $DBbreakdown->area = $breakdown['area'];
            $DBbreakdown->rent_unit_or_rent_area = $breakdown['rent_unit_or_rent_area'];
            $DBbreakdown->operational_costs = $breakdown['operational_costs'];
            $DBbreakdown->deduction = $breakdown['deduction'];
            $DBbreakdown->total_deduction = $breakdown['total_deduction'];
            $DBbreakdown->description = $breakdown['description'];
            $DBbreakdown->value = $breakdown['value'];
            $DBbreakdown->breakdown_type = $breakdown['breakdown_type'];

            $DBbreakdown->save();

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropertyValuationBreakdown  $propertyValuationBreakdown
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyValuationBreakdown $propertyValuationBreakdown)
    {

        $propertyValuationBreakdown->delete();

        dd('ok');
    }
}
