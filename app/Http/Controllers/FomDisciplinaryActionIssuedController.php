<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomDisciplinaryActionIssuedStoreRequest;
use App\Http\Resources\FomDisciplinaryActionIssuedCollection;
use App\Http\Resources\FomDisciplinaryActionIssuedsSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomDisciplinaryActionIssued;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomDisciplinaryActionIssuedController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomDisciplinaryActionIssuedCollection($fomTask->disciplinary_action_issues);
    }

    public function store(FomDisciplinaryActionIssuedStoreRequest $request)
    {

        $fomPermitToWorkForm = FomDisciplinaryActionIssued::create([
            'warning_type' => \request('warning_type'),
            'company' => \request('company'),
            'employee_name' => \request('employee_name'),
            'designation' => \request('designation'),
            'date_of_warning' => \request('date_of_warning'),
            'id_number' => \request('id_number'),
            'significant_comments' => \request('significant_comments'),
            'supervisors_name_and_title' => \request('supervisors_name_and_title'),
            'warned_for' => \request('warned_for'),
            'employees_comments' => \request('employees_comments'),
            'violation_date_time' => \request('violation_date_time'),
            'warning_no' => \request('warning_no'),
            'location' => \request('location'),
            'counseling_comments' => \request('counseling_comments'),
            'violated' => \request('violated'),
            'additional_action' => \request('additional_action'),
            'restate' => \request('restate'),
            'warned_before' => \request('warned_before'),
            'warned_before_type' => \request('warned_before_type'),
            'warned_before_by_whom' => \request('warned_before_by_whom'),
            'removed_for' => \request('removed_for'),

            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));


        return new FomDisciplinaryActionIssuedsSingleResource($fomPermitToWorkForm);

    }

    public function statusUpdate(FomDisciplinaryActionIssued $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomDisciplinaryActionIssued $form)
    {

        $pdf = PDF::loadView('fom.fuf.disciplinary-action-issued',compact('form'));

        return $pdf->stream('disciplinary-action-issued.pdf');

    }
}
