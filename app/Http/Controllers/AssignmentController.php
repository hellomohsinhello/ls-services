<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignmentStatusUpdateRequest;
use App\Http\Requests\AssignmentStoreRequest;
use App\Http\Requests\AssignmentUpdateRequest;
use App\Http\Resources\AssignmentResource;
use App\Http\Resources\AssignmentResourceCollection;
use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Http\Resources\PropertyResourceCollection;
use App\Models\Assignment;
use App\Models\Portfolio;
use App\Models\Property;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AssignmentController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Assignment::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('title', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('ref_no', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('client_ref', 'LIKE', '%' . $keyword . '%');
        }

        return new CommonResourceCollection(
            $resourceQuery
            ->orderBy('created_at','desc')
            ->with(['contact','portfolio','user','manager'])->paginate($limit)
        );

    }

    public function getProperties(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Property::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $assignment_id = Arr::get($searchParams, 'assignment_id', '');

        if (!empty($assignment_id)) {
            $resourceQuery->where('assignment_id', $assignment_id);
        }
        if (!empty($keyword)) {
            $resourceQuery->where('name', 'like',"%$keyword%");
        }

        /*if (!empty($keyword)) {
            $resourceQuery->where('title', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('ref_no', 'LIKE', '%' . $keyword . '%');
        }
        if (!empty($service)) {
            $resourceQuery->where('service_id', (int) $service);
        }*/

        return new PropertyResourceCollection($resourceQuery->with(['valuer','inspector','type','area.province.country','template'])->paginate($limit));

    }

    public function updateStatus(AssignmentStatusUpdateRequest $request, Assignment $assignment)
    {

        $assignment->status_id = \request('status');
        $assignment->save();

        return new AssignmentResource($assignment);
    }

    public function store(AssignmentStoreRequest $request)
    {


        $service = Service::find(\request('service'));

        $status = $service->statuses()->where('title','New')->first();

        $assignment = Assignment::create([
            'title' => \request('title'),
            'status' => 'New',
            'manager_id' => \request('manager'),
            'due_date' => Carbon::parse(\request('due_date')),
            'instruction_date' => Carbon::parse(\request('instruction_date')),
            'client_ref' => \request('client_ref'),
            'user_id' => Auth::user()->id,
            'report_lang' => \request('report_lang'),
            'client_contact_id' => \request('client_contact'),
            'ref_no' => assignment_ref_key($service),
            'is_portfolio' => request('is_portfolio') ?? 0
        ]);

        if (request('is_portfolio')){
            $portfolio = new Portfolio();
            $portfolio->assignment_id = $assignment->id;
            $portfolio->hide_valuation = \request('hide_valuation');
            $portfolio->template_type = \request('template_type');
            $portfolio->report_title = \request('report_title');
            $portfolio->applicant_name = \request('applicant_name');
            $portfolio->introduction = \request('introduction');
            $portfolio->brief_description = \request('brief_description');
            $portfolio->address = \request('address');
            $portfolio->purpose = \request('purpose');
            $portfolio->tenure_status = \request('tenure_status');
            $portfolio->valuation_date = Carbon::parse(\request('valuation_date'));
            $portfolio->final_notes = \request('final_notes');
            $portfolio->instruction = \request('instruction');
            $portfolio->methodology = \request('methodology');
            $portfolio->valuation_basis = \request('valuation_basis');
            $portfolio->forced_sale = \request('forced_sale');
            $portfolio->is_page_signature = \request('is_page_signature');
            $portfolio->valuation_approach = \request('valuation_approach');
            $portfolio->valuation_calculation_summary = \request('valuation_calculation_summary');
            $portfolio->principal_risks = \request('principal_risks');
            $portfolio->general_market_commentary = \request('general_market_commentary');
            $portfolio->special_assumptions_appendices_b = \request('special_assumptions_appendices_b');
            $portfolio->general_market_commentary_2 = \request('general_market_commentary_2');
            $portfolio->save();
        }

        return new AssignmentResource($assignment);
    }

    public function update(AssignmentUpdateRequest $request, Assignment $assignment)
    {

        $assignment->title = \request('title');
        $assignment->manager_id = \request('manager');
        $assignment->due_date = Carbon::parse(\request('due_date'));
        $assignment->instruction_date = Carbon::parse(\request('instruction_date'));
        $assignment->client_ref = \request('client_ref');
        $assignment->report_lang = \request('report_lang');
        $assignment->client_id = \request('client');
        $assignment->client_contact_id = \request('client_contact');
        $assignment->is_portfolio = request('is_portfolio') ?? 0;
        $assignment->save();

        if (request('is_portfolio')){

            $portfolio = Portfolio::firstOrCreate(['assignment_id' => $assignment->id]);

            $portfolio->hide_valuation = \request('hide_valuation');
            $portfolio->template_type = \request('template_type');
            $portfolio->report_title = \request('report_title');
            $portfolio->applicant_name = \request('applicant_name');
            $portfolio->introduction = \request('introduction');
            $portfolio->brief_description = \request('brief_description');
            $portfolio->address = \request('address');
            $portfolio->purpose = \request('purpose');
            $portfolio->tenure_status = \request('tenure_status');
            $portfolio->valuation_date = Carbon::parse(\request('valuation_date'))->addDay();
            $portfolio->final_notes = \request('final_notes');
            $portfolio->instruction = \request('instruction');
            $portfolio->methodology = \request('methodology');
            $portfolio->valuation_basis = \request('valuation_basis');
            $portfolio->forced_sale = \request('forced_sale');
            $portfolio->is_page_signature = \request('is_page_signature');
            $portfolio->valuation_approach = \request('valuation_approach');
            $portfolio->valuation_calculation_summary = \request('valuation_calculation_summary');
            $portfolio->principal_risks = \request('principal_risks');
            $portfolio->general_market_commentary = \request('general_market_commentary');
            $portfolio->special_assumptions_appendices_b = \request('special_assumptions_appendices_b');
            $portfolio->general_market_commentary_2 = \request('general_market_commentary_2');
            $portfolio->save();

        }

        return new AssignmentResource($assignment);

    }

    public function show(Assignment $assignment)
    {

        return new AssignmentResource($assignment->load('service'));
    }

    public function getTemplates(Assignment $assignment)
    {
        return [
            'data' => $assignment->service->templates
        ];
    }

    public function getInspections(Assignment $assignment)
    {
        return [
            'data' => $assignment->service->inspections
        ];
    }

    public function updateAttribute(Assignment $assignment)
    {

        $attributes = \request()->all();


        foreach ($attributes as $key => $value){
            $assignment->{$key} = $value;
        }

        $assignment->save();

        return new CommonSingleResource($assignment);
    }
}
