<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomHseWeeklyInspectionStoreRequest;
use App\Http\Resources\FomHseWeeklyInspectionCollection;
use App\Http\Resources\FomHseWeeklyInspectionSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomHseWeeklyInspection;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomHseWeeklyInspectionController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomHseWeeklyInspectionCollection($fomTask->hse_weekly_inspections);
    }

    public function store(FomHseWeeklyInspectionStoreRequest $request)
    {

        $fomPermitToWorkForm = FomHseWeeklyInspection::create([
            'safety_assessment_summary' => \request('safety_assessment_summary'),
            't_contact_person' => \request('t_contact_person'),
            't_office_tel_no' => \request('t_office_tel_no'),
            't_title_position' => \request('t_title_position'),
            't_mobile_no' => \request('t_mobile_no'),
            't_address' => \request('t_address'),
            't_email' => \request('t_email'),
            'c_contact_person' => \request('c_contact_person'),
            'c_office_tel_no' => \request('c_office_tel_no'),
            'c_title_position' => \request('c_title_position'),
            'c_mobile_no' => \request('c_mobile_no'),
            'c_address' => \request('c_address'),
            'c_email' => \request('c_email'),
            'r_contact_person' => \request('r_contact_person'),
            'r_office_tel_no' => \request('r_office_tel_no'),
            'r_title_position' => \request('r_title_position'),
            'r_mobile_no' => \request('r_mobile_no'),
            'workplace_mobilization_1_points' => \request('workplace_mobilization_1_points'),
            'workplace_mobilization_1_persent' => \request('workplace_mobilization_1_persent'),
            'workplace_mobilization_2_points' => \request('workplace_mobilization_2_points'),
            'workplace_mobilization_2_persent' => \request('workplace_mobilization_2_persent'),
            'workplace_mobilization_3_points' => \request('workplace_mobilization_3_points'),
            'workplace_mobilization_3_persent' => \request('workplace_mobilization_3_persent'),
            'workplace_mobilization_4_points' => \request('workplace_mobilization_4_points'),
            'workplace_mobilization_4_persent' => \request('workplace_mobilization_4_persent'),
            'workplace_mobilization_5_points' => \request('workplace_mobilization_5_points'),
            'workplace_mobilization_5_persent' => \request('workplace_mobilization_5_persent'),
            'workplace_mobilization_6_points' => \request('workplace_mobilization_6_points'),
            'workplace_mobilization_6_persent' => \request('workplace_mobilization_6_persent'),
            'workplace_mobilization_7_points' => \request('workplace_mobilization_7_points'),
            'workplace_mobilization_7_persent' => \request('workplace_mobilization_7_persent'),
            'workplace_mobilization_8_points' => \request('workplace_mobilization_8_points'),
            'workplace_mobilization_8_persent' => \request('workplace_mobilization_8_persent'),
            'workplace_mobilization_9_points' => \request('workplace_mobilization_9_points'),
            'workplace_mobilization_9_persent' => \request('workplace_mobilization_9_persent'),
            'workplace_mobilization_10_points' => \request('workplace_mobilization_10_points'),
            'workplace_mobilization_10_persent' => \request('workplace_mobilization_10_persent'),
            'workplace_mobilization_11_points' => \request('workplace_mobilization_11_points'),
            'workplace_mobilization_11_persent' => \request('workplace_mobilization_11_persent'),
            'workplace_mobilization_12_points' => \request('workplace_mobilization_12_points'),
            'workplace_mobilization_12_persent' => \request('workplace_mobilization_12_persent'),
            'workplace_mobilization_13_points' => \request('workplace_mobilization_13_points'),
            'workplace_mobilization_13_persent' => \request('workplace_mobilization_13_persent'),
            'workplace_mobilization_14_points' => \request('workplace_mobilization_14_points'),
            'workplace_mobilization_14_persent' => \request('workplace_mobilization_14_persent'),
            'workplace_mobilization_15_points' => \request('workplace_mobilization_15_points'),
            'workplace_mobilization_15_persent' => \request('workplace_mobilization_15_persent'),
            'workplace_mobilization_16_points' => \request('workplace_mobilization_16_points'),
            'workplace_mobilization_16_persent' => \request('workplace_mobilization_16_persent'),
            'workplace_mobilization_17_points' => \request('workplace_mobilization_17_points'),
            'workplace_mobilization_17_persent' => \request('workplace_mobilization_17_persent'),
            'workplace_mobilization_18_points' => \request('workplace_mobilization_18_points'),
            'workplace_mobilization_18_persent' => \request('workplace_mobilization_18_persent'),
            'workplace_mobilization_19_points' => \request('workplace_mobilization_19_points'),
            'workplace_mobilization_19_persent' => \request('workplace_mobilization_19_persent'),
            'workplace_mobilization_20_points' => \request('workplace_mobilization_20_points'),
            'workplace_mobilization_20_persent' => \request('workplace_mobilization_20_persent'),
            'workplace_mobilization_21_points' => \request('workplace_mobilization_21_points'),
            'workplace_mobilization_21_persent' => \request('workplace_mobilization_21_persent'),
            'workplace_mobilization_22_points' => \request('workplace_mobilization_22_points'),
            'workplace_mobilization_22_persent' => \request('workplace_mobilization_22_persent'),
            'workplace_mobilization_23_points' => \request('workplace_mobilization_23_points'),
            'workplace_mobilization_23_persent' => \request('workplace_mobilization_23_persent'),
            'workplace_mobilization_24_points' => \request('workplace_mobilization_24_points'),
            'workplace_mobilization_24_persent' => \request('workplace_mobilization_24_persent'),
            'workplace_mobilization_25_points' => \request('workplace_mobilization_25_points'),
            'workplace_mobilization_25_persent' => \request('workplace_mobilization_25_persent'),
            'workplace_mobilization_26_points' => \request('workplace_mobilization_26_points'),
            'workplace_mobilization_26_persent' => \request('workplace_mobilization_26_persent'),
            'workplace_mobilization_27_points' => \request('workplace_mobilization_27_points'),
            'workplace_mobilization_27_persent' => \request('workplace_mobilization_27_persent'),
            'workplace_mobilization_28_points' => \request('workplace_mobilization_28_points'),
            'workplace_mobilization_28_persent' => \request('workplace_mobilization_28_persent'),
            'workplace_mobilization_29_points' => \request('workplace_mobilization_29_points'),
            'workplace_mobilization_29_persent' => \request('workplace_mobilization_29_persent'),
            'workplace_mobilization_30_points' => \request('workplace_mobilization_30_points'),
            'workplace_mobilization_30_persent' => \request('workplace_mobilization_30_persent'),
            'workplace_mobilization_31_points' => \request('workplace_mobilization_31_points'),
            'workplace_mobilization_31_persent' => \request('workplace_mobilization_31_persent'),
            'workplace_mobilization_32_points' => \request('workplace_mobilization_32_points'),
            'workplace_mobilization_32_persent' => \request('workplace_mobilization_32_persent'),
            'workplace_mobilization_33_points' => \request('workplace_mobilization_33_points'),
            'workplace_mobilization_33_persent' => \request('workplace_mobilization_33_persent'),
            'workplace_mobilization_34_points' => \request('workplace_mobilization_34_points'),
            'workplace_mobilization_34_persent' => \request('workplace_mobilization_34_persent'),
            'workplace_mobilization_35_points' => \request('workplace_mobilization_35_points'),
            'workplace_mobilization_35_persent' => \request('workplace_mobilization_35_persent'),
            'workplace_mobilization_36_points' => \request('workplace_mobilization_36_points'),
            'workplace_mobilization_36_persent' => \request('workplace_mobilization_36_persent'),
            'workplace_mobilization_37_points' => \request('workplace_mobilization_37_points'),
            'workplace_mobilization_37_persent' => \request('workplace_mobilization_37_persent'),
            'workplace_mobilization_38_points' => \request('workplace_mobilization_38_points'),
            'workplace_mobilization_38_persent' => \request('workplace_mobilization_38_persent'),
            'workplace_mobilization_39_points' => \request('workplace_mobilization_39_points'),
            'workplace_mobilization_39_persent' => \request('workplace_mobilization_39_persent'),
            'workplace_mobilization_40_points' => \request('workplace_mobilization_40_points'),
            'workplace_mobilization_40_persent' => \request('workplace_mobilization_40_persent'),
            'workplace_mobilization_41_points' => \request('workplace_mobilization_41_points'),
            'workplace_mobilization_41_persent' => \request('workplace_mobilization_41_persent'),
            'workplace_mobilization_42_points' => \request('workplace_mobilization_42_points'),
            'workplace_mobilization_42_persent' => \request('workplace_mobilization_42_persent'),
            'workplace_mobilization_43_points' => \request('workplace_mobilization_43_points'),
            'workplace_mobilization_43_persent' => \request('workplace_mobilization_43_persent'),
            'workplace_mobilization_44_points' => \request('workplace_mobilization_44_points'),
            'workplace_mobilization_44_persent' => \request('workplace_mobilization_44_persent'),
            'workplace_mobilization_45_points' => \request('workplace_mobilization_45_points'),
            'workplace_mobilization_45_persent' => \request('workplace_mobilization_45_persent'),
            'workplace_mobilization_46_points' => \request('workplace_mobilization_46_points'),
            'workplace_mobilization_46_persent' => \request('workplace_mobilization_46_persent'),
            'workplace_mobilization_47_points' => \request('workplace_mobilization_47_points'),
            'workplace_mobilization_47_persent' => \request('workplace_mobilization_47_persent'),
            'workplace_mobilization_48_points' => \request('workplace_mobilization_48_points'),
            'workplace_mobilization_48_persent' => \request('workplace_mobilization_48_persent'),
            'workplace_mobilization_49_points' => \request('workplace_mobilization_49_points'),
            'workplace_mobilization_49_persent' => \request('workplace_mobilization_49_persent'),
            'workplace_mobilization_50_points' => \request('workplace_mobilization_50_points'),
            'workplace_mobilization_50_persent' => \request('workplace_mobilization_50_persent'),
            'workplace_mobilization_51_points' => \request('workplace_mobilization_51_points'),
            'workplace_mobilization_51_persent' => \request('workplace_mobilization_51_persent'),
            'workplace_mobilization_52_points' => \request('workplace_mobilization_52_points'),
            'workplace_mobilization_52_persent' => \request('workplace_mobilization_52_persent'),
            'workplace_mobilization_53_points' => \request('workplace_mobilization_53_points'),
            'workplace_mobilization_53_persent' => \request('workplace_mobilization_53_persent'),
            'workplace_mobilization_54_points' => \request('workplace_mobilization_54_points'),
            'workplace_mobilization_54_persent' => \request('workplace_mobilization_54_persent'),
            'workplace_mobilization_55_points' => \request('workplace_mobilization_55_points'),
            'workplace_mobilization_55_persent' => \request('workplace_mobilization_55_persent'),
            'workplace_mobilization_56_points' => \request('workplace_mobilization_56_points'),
            'workplace_mobilization_56_persent' => \request('workplace_mobilization_56_persent'),
            'workplace_mobilization_57_points' => \request('workplace_mobilization_57_points'),
            'workplace_mobilization_57_persent' => \request('workplace_mobilization_57_persent'),
            'workplace_mobilization_58_points' => \request('workplace_mobilization_58_points'),
            'workplace_mobilization_58_persent' => \request('workplace_mobilization_58_persent'),
            'workplace_mobilization_59_points' => \request('workplace_mobilization_59_points'),
            'workplace_mobilization_59_persent' => \request('workplace_mobilization_59_persent'),
            'workplace_mobilization_60_points' => \request('workplace_mobilization_60_points'),
            'workplace_mobilization_60_persent' => \request('workplace_mobilization_60_persent'),
            'workplace_mobilization_61_points' => \request('workplace_mobilization_61_points'),
            'workplace_mobilization_61_persent' => \request('workplace_mobilization_61_persent'),
            'workplace_mobilization_62_points' => \request('workplace_mobilization_62_points'),
            'workplace_mobilization_62_persent' => \request('workplace_mobilization_62_persent'),
            'workplace_mobilization_63_points' => \request('workplace_mobilization_63_points'),
            'workplace_mobilization_63_persent' => \request('workplace_mobilization_63_persent'),
            'workplace_mobilization_64_points' => \request('workplace_mobilization_64_points'),
            'workplace_mobilization_64_persent' => \request('workplace_mobilization_64_persent'),
            'workplace_mobilization_65_points' => \request('workplace_mobilization_65_points'),
            'workplace_mobilization_65_persent' => \request('workplace_mobilization_65_persent'),
            'workplace_mobilization_66_points' => \request('workplace_mobilization_66_points'),
            'workplace_mobilization_66_persent' => \request('workplace_mobilization_66_persent'),
            'workplace_mobilization_67_points' => \request('workplace_mobilization_67_points'),
            'workplace_mobilization_67_persent' => \request('workplace_mobilization_67_persent'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomHseWeeklyInspectionSingleResource($fomPermitToWorkForm);

    }

    public function statusUpdate(FomHseWeeklyInspection $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomHseWeeklyInspection $form)
    {

        $pdf = PDF::loadView('fom.fuf.hse-weekly-inspection',compact('form'));

        return $pdf->stream('hse-weekly-inspection.pdf');

    }
}
