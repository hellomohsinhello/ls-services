<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomAccessWorkPermitStoreRequest;
use App\Http\Resources\FomAccessWorkPermitCollection;
use App\Http\Resources\FomAccessWorkPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomAccessWorkPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomAccessWorkPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomAccessWorkPermitCollection($fomTask->access_work_permits);
    }

    public function store(FomAccessWorkPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomAccessWorkPermit::create([
            'contractor_name' => \request('contractor_name'),
            'contractor_contact_no' => \request('contractor_contact_no'),
            'site_supervisor_name' => \request('site_supervisor_name'),
            'site_supervisor_contact_no' => \request('site_supervisor_contact_no'),
            'issuance_date' => \request('issuance_date'),
            'validity_date' => \request('validity_date'),
            'pre_work_condition' => \request('pre_work_condition'),
            'post_work_condition' => \request('post_work_condition'),
            'areas' => json_encode(\request('areas')),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);


//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomAccessWorkPermitSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomAccessWorkPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomAccessWorkPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.access-work-permit',compact('form'));

        return $pdf->stream('access-work-permit.pdf');

    }

}
