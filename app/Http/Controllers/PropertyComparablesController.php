<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\PropertyValuationComparable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyComparablesController extends Controller
{
    public function index(Request $request)
    {

        $searchParams = $request->all();
        $resourceQuery = PropertyValuationComparable::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        $property_id = Arr::get($searchParams, 'property_id', '');

        if (!empty($property_id)) {
            $resourceQuery->where('property_id',$property_id);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','desc')
                ->with([])->paginate($limit)
        );

    }


    public function store()
    {


        $comparables = \request('comparables');
        $property = \request('property');

        foreach ($comparables as $comparable){
            PropertyValuationComparable::create([
                'date' => $comparable['date'],
                'type' => $comparable['type'],
                'area' => $comparable['area'],
                'area_price' => $comparable['area_price'],
                'total_price' => get($comparable['total_price'],10),
                'transaction_price' => $comparable['transaction_price'],
                'location' => $comparable['location'],
                'property_id' => $property,
                'transaction_type' => $comparable['transaction_type'],
                'details' => $comparable['details'],
                'type_ar' => get($comparable['type_ar']),
                'transaction_type_ar' => get($comparable['transaction_type_ar']),
                'property_type' => get($comparable['property_type']),
                'details_ar' => get($comparable['details_ar']),
                'sort_order' => get($comparable['sort_order'],1),
                'sales_index_id' => get($comparable['sales_index_id'],-999),
                'land_area' => get($comparable['land_area'],0),
                'land_area_price' => get($comparable['land_area_price'],0),
            ]);

        }

        return $comparables;
    }


    public function destroy(PropertyValuationComparable $property_comparable)
    {
        $property_comparable->delete();
    }

    public function update()
    {


        $comparables = \request('comparables');

        foreach ($comparables as $comparable){
            $DBcomparable = PropertyValuationComparable::find($comparable['id']);

            $DBcomparable->date = $comparable['date'];
            $DBcomparable->type = $comparable['type'];
            $DBcomparable->area = $comparable['area'];
            $DBcomparable->area_price = $comparable['area_price'];
            $DBcomparable->total_price = $comparable['total_price'];
            $DBcomparable->transaction_price = $comparable['transaction_price'];
            $DBcomparable->location = $comparable['location'];
            $DBcomparable->property_id = $comparable['property_id'];
            $DBcomparable->inspection_id = $comparable['inspection_id'];
            $DBcomparable->transaction_type = $comparable['transaction_type'];
            $DBcomparable->created_at = $comparable['created_at'];
            $DBcomparable->details = $comparable['details'];
            $DBcomparable->report_id = $comparable['report_id'];
            $DBcomparable->assignment_id = $comparable['assignment_id'];
            $DBcomparable->type_ar = $comparable['type_ar'];
            $DBcomparable->transaction_type_ar = $comparable['transaction_type_ar'];
            $DBcomparable->property_type = $comparable['property_type'];
            $DBcomparable->details_ar = $comparable['details_ar'];
            $DBcomparable->sort_order = $comparable['sort_order'];
            $DBcomparable->sales_index_id = $comparable['sales_index_id'];
            $DBcomparable->land_area = $comparable['land_area'];
            $DBcomparable->land_area_price = $comparable['land_area_price'];
            $DBcomparable->adjustment_1 = $comparable['adjustment_1'];
            $DBcomparable->adjustment_2 = $comparable['adjustment_2'];
            $DBcomparable->adjustment_3 = $comparable['adjustment_3'];

            $DBcomparable->save();
        }
    }

}
