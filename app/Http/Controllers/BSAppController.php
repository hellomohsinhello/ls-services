<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\BuildingSurveyAssignment;
use App\Models\BuildingSurveyProperty;
use App\Models\Defect;
use App\Models\Property;
use Illuminate\Http\Request;

class BSAppController extends Controller
{
    public function getAssignments()
    {
        return BuildingSurveyAssignment::all();
    }

    public function getProperties(BuildingSurveyAssignment $assignment)
    {
        return $assignment->properties()->get();
    }

    public function getDefects(BuildingSurveyProperty $property)
    {
        return $property->defects()->get();
    }

    public function saveProperty(BuildingSurveyAssignment $assignment)
    {
        return BuildingSurveyProperty::create([
            'name' => \request()->get('name'),
            'template_id' => \request()->get('template'),
            'assignment_id' => $assignment->id,
        ]);
    }

    public function saveDefects(BuildingSurveyProperty $property)
    {

        $liberal_party =  str_replace('LiberalParty.','',\request()->get('liberal_party'));
        $priority =  str_replace('Priority.','',\request()->get('priority'));

        $defect = Defect::create([
            'location' => \request()->get('location'),
            'description' => \request()->get('description'),
            'recommendation' => \request()->get('recommendation'),
            'liable_parties' => $liberal_party,
            'priority' => $priority,
            'property_id' => $property->id,
        ]);

        $defect->rating = json_encode([
            'ss_requirements' => 0,
            'po_condition' => 0,
            'fs_performance' => 0,
            'ee_performance' => 0,

        ]);

        $defect->save();

        $defect->addMediaFromBase64(\request()->get('image'))->toMediaCollection('image');

        return $defect;

    }
}
