<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomAttendanceFormStoreRequest;
use App\Http\Resources\FomAttendanceFormCollection;
use App\Http\Resources\FomAttendanceFormSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomAttendanceForm;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomAttendanceFormController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomAttendanceFormCollection($fomTask->attendance_forms);
    }

    public function store(FomAttendanceFormStoreRequest $request)
    {

        $fomPermitToWorkForm = FomAttendanceForm::create([
            'attendances' => json_encode(\request('attendances')),
            'day' => \request('day'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

        #send emails to receivers

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));
        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomAttendanceFormSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomAttendanceForm $fomAttendanceForm)
    {
        $fomAttendanceForm->status = \request('status');
        $fomAttendanceForm->comment = \request('comment');
        $fomAttendanceForm->save();
    }

    public function downlaod(FomAttendanceForm $form)
    {

        $pdf = PDF::loadView('fom.fuf.attendance-form',compact('form'));
        return $pdf->stream('attendance-form.pdf');
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }
}
