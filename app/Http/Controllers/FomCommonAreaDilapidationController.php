<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomCommonAreaDilapidationStoreRequest;
use App\Http\Resources\FomCommonAreaDilapidationCollection;
use App\Http\Resources\FomCommonAreaDilapidationSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomCommonAreaDilapidation;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomCommonAreaDilapidationController extends Controller
{
    public function index(FomTask $fomTask)
    {

        return new FomCommonAreaDilapidationCollection($fomTask->common_area_dilapidations);
    }

    public function store(FomCommonAreaDilapidationStoreRequest $request)
    {

        $fomPermitToWorkForm = FomCommonAreaDilapidation::create([
            'items' => json_encode(\request('items')),
            'project_manager' => \request('project_manager'),
            'inspection_date' => \request('inspection_date'),
            'report_issue_date' => \request('report_issue_date'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);


//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomCommonAreaDilapidationSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomCommonAreaDilapidation $dilapidation)
    {
        $dilapidation->status = \request('status');
        $dilapidation->comment = \request('comment');
        $dilapidation->save();
    }
    public function downlaod(FomCommonAreaDilapidation $form)
    {

        $pdf = PDF::loadView('fom.fuf.common-area-dilapidation-report',compact('form'));
        return $pdf->stream('common-area-dilapidation-report.pdf');
    }


    public function task()
    {
        return $this->belongsTo(FomTask::class);
    }

}
