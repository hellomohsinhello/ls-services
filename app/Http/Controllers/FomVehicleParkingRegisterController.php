<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomVehicleParkingRegisterStoreRequest;
use App\Http\Resources\FomVehicleParkingRegisterCollection;
use App\Http\Resources\FomVehicleParkingRegisterSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomTask;
use App\Models\FomVehicleParkingRegister;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomVehicleParkingRegisterController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomVehicleParkingRegisterCollection($fomTask->vehicle_parking_registers);
    }

    public function store(FomVehicleParkingRegisterStoreRequest $request)
    {

        $fomPermitToWorkForm = FomVehicleParkingRegister::create([
            'vehicles' => json_encode(\request('vehicles')),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));


        return new FomVehicleParkingRegisterSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomVehicleParkingRegister $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomVehicleParkingRegister $form)
    {

        $pdf = PDF::loadView('fom.fuf.vehicle-parking-register',compact('form'));

        return $pdf->stream('vehicle-parking-register.pdf');

    }
}
