<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomExtendedHoursPermitStoreRequest;
use App\Http\Resources\FomExtendedHoursPermitCollection;
use App\Http\Resources\FomExtendedHoursPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomExtendedHoursPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomExtendedHoursPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomExtendedHoursPermitCollection($fomTask->extended_hours_permits);
    }

    public function store(FomExtendedHoursPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomExtendedHoursPermit::create([
            'company' => \request('company'),
            'date' => \request('time.0'),
            'time' => \request('time.1'),
            'time_end' => \request('time.1'),
            'location_of_work' => \request('location_of_work'),
            'description_of_work' => \request('description_of_work'),
            'equipment_needed' => \request('equipment_needed'),
            'supervisor_name' => \request('supervisor_name'),
            'supervisor_contact_number' => \request('supervisor_contact_number'),
            'safety_name' => \request('safety_name'),
            'safety_contact_number' => \request('safety_contact_number'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));


        return new FomExtendedHoursPermitSingleResource($fomPermitToWorkForm);

    }

    public function statusUpdate(FomExtendedHoursPermit $permit)
    {

        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();

    }

    public function downlaod(FomExtendedHoursPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.extended-hours-permit',compact('form'));

        return $pdf->stream('extended-hours-permit.pdf');

    }

}
