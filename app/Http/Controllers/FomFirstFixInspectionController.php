<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomFirstFixInspectionStoreRequest;
use App\Http\Resources\FomFirstFixInspectionCollection;
use App\Http\Resources\FomFirstFixInspectionSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomFirstFixInspection;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomFirstFixInspectionController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomFirstFixInspectionCollection($fomTask->first_fix_inspections);
    }

    public function store(FomFirstFixInspectionStoreRequest $request)
    {

        $fomPermitToWorkForm = FomFirstFixInspection::create([
            't_name_of_tenant' => \request('t_name_of_tenant'),
            't_shop_unit_no' => \request('t_shop_unit_no'),
            't_doc_ref_no' => \request('t_doc_ref_no'),
            't_file_no' => \request('t_file_no'),
            't_unit' => \request('t_unit'),
            'c_contact_person' => \request('c_contact_person'),
            'c_office_tel_no' => \request('c_office_tel_no'),
            'c_title_position' => \request('c_title_position'),
            'c_mobile_no' => \request('c_mobile_no'),
            'c_address' => \request('c_address'),
            'c_email' => \request('c_email'),
            'f_contact_person' => \request('f_contact_person'),
            'f_office_tel_no' => \request('f_office_tel_no'),
            'f_title_position' => \request('f_title_position'),
            'f_mobile_no' => \request('f_mobile_no'),
            'question_1' => \request('question_1'),
            'question_1_remarks' => \request('question_1_remarks'),
            'question_2' => \request('question_2'),
            'question_2_remarks' => \request('question_2_remarks'),
            'question_3' => \request('question_3'),
            'question_3_remarks' => \request('question_3_remarks'),
            'question_4' => \request('question_4'),
            'question_4_remarks' => \request('question_4_remarks'),
            'question_5' => \request('question_5'),
            'question_5_remarks' => \request('question_5_remarks'),
            'question_6' => \request('question_6'),
            'question_6_remarks' => \request('question_6_remarks'),
            'question_7' => \request('question_7'),
            'question_7_remarks' => \request('question_7_remarks'),
            'question_8' => \request('question_8'),
            'question_8_remarks' => \request('question_8_remarks'),
            'question_9' => \request('question_9'),
            'question_9_remarks' => \request('question_9_remarks'),
            'question_10' => \request('question_10'),
            'question_10_remarks' => \request('question_10_remarks'),
            'question_11' => \request('question_11'),
            'question_11_remarks' => \request('question_11_remarks'),
            'question_12' => \request('question_12'),
            'question_12_remarks' => \request('question_12_remarks'),
            'question_13' => \request('question_13'),
            'question_13_remarks' => \request('question_13_remarks'),
            'question_14' => \request('question_14'),
            'question_14_remarks' => \request('question_14_remarks'),
            'question_15' => \request('question_15'),
            'question_15_remarks' => \request('question_15_remarks'),
            'question_16' => \request('question_16'),
            'question_16_remarks' => \request('question_16_remarks'),
            'question_17' => \request('question_17'),
            'question_17_remarks' => \request('question_17_remarks'),
            'question_18' => \request('question_18'),
            'question_18_remarks' => \request('question_18_remarks'),
            'question_19' => \request('question_19'),
            'question_19_remarks' => \request('question_19_remarks'),
            'question_20' => \request('question_20'),
            'question_20_remarks' => \request('question_20_remarks'),
            'question_21' => \request('question_21'),
            'question_21_remarks' => \request('question_21_remarks'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomFirstFixInspectionSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomFirstFixInspection $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomFirstFixInspection $form)
    {

        $pdf = PDF::loadView('fom.fuf.first-fix-inspection',compact('form'));

        return $pdf->stream('first-fix-inspection.pdf');

    }
}
