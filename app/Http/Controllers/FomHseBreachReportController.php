<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomHseBreachReportStoreRequest;
use App\Http\Resources\FomHseBreachReportCollection;
use App\Http\Resources\FomHseBreachReportSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomHseBreachReport;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomHseBreachReportController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomHseBreachReportCollection($fomTask->material_delivery_permits);
    }

    public function store(FomHseBreachReportStoreRequest $request)
    {

        $fomPermitToWorkForm = FomHseBreachReport::create([
            'contractor_name' => \request('contractor_name'),
            'date' => \request('date'),
            'initiated_by' => \request('initiated_by'),
            'ls_project_manager' => \request('ls_project_manager'),
            'document_reference' => \request('document_reference'),
            'requirement' => \request('requirement'),
            'observation' => \request('observation'),
            'dispositions' => json_encode(\request('dispositions')),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomHseBreachReportSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomHseBreachReport $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomHseBreachReport $form)
    {

        $pdf = PDF::loadView('fom.fuf.hse-breach-report',compact('form'));

        return $pdf->stream('hse-breach-report.pdf');

    }


}
