<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\PropertyValuationCostUC;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationCostUCController extends Controller
{

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationCostUC::query();
        $property = Arr::get($searchParams, 'property_id', '');
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','asc')
                ->with([])->paginate($limit)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $cost = PropertyValuationCostUC::create([
            'property_id' => \request('property_id'),
            'description' => \request('description'),
            'type' => \request('type'),
            'area_length' => \request('area_length') ?? 0,
            'complete_percent' => 0,
            'rate_area' => 0,
            'cost_as_is' => 0,
            'cost_as_completed' => 0,
            'apply_profit' => 0,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationCostUC  $propertyValuationCostUC
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationCostUC $propertyValuationCostUC)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationCostUC  $propertyValuationCostUC
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationCostUC $propertyValuationCostUC)
    {
        //
    }

    public function update()
    {


        $costs = \request('costs');

        foreach ($costs as $cost){

            $DBcost = PropertyValuationCostUC::find($cost['id']);

            $DBcost->description = $cost['description'];
            $DBcost->area_length = $cost['area_length'];
            $DBcost->complete_percent = $cost['complete_percent'];
            $DBcost->rate_area = $cost['rate_area'];
            $DBcost->cost_as_is = $cost['cost_as_is'];
            $DBcost->cost_as_completed = $cost['cost_as_completed'];
            $DBcost->apply_profit = $cost['apply_profit'];
            $DBcost->type = $cost['type'];

            $DBcost->save();

        }
    }


    public function destroy($propertyValuationCostUC)
    {
        PropertyValuationCostUC::find($propertyValuationCostUC)->delete();

        return dd('ok');
    }
}
