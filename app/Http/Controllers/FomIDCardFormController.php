<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomIDCardFormStoreRequest;
use App\Http\Resources\FomIDCardFormCollection;
use App\Http\Resources\FomIDCardFormSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomIDCardForm;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomIDCardFormController extends Controller
{
    public function index(FomTask $fomTask)
    {

        return new FomIDCardFormCollection($fomTask->id_card_forms);

    }

    public function store(FomIDCardFormStoreRequest $request)
    {

        $fomPermitToWorkForm = FomIDCardForm::create([
            'cards' => json_encode(\request('cards')),
            'request' => \request('request'),
            'contractor_contact_number' => \request('contractor_contact_number'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomIDCardFormSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomIDCardForm $fomIDCardForm)
    {
        $fomIDCardForm->status = \request('status');
        $fomIDCardForm->comment = \request('comment');
        $fomIDCardForm->save();
    }

    public function downlaod(FomIDCardForm $form)
    {

        $pdf = PDF::loadView('fom.fuf.h-s-induction-id-card-form',compact('form'));
//        return $pdf->download('h-s-induction-id-card-form.pdf');
        return $pdf->stream('h-s-induction-id-card-form.pdf');
    }
}
