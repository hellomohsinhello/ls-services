<?php
/**
 * File RoleController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers;

use App\Http\Requests\RoleUpdateRequest;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Resources\PermissionResource;
use App\Laravue\Models\Permission;
use App\Laravue\Models\User;
use Illuminate\Http\Request;
use App\Laravue\Models\Role;
use App\Http\Resources\RoleResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Validator;

/**
 * Class RoleController
 *
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function index(Request $request)
    {

        $searchParams = $request->all();
        $resourceQuery = Role::query();


        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('name', 'LIKE', '%' . $keyword . '%');
        }

        return RoleResource::collection($resourceQuery->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRoleRequest
     * @return \App\Http\Resources\RoleResource
     */
    public function store(StoreRoleRequest $request)
    {
        $user = Role::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return new RoleResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  Role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Role $role
     * @return RoleResource
     */
    public function update(Request $request, Role $role)
    {
        if ($role === null || $role->isAdmin()) {
            return response()->json(['error' => 'Role not found'], 404);
        }

        $permissionIds = $request->get('permissions', []);
        $permissions = Permission::allowed()->whereIn('id', $permissionIds)->get();
        $role->syncPermissions($permissions);
        $role->save();
        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Role $role
     * @return RoleResource
     */
    public function updateInfo(RoleUpdateRequest $request, Role $role)
    {
        if ($role === null || $role->isAdmin()) {
            return response()->json(['error' => 'Role not found'], 404);
        }

//        $role->name = \request('name');
        $role->description = \request('description');
        $role->save();
        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return null
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get permissions from role
     *
     * @param  Role $role
     * @return
     */
    public function permissions(Role $role)
    {
        return PermissionResource::collection($role->permissions);
    }

    /**
     * @param bool $isNew
     * @return array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name' => $isNew ? 'required|unique:roles' : 'required',
            'description' => 'required',
        ];
    }
}
