<?php

namespace App\Http\Controllers;

use App\Http\Resources\FomProjectSingleResource;
use App\Http\Resources\FomTaskGroupSettingsCollection;
use App\Mail\FomTaskGroupAssigned;
use App\Mail\FomTaskNotificationMessage;
use App\Mail\FomTaskStatusUpdated;
use App\Models\FomProject;
use App\Models\FomTask;
use App\Models\FomTaskGroup;
use App\Models\FomTaskGroupSettings;
use App\Models\FomTaskGroupUser;
use App\Notifications\FomTaskGroupAssignedNotification;
use App\Notifications\FomTaskStatusUpdatedNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;

class FomTaskGroupSettingsController extends Controller
{
    public function groupsTasks()
    {
        $searchParams = \request()->all();
        $resourceQuery = FomProject::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $project = Arr::get($searchParams, 'project_id', '');

        if (!empty($project)){

            $groups = FomProject::find($project)->load(['task_groups.setting','task_groups.tasks.setting','task_groups.receivers','task_groups.senders','building','task_groups.tasks.downloader.user','task_groups.tasks.comments']);

            $groups->task_groups_steps = $groups->task_groups->groupBy('step_t');

            return new FomProjectSingleResource($groups);
        }

    }

    public function groupsTasksUpdate(FomTaskGroup $fomTaskGroup)
    {

        $fomTaskGroup->due_date = Carbon::parse(\request()->get('due_date'));
        $fomTaskGroup->assigned = \request()->get('assigned');
        $fomTaskGroup->save();

        $users = array_merge(\request('senders'),\request('receivers'));

        $fomTaskGroup->users()->sync([]);
        $fomTaskGroup->users()->sync($users);

        return $fomTaskGroup->load('setting');

    }

    public function tasksUpdate(FomTask $fomTask)
    {

        $fomTask->status = \request()->get('status');

        if (\request('notifications') and $fomTask->status != 'Not Required' and $fomTask->task_group->assigned){

            Notification::send($fomTask->task_group->senders, new FomTaskStatusUpdatedNotification($fomTask));

        }

        $fomTask->save();

        return $fomTask;

    }

    public function tasksNotify(FomTask $fomTask)
    {

        $fomTask->notification_message = \request()->get('notification_message');

        Mail::to($fomTask->task_group->receivers)->send(new FomTaskNotificationMessage($fomTask));

        $fomTask->save();

        return $fomTask;

    }
}
