<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\Models\PropertyValuationComparable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationComparableController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationComparable::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $area = Arr::get($searchParams, 'area', '');
        $transaction_type = Arr::get($searchParams, 'transaction_type', '');
        $type = Arr::get($searchParams, 'type', '');

        if (!empty($keyword)) {
            $resourceQuery->where('area_price','like',  "%{$keyword}%");
            $resourceQuery->orWhere('total_price','like',  "%{$keyword}%");
            $resourceQuery->orWhere('transaction_price','like',  "%{$keyword}%");
            $resourceQuery->orWhere('transaction_type','like',  "%{$keyword}%");
            $resourceQuery->orWhere('details','like',  "%{$keyword}%");
            $resourceQuery->orWhere('property_type','like',  "%{$keyword}%");
            $resourceQuery->orWhere('land_area','like',  "%{$keyword}%");
            $resourceQuery->orWhere('land_area_price','like',  "%{$keyword}%");
        }
        if (!empty($area)) {
            $resourceQuery->where('area',$area);
        }

        if ($request->has('dates')) {
            $from = $request->get('dates')[0];
            $to = $request->get('dates')[1];
            $resourceQuery->whereBetween('date', [$from, $to]);
        }

        if (!empty($transaction_type)) {
            $resourceQuery->where('transaction_type',$transaction_type);
        }
        if (!empty($type)) {
            $resourceQuery->where('type',$type);
        }

//        dd(Carbon::parse($date)->format('d-m-Y'));
//        return  $resourceQuery->getBindings();

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','desc')
                ->with([])->paginate($limit)
        );

//property_valuation_comparables
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyValuationComparable  $propertyValuationComparable
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationComparable $propertyValuationComparable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyValuationComparable  $propertyValuationComparable
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationComparable $propertyValuationComparable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PropertyValuationComparable  $propertyValuationComparable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyValuationComparable $propertyValuationComparable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropertyValuationComparable  $propertyValuationComparable
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyValuationComparable $propertyValuationComparable)
    {
        $propertyValuationComparable->delete();

        dd('ok');
    }
}
