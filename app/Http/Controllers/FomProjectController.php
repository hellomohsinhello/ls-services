<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomProjectCreateRequest;
use App\Http\Requests\FomProjectUpdateRequest;
use App\Http\Resources\FomProjectResourceCollection;
use App\Http\Resources\FomProjectSingleResource;
use App\Laravue\Acl;
use App\Laravue\Models\User;
use App\Models\FomBuilding;
use App\Models\FomProject;
use App\Models\FomTask;
use App\Models\FomTaskGroup;
use App\Models\FomTaskGroupSettings;
use App\Models\FomTaskGroupUser;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class FomProjectController extends Controller
{
    public function index(Request $request)
    {
        if (!auth()->user()->hasAnyRole([
            Acl::ROLE_USER,
            Acl::ROLE_FOM_MANAGER,
            Acl::ROLE_ADMIN,
            Acl::ROLE_BS_HEAD,
        ])){
            abort(403);
        }


        $searchParams = $request->all();
        $resourceQuery = FomProject::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $building = Arr::get($searchParams, 'building', '');
        $tenant = Arr::get($searchParams, 'tenant', '');
        $owner = Arr::get($searchParams, 'owner', '');
        $contractor = Arr::get($searchParams, 'contractor', '');
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');

        /*if (auth()->user()->hasRole(Acl::ROLE_CONTRACTOR)){
            $resourceQuery->where('contractor_id',auth()->user()->id);
        }elseif (auth()->user()->hasRole(Acl::ROLE_TENANT)){
            $resourceQuery->where('tenant_id',auth()->user()->id);
        }elseif (auth()->user()->hasRole(Acl::ROLE_OWNER)){
            $resourceQuery->where('owner_id',auth()->user()->id);
        }*/

        if (!auth()->user()->hasAnyRole([
            Acl::ROLE_FOM_MANAGER,
            Acl::ROLE_ADMIN,
            Acl::ROLE_BS_HEAD,
        ])){
            $resourceQuery->where('contractor_id',auth()->user()->id);
            $resourceQuery->orWhere('tenant_id',auth()->user()->id);
            $resourceQuery->orWhere('owner_id',auth()->user()->id);
        }

        if (!empty($keyword)) {
            $resourceQuery->where('units','like', '%'.$keyword.'%');
            $resourceQuery->orWhere('type','like', '%'.$keyword.'%');
        }
        if (!empty($building)) {
            $resourceQuery->where('building_id',  $building );
        }
        if (!empty($tenant)) {
            $resourceQuery->where('tenant_id',  $tenant );
        }
        if (!empty($owner)) {
            $resourceQuery->where('owner_id',  $owner );
        }
        if (!empty($contractor)) {
            $resourceQuery->where('contractor_id',  $contractor );
        }
        if (!empty($status)) {
            $resourceQuery->where('status',  $status );
        }

        $resourceQuery->with(['owner','tenant','contractor','building' ]);

        return new FomProjectResourceCollection($resourceQuery->latest()->paginate($limit));
    }

    public function update(FomProjectUpdateRequest $request, FomProject $fomProject)
    {
        $owner_id = $fomProject->owner_id;
        $tenant_id = $fomProject->tenant_id;
        $contractor_id = $fomProject->contractor_id;

        $fomProject->area = \request('area');
        $fomProject->contractor_id = \request('contractor');
        $fomProject->owner_id = \request('owner');
        $fomProject->tenant_id = \request('tenant');
        $fomProject->building_id = \request('building');
        $fomProject->type = \request('type');
        $fomProject->status = \request('status');
        $fomProject->units = \request('units');//json_encode(\request('units'));
        $fomProject->oa_email = \request('oa_email');//json_encode(\request('units'));
        $fomProject->building_management_email = \request('building_management_email');//json_encode(\request('units'));
        $fomProject->levels = \request('levels');//json_encode(\request('units'));
        $fomProject->save();

        $changes = $fomProject->getChanges();

/*
        if (array_key_exists('owner_id',$changes)){
            FomTaskGroupUser::where('project_id',$fomProject->id)
                ->where('user_id',$owner_id)
                ->update([
                'user_id' => $fomProject->owner_id
            ]);
        }
        if (array_key_exists('tenant_id',$changes)){
            FomTaskGroupUser::where('project_id', $fomProject->id)
                ->where('user_id',$tenant_id)
                ->update([
                    'user_id' => $fomProject->tenant_id
                ]);
        }
        if (array_key_exists('contractor_id',$changes)){
            FomTaskGroupUser::where('project_id', $fomProject->id)
                ->where('user_id',$contractor_id)
                ->update([
                    'user_id' => $fomProject->contractor_id
                ]);
        }*/

        return new FomProjectSingleResource($fomProject);
    }

    public function store(FomProjectCreateRequest $request)
    {
        if (is_string(\request('building'))){
            $building = FomBuilding::create([
                'name' => \request('building')
            ]);

            $building_id = $building->id;
        }else{
            $building_id = \request('building');
        }

        $resource = FomProject::create([
            'units' => \request('units'),//json_encode(\request('units')),
            'owner_id' => \request('owner'),
            'tenant_id' => \request('tenant'),
            'contractor_id' => \request('contractor'),
            'building_id' => $building_id,
            'type' => \request('type'),
            'status' => \request('status'),
            'area' => \request('area'),
            'levels' => \request('levels'),
            'oa_email' => \request('oa_email'),
            'building_management_email' => \request('building_management_email'),
        ]);

        $this->update_project_notifiers($resource);

        return new FomProjectSingleResource($resource);
    }

    public function show(FomProject $fomProject)
    {
        return new FomProjectSingleResource(
            $fomProject
                ->load('tasks')
        );
    }

    private function update_project_notifiers($resource)
    {
        $tasksGroupsSettings = FomTaskGroupSettings::all();

        $fomManagers = User::whereHas("roles", function($q){ $q->where("name", Acl::ROLE_FOM_MANAGER); })->get();

        foreach ($tasksGroupsSettings as $tasksGroupsSetting){

            $DBtasksGroup = FomTaskGroup::create([
                'project_id' => $resource->id,
                'task_group_setting_id' => $tasksGroupsSetting->id,
                'due_date' => null,
                'assigned' => false
            ]);


            if ($tasksGroupsSetting->receiver_contractor){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->contractor->id,
                    'project_id' => $resource->id,
                    'type' => 'receiver',
                ]);
            }
            if ($tasksGroupsSetting->receiver_owner){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->owner->id,
                    'project_id' => $resource->id,
                    'type' => 'receiver',
                ]);
            }
            if ($tasksGroupsSetting->receiver_tenant){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->tenant->id,
                    'project_id' => $resource->id,
                    'type' => 'receiver',
                ]);
            }
            if ($tasksGroupsSetting->receiver_fom_manager){
                foreach ($fomManagers as $fomManager){
                    FomTaskGroupUser::firstOrCreate([
                        'task_group_id' => $DBtasksGroup->id,
                        'user_id' => $fomManager->id,
                        'project_id' => $resource->id,
                        'type' => 'receiver',
                    ]);
                }
            }
            if ($tasksGroupsSetting->sender_contractor){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->contractor->id,
                    'project_id' => $resource->id,
                    'type' => 'sender',
                ]);
            }
            if ($tasksGroupsSetting->sender_owner){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->owner->id,
                    'project_id' => $resource->id,
                    'type' => 'sender',
                ]);
            }
            if ($tasksGroupsSetting->sender_tenant){
                FomTaskGroupUser::firstOrCreate([
                    'task_group_id' => $DBtasksGroup->id,
                    'user_id' => $resource->tenant->id,
                    'project_id' => $resource->id,
                    'type' => 'sender',
                ]);
            }
            if ($tasksGroupsSetting->sender_fom_manager){
                foreach ($fomManagers as $fomManager){
                    FomTaskGroupUser::firstOrCreate([
                        'task_group_id' => $DBtasksGroup->id,
                        'user_id' => $fomManager->id,
                        'project_id' => $resource->id,
                        'type' => 'sender',
                    ]);
                }
            }

            foreach ($tasksGroupsSetting->tasks as $taskSetting){
                FomTask::create([
                    'project_id' => $resource->id,
                    'task_setting_id' => $taskSetting->id,
                    'task_group_id' => $DBtasksGroup->id,
                    'status' => 'Pending',
                    'notification_message' => $taskSetting->notification_message,
                ]);
            }
        }

    }

}
