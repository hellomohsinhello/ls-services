<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermitToWorkFormStoreRequest;
use App\Http\Resources\PermitToWorkFormCollection;
use App\Http\Resources\PermitToWorkFormSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomPermitToWorkForm;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;
use Illuminate\Http\Request;

class PermitToWorkFormController extends Controller
{
    public function indexPermitToWorkForm(FomTask $fomTask)
    {

        return new PermitToWorkFormCollection($fomTask->permit_to_works);

    }

    public function storePermitToWorkForm(PermitToWorkFormStoreRequest $request)
    {
//        $work_permit_validity = (array) \request('work_permit_validity');

        $fomPermitToWorkForm = FomPermitToWorkForm::create([
            'sub_contractors' => json_encode(\request('sub_contractors')),
            'approvals' => json_encode(\request('approvals')),
            'work_permit_number' => \request('work_permit_number'),
            'company_name' => \request('company_name'),
            'site_supervisor_name' => \request('site_supervisor_name'),
            'site_supervisor_contact_number' => \request('site_supervisor_contact_number'),
//            'start_date' => $work_permit_validity[0],
//            'end_date' => $work_permit_validity[1],
            'type_of_application' => \request('type_of_application'),
            'design_approvals' => json_encode(\request('design_approvals')),
//            'electricity_comment' => \request('electricity_comment'),
//            'noisy_non_noisy_works_comment' => \request('noisy_non_noisy_works_comment'),
//            'insurance_comment' => \request('insurance_comment'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new PermitToWorkFormSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdatePermitToWorkForm(FomPermitToWorkForm $fomPermitToWorkForm)
    {
        $work_permit_validity = (array) \request('work_permit_validity');

        $fomPermitToWorkForm->status = \request('status');
        $fomPermitToWorkForm->electricity_comment = \request('electricity_comment');
        $fomPermitToWorkForm->insurance_comment = \request('insurance_comment');
        $fomPermitToWorkForm->noisy_non_noisy_works_comment = \request('noisy_non_noisy_works_comment');
        $fomPermitToWorkForm->comment = \request('comment');
        $fomPermitToWorkForm->start_date = Carbon::parse(\request('start_date'));
        $fomPermitToWorkForm->end_date = Carbon::parse(\request('end_date'));
        $fomPermitToWorkForm->save();

        return new PermitToWorkFormSingleResource($fomPermitToWorkForm);

    }

    public function downlaod(FomPermitToWorkForm $form)
    {

        $pdf = PDF::loadView('fom.fuf.access-work-permit-form',compact('form'));
//        return $pdf->download('access-work-permit-form.pdf');
        return $pdf->stream('access-work-permit-form.pdf');
    }
}
