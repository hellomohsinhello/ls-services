<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomExitPassPermitStoreRequest;
use App\Http\Resources\FomExitPassPermitCollection;
use App\Http\Resources\FomExitPassPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomExitPassPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;


class FomExitPassPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomExitPassPermitCollection($fomTask->exit_pass_permits);
    }

    public function store(FomExitPassPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomExitPassPermit::create([
            'materials' => json_encode(\request('materials')),
            'b_date_of_exit' => \request('b_date_of_exit'),
            'c_address' => \request('c_address'),
            'c_city' => \request('c_city'),
            'c_contact_number' => \request('c_contact_number'),
            'c_email' => \request('c_email'),
            'c_primary_contact_name' => \request('c_primary_contact_name'),
            'c_title' => \request('c_title'),
            'co_address' => \request('co_address'),
            'co_company' => \request('co_company'),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));


        return new FomExitPassPermitSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomExitPassPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomExitPassPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.exit-pass-permit',compact('form'));

        return $pdf->stream('exit-pass-permit.pdf');

    }
}
