<?php

namespace App\Http\Controllers;

use App\Http\Resources\StatusResourceCollection;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class StatusController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Status::query();

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('area', 'LIKE', '%' . $keyword . '%');
        }

        return new StatusResourceCollection($resourceQuery->paginate($limit));

    }

    public function getStatuesByResource(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Status::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('area', 'LIKE', '%' . $keyword . '%');
        }

        return new StatusResourceCollection($resourceQuery->paginate($limit));

    }

}
