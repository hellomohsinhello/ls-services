<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonSingleResource;
use App\Models\Inspection;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyValuationController extends Controller
{
    public function show(Property $properties_valuation)
    {
        $properties_valuation->inspection()->firstOrCreate([]);

        return new CommonSingleResource($properties_valuation->load('inspection'));
    }
}
