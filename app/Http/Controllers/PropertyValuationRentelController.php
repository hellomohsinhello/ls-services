<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonResourceCollection;
use App\PropertyValuationRentel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PropertyValuationRentelController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = PropertyValuationRentel::query();
        $property = Arr::get($searchParams, 'property_id', '');
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        if (!empty($property)) {
            $resourceQuery->where('property_id',  $property);
        }

        return new CommonResourceCollection(
            $resourceQuery
                ->orderBy('created_at','asc')
                ->with([])->paginate($limit)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PropertyValuationRentel  $propertyValuationRentel
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyValuationRentel $propertyValuationRentel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PropertyValuationRentel  $propertyValuationRentel
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyValuationRentel $propertyValuationRentel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropertyValuationRentel  $propertyValuationRentel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyValuationRentel $propertyValuationRentel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropertyValuationRentel  $propertyValuationRentel
     * @return \Illuminate\Http\Response
     */

}
