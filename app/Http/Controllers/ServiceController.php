<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceResourceCollection;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Service::query();

        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('area', 'LIKE', '%' . $keyword . '%');
        }

        return new ServiceResourceCollection($resourceQuery->paginate($limit));

    }

}
