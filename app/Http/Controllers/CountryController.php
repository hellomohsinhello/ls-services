<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = Country::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');

        if (!empty($keyword)) {
            $resourceQuery->where('area', 'LIKE', '%' . $keyword . '%');
        }

        return new CountryResource($resourceQuery->with(['provinces','provinces.areas'])->paginate($limit));
    }

}
