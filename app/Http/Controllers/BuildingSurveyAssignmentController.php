<?php

namespace App\Http\Controllers;

use App\Http\Requests\BuildingSurveyAssignmentStoreRequest;
use App\Http\Resources\CommonResourceCollection;
use App\Http\Resources\CommonSingleResource;
use App\Laravue\Acl;
use App\Laravue\Models\User;
use App\Models\Assignment;
use App\Models\BuildingSurveyAssignment;
use App\Models\Service;
use App\Notifications\BuildingSurveyAssignmentCreatedNotification;
use App\Notifications\BuildingSurveyAssignmentStatusUpdateNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class BuildingSurveyAssignmentController extends Controller
{

    private $assignment;

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $resourceQuery = BuildingSurveyAssignment::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        if (!empty($keyword)) {
            $resourceQuery->where('title', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('ref_no', 'LIKE', '%' . $keyword . '%');
            $resourceQuery->orWhere('client_ref', 'LIKE', '%' . $keyword . '%');
        }


        if (!auth()->user()->hasAnyRole([
            Acl::ROLE_ADMIN,
            Acl::ROLE_BS_HEAD,
            Acl::ROLE_BS_MANAGER,
            Acl::ROLE_BS_ADMIN
        ])){

            $resourceQuery->whereHas('team', function($q){
                return $q->where('user_id',\auth()->id());
            });

        }


        return new CommonResourceCollection($resourceQuery->orderBy('created_at','desc')->with(['user','manager','client_contact','team'])->paginate($limit));


    }

    public function store(BuildingSurveyAssignmentStoreRequest $request)
    {

        $service = Service::find(1);

        $assignment = BuildingSurveyAssignment::create([
            'title' => \request('title'),
            'status' => 'New',
            'manager_id' => \request('manager'),
            'due_date' => Carbon::parse(\request('due_date')),
            'draft_issued_at' =>  \request('draft_issued_at') ? Carbon::parse(\request('draft_issued_at')):null,
            'final_issued_at' =>  \request('final_issued_at') ? Carbon::parse(\request('final_issued_at')):null,
            'contract_executed_at' =>  \request('contract_executed_at') ? Carbon::parse(\request('contract_executed_at')):null,
            
            'client_ref' => \request('client_ref'),
            'user_id' => Auth::user()->id,
            'report_lang' => \request('report_lang'),
            'client_contact_id' => \request('client_contact'),
            'price' => \request('price'),
            'service' => \request('service'),
            'target_date' =>  \request('target_date') ? Carbon::parse(\request('target_date')):null,
            'payment_percentage' => \request('payment_percentage'),
            'payment_received' => \request('payment_received'),
            'ref_no' => bs_assignment_ref_key(),
        ]);

        $assignment->team()->sync($request->get('team'));

//        notify team

        Notification::send($assignment->team, new BuildingSurveyAssignmentCreatedNotification($assignment,'team member'));

//        notify manager
        Notification::send($assignment->manager, new BuildingSurveyAssignmentCreatedNotification($assignment,'manager'));

        return new CommonSingleResource($assignment);
    }

    public function updateAttribute(BuildingSurveyAssignment $assignment)
    {

        $attributes = \request()->all();

        if (array_key_exists('notify_via_notification', $attributes)){

//            get name of status(status method)
            $method = \request('notify_via_notification');


//            check if that function is exists.
            if (method_exists($this, $method)
                && is_callable(array($this, $method)))
            {

                $this->assignment = $assignment;

//                call notification function
                call_user_func( array( $this, $method ) );

            }

            unset($attributes['notify_via_notification']);
        }

        foreach ($attributes as $key => $value){
            $assignment->{$key} = $value;
        }

        $assignment->save();

        return new CommonSingleResource($assignment);
    }

    public function notifyUponFinalized()
    {

        $user = User::role(Acl::ROLE_BS_HEAD)->get();

        Notification::send($user,new BuildingSurveyAssignmentStatusUpdateNotification($this->assignment,\request('status')));

    }

    public function notifyUponCompleted()
    {

        Notification::send($this->assignment->manager,new BuildingSurveyAssignmentStatusUpdateNotification($this->assignment,\request('status')));

    }

    public function update(BuildingSurveyAssignmentStoreRequest $request, BuildingSurveyAssignment $building_survey_assignment)
    {

        $building_survey_assignment->title = \request('title');
        $building_survey_assignment->manager_id = \request('manager');
        $building_survey_assignment->due_date = Carbon::parse(\request('due_date'));
        $building_survey_assignment->draft_issued_at = \request('draft_issued_at') ? Carbon::parse(\request('draft_issued_at')):null;
        $building_survey_assignment->final_issued_at = \request('final_issued_at') ? Carbon::parse(\request('final_issued_at')):null;
        $building_survey_assignment->contract_executed_at = \request('contract_executed_at') ? Carbon::parse(\request('contract_executed_at')):null;
        $building_survey_assignment->client_ref = \request('client_ref');
        $building_survey_assignment->report_lang = \request('report_lang');
        $building_survey_assignment->client_contact_id = \request('client_contact');
        $building_survey_assignment->payment_percentage = \request('payment_percentage');
        $building_survey_assignment->payment_received = \request('payment_received');
        $building_survey_assignment->price = \request('price');
        $building_survey_assignment->service = \request('service');
        $building_survey_assignment->target_date =  \request('target_date') ? Carbon::parse(\request('target_date')):null;


        $building_survey_assignment->team()->sync($request->get('team'));

        $building_survey_assignment->save();

        return new CommonSingleResource($building_survey_assignment);

    }

}
