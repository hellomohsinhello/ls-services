<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommonSingleResource;
use App\Http\Resources\DefectResource;
use App\Http\Resources\FomTaskSingleResource;
use App\Http\Resources\UserResource;
use App\Laravue\Models\User;
use App\Mail\TaskFileUploaded;
use App\Models\Defect;
use App\Models\FomTask;
use App\Models\Media;
use App\Models\Property;
use App\Notifications\TaskFileUploadedNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Spatie\PdfToImage\Pdf;

class ImageController extends Controller
{
    public function store()
    {
        $resource = \request('resource');

        switch ($resource){
            case 'defects':
                $defect = Defect::find(\request('resource_id'));
                $defect->addMediaFromRequest('file')->toMediaCollection('image');
                $response = [
                    'message' => 'Image Updated Successfully',
                    'type' => 'success',
                    'defect' => new DefectResource($defect)
                ];
                break;
            case 'avatar':
                $resource = User::find(\request('resource_id'));
                $resource->addMediaFromRequest('file')->toMediaCollection('avatar');
                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'user' => new UserResource($resource)
                    ]
                ];
                break;
            case 'signature':
                $resource = User::find(\request('resource_id'));
                $resource->addMediaFromRequest('file')->toMediaCollection('signature');
                $response = [
                    'data' => [
                        'message' => 'Signature Updated Successfully',
                        'type' => 'success',
                        'user' => new UserResource($resource)
                    ]
                ];
                break;
            case 'valuation-photos':
                $resource = Property::find(\request('resource_id'));
                $resource->addMediaFromRequest('file')->toMediaCollection('photos');
                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'property' => new CommonSingleResource($resource)
                    ]
                ];
                break;
            case 'valuation-appendices':
                $resource = Property::find(\request('resource_id'));
                $resource->addMediaFromRequest('file')->toMediaCollection('appendices');
                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'property' => new CommonSingleResource($resource)
                    ]
                ];
                break;
            case 'valuation-appendicess-pdf':
                $resource = Property::find(\request('resource_id'));
                $this->upload_pdf_to_save_images($resource, 'appendices');
                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'property' => new CommonSingleResource($resource)
                    ]
                ];
                break;
            case 'valuation-documents':
                $resource = Property::find(\request('resource_id'));

                $resource->addMediaFromRequest('file')->toMediaCollection('documents');

                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'property' => new CommonSingleResource($resource)
                    ]
                ];

                break;
            case 'valuation-documents-pdf':

                $resource = Property::find(\request('resource_id'));

                $this->upload_pdf_to_save_images($resource, 'documents');

                $response = [
                    'data' => [
                        'message' => 'Image Updated Successfully',
                        'type' => 'success',
                        'property' => new CommonSingleResource($resource)
                    ]
                ];

                break;
            case 'fom_tasks':
                $resource = FomTask::find(\request('resource_id'));

                $name = $resource->task_group->project->building->name . ' '
                    .$resource->task_group->project->units . '-stage'
                    .($resource->task_group->step + 1) . '-task'
                    .($resource->id);

                $resource->addMediaFromRequest('file')
                    ->usingFileName(str_slug($name).'.'.\request()->file('file')->getClientOriginalExtension())
                    ->toMediaCollection('file');

                if ($resource->task_group->isAssigned()){
                    Notification::send($resource->task_group->users, new TaskFileUploadedNotification($resource));
                }

                $response = [
                    'data' => [
                        'message' => 'Congrats, file upload successfully',
                        'type' => 'success',
                        'fom_task' => new FomTaskSingleResource($resource)
                    ]
                ];
                break;
        }

        return $response;
    }

    public function update(Media $image)
    {

        $image->title = \request('title');

        $image->save();

        return new CommonSingleResource($image);

    }

    public function destroy(Media $image)
    {
        $image->delete();
    }

    public function upload_pdf_to_save_images($resource,$collection)
    {

        if (\request()->hasFile('file') && \request()->file('file')->isValid()){

            $pdf_name = time().'.pdf';

            \request()->file->storeAs('pdf-docs',$pdf_name);

            if (file_exists(storage_path('app/pdf-docs').'/'.$pdf_name)){

                $pdf = new Pdf(storage_path('app/pdf-docs').'/'.$pdf_name);

                $pages = $pdf->getNumberOfPages();

                for ($i = 1; $i <= $pages; $i++){

                    $filename = storage_path('temp/'.time().'pdf-docs.jpg');

                    $pdf->setPage($i)->saveImage($filename);

                    $resource->addMedia($filename)->toMediaCollection($collection);
                }

            }

        }
    }

}
