<?php

namespace App\Http\Controllers;

use App\Models\Defect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuggestionController extends Controller
{
    public function getDefects($keyword)
    {

        $descriptions = DB::table('defects')
            ->distinct('description')
            ->whereNotNull('description')
            ->where('description','!=','')
            ->select('description as value');

        $poacs = DB::table('defects')
            ->distinct('poac')
            ->whereNotNull('poac')
            ->where('poac','!=','')
            ->select('poac as value');

        $components = DB::table('defects')
            ->distinct('component')
            ->whereNotNull('component')
            ->where('component','!=','')
            ->select('component as value');

        $locations = DB::table('defects')
            ->distinct('location')
            ->whereNotNull('location')
            ->where('location','!=','')
            ->select('location as value');

        $recs = DB::table('defects')
            ->distinct('rec')
            ->whereNotNull('rec')
            ->where('rec','!=','')
            ->select('rec as value');

        $suggesion = DB::table('defects')
            ->distinct('poac')
            ->select('poac as value')
            ->unionAll($poacs,$descriptions,$components,$locations,$recs)
            ->get();


        return $suggesion->unique();


        $descriptions = DB::table('defects')
            ->distinct('description')
            ->whereNotNull('description')
            ->where('description','!=','')
            ->select('description as value');

        $poacs = DB::table('defects')
            ->distinct('poac')
            ->whereNotNull('poac')
            ->where('poac','!=','')
            ->select('poac as value');

        $components = DB::table('defects')
            ->distinct('component')
            ->whereNotNull('component')
            ->where('component','!=','')
            ->select('component as value');

        $locations = DB::table('defects')
            ->distinct('location')
            ->whereNotNull('location')
            ->where('location','!=','')
            ->select('location as value');

        $recs = DB::table('defects')
            ->distinct('rec')
            ->whereNotNull('rec')
            ->where('rec','!=','')
            ->select('rec as value')
            ->unionAll($descriptions,$poacs,$components,$locations)
            ->get();


        return $recs->union($components)->unique();
    }
}
