<?php

namespace App\Http\Controllers;

use App\Laravue\Models\User;
use App\Models\Assignment;
use App\Models\Comment;
use App\Models\Property;
use Illuminate\Support\Facades\Auth;
USE PDFSnp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;


class ValuationReportController extends Controller
{
    public function preview(Assignment $assignment, Property $property)
    {

        $template_view = "valuations-reports.".$property->template->view;

        if (!View::exists($template_view)){
            dd($property->template->title.' Under Construction');
        }

//            return view($template_view, compact('assignment','property'));

//        dd(auth('api')->user());

        if (\request()->has('draft')){
            $this->addNewComment($property,'report draft is generated.');
        }
        if (\request()->has('final')){
            $this->addNewComment($property,'report final is generated.');
        }

        $pdf = PDFSnp::loadView($template_view, compact('assignment','property'))
            ->setOption('footer-center','Land Sterling Property Consultants | Confidential | Page [sitepage] of [sitepages]')
            ->setOption('footer-font-size','6')
            ->setOption('page-size','A4')
            ->setOption('title', $assignment->ref_no)
            ->setOption('header-html', \view('valuations-reports.layout.header'))
            ->setOption('header-spacing', 7)
            ->setOption('margin-top', '17mm')
            ->setOption('margin-left', '10mm')
            ->setOption('margin-right', '10mm')
            ->setOption('margin-bottom', '10mm');

        if (!$property->template->is_portfolio){
            $pdf->setOption('cover', \view('valuations-reports.layout.cover-page',compact('assignment','property')));
        }else{
            $pdf->setOption('cover', \view('valuations-reports.layout.cover-page-portfolio',compact('assignment','property')));
        }

        return $pdf->stream($property->ref_number.'.pdf');

    }

    public function addNewComment($property, $message)
    {
        if (\request()->has('u')){
            $comment = new Comment();
            $comment->commenter()->associate(User::find(\request()->get('u')));
            $comment->commentable()->associate($property);
            $comment->comment = $message;
            $comment->save();
        }
    }
}
