<?php

namespace App\Http\Controllers;

use App\Http\Requests\FomMaterialDeliveryPermitStoreRequest;
use App\Http\Resources\FomMaterialDeliveryPermitCollection;
use App\Http\Resources\FomMaterialDeliveryPermitSingleResource;
use App\Mail\FomFillUpFormFilled;
use App\Models\FomMaterialDeliveryPermit;
use App\Models\FomTask;
use App\Notifications\FomFillUpFormFilledNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PDF;

class FomMaterialDeliveryPermitController extends Controller
{
    public function index(FomTask $fomTask)
    {
        return new FomMaterialDeliveryPermitCollection($fomTask->material_delivery_permit);
    }

    public function store(FomMaterialDeliveryPermitStoreRequest $request)
    {

        $fomPermitToWorkForm = FomMaterialDeliveryPermit::create([
            'company' => \request('company'),
            'ls_permit_no' => \request('ls_permit_no'),
            'address' => \request('address'),
            'c_primary_contact_name' => \request('c_primary_contact_name'),
            'c_title' => \request('c_title'),
            'c_city' => \request('c_city'),
            'c_address' => \request('c_address'),
            'c_fax_no' => \request('c_fax_no'),
            'c_phone_no' => \request('c_phone_no'),
            'c_email' => \request('c_email'),
            'v_make_model' => \request('v_make_model'),
            'v_vehicle_height' => \request('v_vehicle_height'),
            'v_vehicle_registration' => \request('v_vehicle_registration'),
            'v_vehicle_weight' => \request('v_vehicle_weight'),
            'v_drivers_name' => \request('v_drivers_name'),
            'v_drivers_license_number' => \request('v_drivers_license_number'),
            'v_insurance_validity_period' => \request('v_insurance_validity_period'),
            'v_license_plate_number' => \request('v_license_plate_number'),
            'v_delivery_date' => \request('v_delivery_date'),
            'v_unit' => \request('v_unit'),
            'v_description_of_material' => \request('v_description_of_material'),
            'v_delivery_days' => json_encode(\request('v_delivery_days')),
            'task_id' => \request('task_id'),
            'status' => 'Pending',
        ]);

//        Mail::to($fomPermitToWorkForm->task->task_group->receivers)
//            ->send(new FomFillUpFormFilled($fomPermitToWorkForm->task));

        Notification::send($fomPermitToWorkForm->task->task_group->receivers,new FomFillUpFormFilledNotification($fomPermitToWorkForm->task));

        return new FomMaterialDeliveryPermitSingleResource($fomPermitToWorkForm);
    }

    public function statusUpdate(FomMaterialDeliveryPermit $permit)
    {
        $permit->status = \request('status');
        $permit->comment = \request('comment');
        $permit->save();
    }

    public function downlaod(FomMaterialDeliveryPermit $form)
    {

        $pdf = PDF::loadView('fom.fuf.material-delivery-permit',compact('form'));

        return $pdf->stream('material-delivery-permit.pdf');

    }
}
