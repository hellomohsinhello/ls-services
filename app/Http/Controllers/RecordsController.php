<?php

namespace App\Http\Controllers;

use App\Element;
use App\Laravue\Acl;
use App\Laravue\Models\User;
use App\Models\Client;
use App\Models\ClientContact;
use App\Models\Country;
use App\Models\FomBuilding;
use App\Models\PropertyType;
use App\Models\Service;
use Illuminate\Http\Request;

class RecordsController extends Controller
{
    public function getUsers()
    {
        return [
            'data' => User::with(['roles'])->get(),
        ];
    }

    public function getClients()
    {
        return [
            'data' => Client::with('contacts')->get(),
        ];
    }

    public function getBuildings()
    {

        if (!auth()->user()->hasAnyRole([
            Acl::ROLE_USER,
            Acl::ROLE_FOM_MANAGER,
            Acl::ROLE_ADMIN,
            Acl::ROLE_BS_HEAD,
        ])){
            abort(403);
        }

        $query = FomBuilding::query();

        $query->whereHas('projects',function($q){

            if (!auth()->user()->hasAnyRole([
                Acl::ROLE_FOM_MANAGER,
                Acl::ROLE_ADMIN,
                Acl::ROLE_BS_HEAD,
            ])){
                $q->where('contractor_id',auth()->user()->id);
                $q->orWhere('tenant_id',auth()->user()->id);
                return $q->orWhere('owner_id',auth()->user()->id);
            }

        })->get();

        $data = $query->with(['projects' => function($q){

            if (!auth()->user()->hasAnyRole([
                Acl::ROLE_FOM_MANAGER,
                Acl::ROLE_ADMIN,
                Acl::ROLE_BS_HEAD,
            ])){

                $q->where('contractor_id',auth()->user()->id);
                $q->orWhere('tenant_id',auth()->user()->id);
                return $q->orWhere('owner_id',auth()->user()->id);

            }

        }])->get();

        return [
            'data' => $data,
        ];

    }

    public function getServices()
    {
        return [
            'data' => Service::all(),
        ];
    }

    public function getCountries()
    {
        return [
            'data' => Country::with(['areas.province'])->get(),
        ];
    }
    public function getElements()
    {
        return [
            'data' => \App\Models\Element::all(),
        ];
    }

    public function getPropertyTypes()
    {
        return [
            'data' => PropertyType::all(),
        ];
    }
}
