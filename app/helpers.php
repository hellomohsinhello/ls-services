<?php

if (! function_exists('assignment_ref_key')) {

    function assignment_ref_key() {
        $thisYearNumber = \App\Models\Assignment::whereYear('created_at', date('Y'))
            ->where('ref_no','like','________VAL%')
            ->count();
        $thisYearNumber++;

        return date('Y-m').'-VAL-'.$thisYearNumber;
    }

    function bs_assignment_ref_key() {
        $thisYearNumber = \App\Models\BuildingSurveyAssignment::whereYear('created_at', date('Y'))
            ->where('ref_no','like','________BS%')
            ->count();
        $thisYearNumber++;

        return date('Y-m').'-BS-'.$thisYearNumber;
    }

    function get(&$var, $default=null) {
        return isset($var) ? $var : $default;
    }

    function update_properties_refs($assignment) {
        /*$properties = $assignment->properites()
            ->orderBy('created_at')
            ->get();*/
        $properties = \App\Models\Property::where('assignment_id',$assignment->id)->orderBy('created_at')->get();

        $alphabates = [
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
            'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
            'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
            'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
            'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
            'FA','FB','FC','FD','FF','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
            'GA','GB','GC','GD','GG','GG','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
        ];

        $i = 0;
        foreach ($properties as $property){
            $property->ref_number = $assignment->ref_no . $alphabates[$i];
            $property->save();
            $i++;
        }
    }
}
