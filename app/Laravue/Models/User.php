<?php

namespace App\Laravue\Models;

use App\Laravue\Acl;
use App\Models\Comment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property Role[] $roles
 *
 * @method static User create(array $user)
 * @package App
 */
class User extends Authenticatable implements JWTSubject, HasMedia
{
    use Notifiable, HasRoles, HasMediaTrait;

    /*public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }*/

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile();
        $this
            ->addMediaCollection('signature')
            ->singleFile();
    }

    protected $appends = ['avatar','signature','fullname'];

    /*protected static function boot(){

        parent::boot();

        static::addGlobalScope(new OrganizationScope());
    }*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'password', 'client_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Set permissions guard to API by default
     * @var string
     */
    protected $guard_name = 'api';

    /**
     * @inheritdoc
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @inheritdoc
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        foreach ($this->roles  as $role) {
            if ($role->isAdmin()) {
                return true;
            }
        }

        return false;
    }

    public function getAvatarAttribute()
    {

        return $this->getFirstMediaUrl('avatar') ?
            $this->getFirstMedia('avatar')->getFullUrl() :
            'https://www.gravatar.com/avatar/' .md5( strtolower( trim( $this->email ) ) );
    }

    public function getSignatureAttribute()
    {
        return $this->getFirstMediaUrl('signature') ? $this->getFirstMedia('signature')->getFullUrl() : '';
    }
    public function getSignaturePathAttribute()
    {
        return $this->getFirstMediaUrl('signature') ? $this->getFirstMedia('signature')->getPath() : '';
    }

    public function getFullnameAttribute()
    {
        return $this->firstname . ' '. $this->lastname;
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'commenter');
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'causer');
    }


    public function isMe()
    {
        return $this->id == auth()->id();
    }

    public function insider()
    {
        return $this->hasAnyRole([Acl::ROLE_ADMIN, Acl::ROLE_FOM_MANAGER]);
    }
    public function outsider()
    {
        return ! $this->insider();
    }

    public function canNotify()
    {
        if ($this->outsider())
            return true;

        return \request('notifications');
    }

    public function hasFomProjects()
    {
        return DB::table('fom_task_group_users')->where('user_id',$this->id)->exists();
    }

    public function hasBsProjects()
    {
        return DB::table('building_survey_teams')->where('user_id',$this->id)->exists();
    }

}
