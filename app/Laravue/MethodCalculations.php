<?php
/**
 * File Acl.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Laravue;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * Class Acl
 *
 * @package App\Laravue
 */
final class MethodCalculations
{
    public static function costAsIs($item)
    {
        return ($item->area_length) * ($item->complete_percent) * ($item->rate_area) / (100);
    }

    public static function costAsCompleted($item)
    {
        return ($item->area_length) * ($item->rate_area);
    }

    public static function permanentCostAsIs($item, $costs_ucs)
    {
        $total = 0;

        foreach ($costs_ucs as $costs_uc){
            if ($costs_uc->type == 'normal'){
                $total += self::costAsIs($costs_uc);

            }
        }

        return $total * $item->area_length * $item->complete_percent / 10000;
    }

    public static function permanentCostAsCompleted($item, $costs_ucs)
    {
        $total = 0;

        foreach ($costs_ucs as $costs_uc){
            if ($costs_uc->type == 'normal'){
                $total += self::costAsCompleted($costs_uc);
            }
        }

        return $total * $item->area_length / 100;
    }



    public static function total_cost_as_is($costs_ucs, $property)
    {
        $total = 0;
        $total_no_apply_profit = 0;

        foreach ($costs_ucs as $costs_uc){
            if ($costs_uc->type == 'normal'){
                if ($costs_uc->apply_profit){
                    $total += self::costAsIs($costs_uc);
                }else{
                    $total_no_apply_profit += self::costAsIs($costs_uc);
                }
            }elseif ($costs_uc->type == 'permanent'){
                if ($costs_uc->apply_profit){
                    $total += self::permanentCostAsIs($costs_uc, $costs_ucs);
                }else{
                    $total_no_apply_profit += self::permanentCostAsIs($costs_uc, $costs_ucs);
                }
            }
        }

        return ($total * (($property->cost_method_uc_dev_profit_as_is / 100) + 1)) + $total_no_apply_profit;
    }


    public static function total_cost_as_completed($costs_ucs, $property)
    {
        $total = 0;
        $total_no_apply_profit = 0;

        foreach ($costs_ucs as $costs_uc){
            if ($costs_uc->type == 'normal'){
                if ($costs_uc->apply_profit){
                    $total += self::costAsCompleted($costs_uc);
                }else{
                    $total_no_apply_profit += self::costAsCompleted($costs_uc);
                }
            }elseif ($costs_uc->type == 'permanent'){
                if ($costs_uc->apply_profit){
                    $total += self::permanentCostAsCompleted($costs_uc, $costs_ucs);
                }else{
                    $total_no_apply_profit += self::permanentCostAsCompleted($costs_uc, $costs_ucs);
                }
            }
        }

        return ($total * (($property->cost_method_uc_dev_profit_as_comp / 100) + 1)) + $total_no_apply_profit;
    }

}
