<?php
/**
 * File Acl.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Laravue;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/**
 * Class Acl
 *
 * @package App\Laravue
 */
final class Acl
{
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGEMENT = 'management';
    const ROLE_BS_HEAD = 'bs head';
    const ROLE_FOM_HEAD = 'fom head';
    const ROLE_FOM_MANAGER = 'fom manager';
    const ROLE_BS_MANAGER = 'bs manager';
    const ROLE_FOM_ADMIN = 'fom admin';
    const ROLE_BS_ADMIN = 'bs admin';
    const ROLE_USER = 'user';
    const ROLE_CLIENT_CONTACT = 'client contact';
    const ROLE_VALUTION_HEAD = 'valuation head';
    const ROLE_VALUTION_MANAGER = 'valuation manager';
    const ROLE_VALUTION_VALUAER = 'valuation valuer';
    const ROLE_VALUTION_ADMIN = 'valuation admin';

//    const ROLE_TENANT = 'tenant';
//    const ROLE_OWNER = 'owner';
//    const ROLE_CONTRACTOR = 'contractor';

    const PERMISSION_VIEW_MENU_BUILDING_SURVEY = 'view menu building survey';
    const PERMISSION_VIEW_MENU_ELEMENT_UI = 'view menu element ui';
    const PERMISSION_VIEW_MENU_PERMISSION = 'view menu permission';
    const PERMISSION_VIEW_MENU_CHARTS = 'view menu charts';
    const PERMISSION_VIEW_MENU_ADMINISTRATOR = 'view menu administrator';
//    const PERMISSION_VIEW_FIT_OUT_MANAGEMENT = 'view menu fit-out-management';
//    const PERMISSION_VIEW_BUILDING_INSPECTION = 'view menu building inspection';
    const PERMISSION_VIEW_PROPERTY_VALUATION = 'view menu property valuation';


    const PERMISSION_USER_MANAGE = 'manage user';
    const PERMISSION_USER_MANAGE_PROFILE = 'manage profile';
    const PERMISSION_PERMISSION_MANAGE = 'manage permission';
    const PERMISSION_ROLE_MANAGE_INFO = 'manage role info';

    /**
     * @param array $exclusives Exclude some permissions from the list
     * @return array
     */
    public static function permissions(array $exclusives = []): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function($value, $key) use ($exclusives) {
                return !in_array($value, $exclusives) && Str::startsWith($key, 'PERMISSION_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }

    public static function menuPermissions(): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function($value, $key) {
                return Str::startsWith($key, 'PERMISSION_VIEW_MENU_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }

    /**
     * @return array
     */
    public static function roles(): array
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $roles =  Arr::where($constants, function($value, $key) {
                return Str::startsWith($key, 'ROLE_');
            });

            return array_values($roles);
        } catch (\ReflectionException $exception) {
            return [];
        }
    }
}
