<?php

use Illuminate\Http\Request;
use \App\Laravue\Faker;
use \App\Laravue\JsonResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api'], function () {
    Route::post('auth/login', 'AuthController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');
    });

    Route::apiResource('users', 'UserController');

    Route::apiResource('users-notifications', 'UserNotificationController');

    Route::get('users/{user}/activities', 'UserController@activities');
    Route::put('users/{user}/password', 'UserController@passwordUpdate');
    Route::get('clients/types', 'ClientController@types');
    Route::get('clients/{client}/client-contacts', 'ClientController@clientContacts');
    Route::apiResource('clients', 'ClientController');

    Route::get('assignments/{assignment}/templates', 'AssignmentController@getTemplates');
    Route::get('assignments/properties', 'AssignmentController@getProperties');
    Route::put('assignments/{assignment}/status', 'AssignmentController@updateStatus');
    Route::get('assignments/{assignment}/inspection-types', 'AssignmentController@getInspections');
    Route::put('assignments/{assignment}/update-attributes', 'AssignmentController@updateAttribute');
    Route::apiResource('assignments', 'AssignmentController');
    Route::apiResource('property-valuation-inspections', 'PropertyValuationInspectionController');
    Route::apiResource('property-valuation-valuations', 'PropertyValuationValuationController');
    Route::apiResource('property-valuation-costs', 'PropertyValuationCostController');
    Route::apiResource('property-valuation-costs-uc', 'PropertyValuationCostUCController');
    Route::apiResource('property-comparables', 'PropertyComparablesController');
    Route::apiResource('property-rentels', 'PropertyRentelsController');
    Route::apiResource('properties-valuations', 'PropertyValuationController');
    Route::apiResource('property-valuation-comparables', 'PropertyValuationComparableController');
    Route::apiResource('property-valuation-breakdowns', 'PropertyValuationBreakdownController');
    Route::apiResource('property-valuation-rentels', 'PropertyValuationRentelController');

    Route::apiResource('leads', 'LeadController');

    Route::apiResource('maps-locations', 'MapsLocationController');

    Route::get('properties/{property}/defects', 'PropertyController@getDefects');
    Route::put('defects/sort', 'DefectController@sort');
    Route::apiResource('properties', 'PropertyController');

    Route::get('fuf/permitToWorkForm/{form}/download', 'PermitToWorkFormController@downlaod')->name('permitToWorkForm.download');
    Route::get('fuf/permitToWorkForm/{fomTask}', 'PermitToWorkFormController@indexPermitToWorkForm');
    Route::post('fuf/permitToWorkForm/{fomPermitToWorkForm}/status', 'PermitToWorkFormController@statusUpdatePermitToWorkForm');
    Route::post('fuf/permitToWorkForm', 'PermitToWorkFormController@storePermitToWorkForm');


    Route::get('fuf/IDCardForm/{form}/download', 'FomIDCardFormController@downlaod')->name('IDCardForm.download');
    Route::get('fuf/IDCardForm/{fomTask}', 'FomIDCardFormController@index');
    Route::post('fuf/IDCardForm/{fomIDCardForm}/status', 'FomIDCardFormController@statusUpdate');
    Route::post('fuf/IDCardForm', 'FomIDCardFormController@store');

    Route::get('fuf/attendanceForm/{form}/download', 'FomAttendanceFormController@downlaod')->name('attendanceForm.download');
    Route::get('fuf/attendanceForm/{fomTask}', 'FomAttendanceFormController@index');
    Route::post('fuf/attendanceForm/{fomAttendanceForm}/status', 'FomAttendanceFormController@statusUpdate');
    Route::post('fuf/attendanceForm', 'FomAttendanceFormController@store');

    Route::get('fuf/commonAreaDilapidation/{form}/download', 'FomCommonAreaDilapidationController@downlaod')->name('commonAreaDilapidation.download');
    Route::get('fuf/commonAreaDilapidation/{fomTask}', 'FomCommonAreaDilapidationController@index');
    Route::post('fuf/commonAreaDilapidation/{dilapidation}/status', 'FomCommonAreaDilapidationController@statusUpdate');
    Route::post('fuf/commonAreaDilapidation', 'FomCommonAreaDilapidationController@store');

    Route::get('fuf/deliveryAccessPermit/{form}/download', 'FomDeliveryAccessPermitController@downlaod')->name('deliveryAccessPermit.download');
    Route::get('fuf/deliveryAccessPermit/{fomTask}', 'FomDeliveryAccessPermitController@index');
    Route::post('fuf/deliveryAccessPermit/{permit}/status', 'FomDeliveryAccessPermitController@statusUpdate');
    Route::post('fuf/deliveryAccessPermit', 'FomDeliveryAccessPermitController@store');

    Route::get('fuf/certificateOfConformity/{form}/download', 'FomCertificateOfConformityController@downlaod')->name('certificateOfConformity.download');
    Route::get('fuf/certificateOfConformity/{fomTask}', 'FomCertificateOfConformityController@index');
    Route::post('fuf/certificateOfConformity/{dilapidation}/status', 'FomCertificateOfConformityController@statusUpdate');
    Route::post('fuf/certificateOfConformity', 'FomCertificateOfConformityController@store');

    Route::get('fuf/exitPassPermit/{form}/download', 'FomExitPassPermitController@downlaod')->name('exitPassPermit.download');
    Route::get('fuf/exitPassPermit/{fomTask}', 'FomExitPassPermitController@index');
    Route::post('fuf/exitPassPermit/{permit}/status', 'FomExitPassPermitController@statusUpdate');
    Route::post('fuf/exitPassPermit', 'FomExitPassPermitController@store');

    Route::get('fuf/hotWorkPermit/{form}/download', 'FomHotWorkPermitController@downlaod')->name('hotWorkPermit.download');
    Route::get('fuf/hotWorkPermit/{fomTask}', 'FomHotWorkPermitController@index');
    Route::post('fuf/hotWorkPermit/{permit}/status', 'FomHotWorkPermitController@statusUpdate');
    Route::post('fuf/hotWorkPermit', 'FomHotWorkPermitController@store');

    Route::get('fuf/extendedHoursPermit/{form}/download', 'FomExtendedHoursPermitController@downlaod')->name('extendedHoursPermit.download');
    Route::get('fuf/extendedHoursPermit/{fomTask}', 'FomExtendedHoursPermitController@index');
    Route::post('fuf/extendedHoursPermit/{permit}/status', 'FomExtendedHoursPermitController@statusUpdate');
    Route::post('fuf/extendedHoursPermit', 'FomExtendedHoursPermitController@store');


    Route::get('fuf/accessWorkPermit/{form}/download', 'FomAccessWorkPermitController@downlaod')->name('accessWorkPermit.download');
    Route::get('fuf/accessWorkPermit/{fomTask}', 'FomAccessWorkPermitController@index');
    Route::post('fuf/accessWorkPermit/{permit}/status', 'FomAccessWorkPermitController@statusUpdate');
    Route::post('fuf/accessWorkPermit', 'FomAccessWorkPermitController@store');

    Route::get('fuf/firstFixInspection/{form}/download', 'FomFirstFixInspectionController@downlaod')->name('firstFixInspection.download');
    Route::get('fuf/firstFixInspection/{fomTask}', 'FomFirstFixInspectionController@index');
    Route::post('fuf/firstFixInspection/{permit}/status', 'FomFirstFixInspectionController@statusUpdate');
    Route::post('fuf/firstFixInspection', 'FomFirstFixInspectionController@store');

    Route::get('fuf/vehicleParkingRegister/{form}/download', 'FomVehicleParkingRegisterController@downlaod')->name('vehicleParkingRegister.download');
    Route::get('fuf/vehicleParkingRegister/{fomTask}', 'FomVehicleParkingRegisterController@index');
    Route::post('fuf/vehicleParkingRegister/{permit}/status', 'FomVehicleParkingRegisterController@statusUpdate');
    Route::post('fuf/vehicleParkingRegister', 'FomVehicleParkingRegisterController@store');

    Route::get('fuf/wasteDisposalPermit/{form}/download', 'FomWasteDisposalPermitController@downlaod')->name('wasteDisposalPermit.download');
    Route::get('fuf/wasteDisposalPermit/{fomTask}', 'FomWasteDisposalPermitController@index');
    Route::post('fuf/wasteDisposalPermit/{permit}/status', 'FomWasteDisposalPermitController@statusUpdate');
    Route::post('fuf/wasteDisposalPermit', 'FomWasteDisposalPermitController@store');

    Route::get('fuf/hseWeeklyInspection/{form}/download', 'FomHseWeeklyInspectionController@downlaod')->name('hseWeeklyInspection.download');
    Route::get('fuf/hseWeeklyInspection/{fomTask}', 'FomHseWeeklyInspectionController@index');
    Route::post('fuf/hseWeeklyInspection/{permit}/status', 'FomHseWeeklyInspectionController@statusUpdate');
    Route::post('fuf/hseWeeklyInspection', 'FomHseWeeklyInspectionController@store');

    Route::get('fuf/materialDeliveryPermit/{form}/download', 'FomMaterialDeliveryPermitController@downlaod')->name('materialDeliveryPermit.download');
    Route::get('fuf/materialDeliveryPermit/{fomTask}', 'FomMaterialDeliveryPermitController@index');
    Route::post('fuf/materialDeliveryPermit/{permit}/status', 'FomMaterialDeliveryPermitController@statusUpdate');
    Route::post('fuf/materialDeliveryPermit', 'FomMaterialDeliveryPermitController@store');

    Route::get('fuf/disciplinaryActionIssued/{form}/download', 'FomDisciplinaryActionIssuedController@downlaod')->name('disciplinaryActionIssued.download');
    Route::get('fuf/disciplinaryActionIssued/{fomTask}', 'FomDisciplinaryActionIssuedController@index');
    Route::post('fuf/disciplinaryActionIssued/{permit}/status', 'FomDisciplinaryActionIssuedController@statusUpdate');
    Route::post('fuf/disciplinaryActionIssued', 'FomDisciplinaryActionIssuedController@store');

    Route::get('fuf/mepServicesPermit/{form}/download', 'FomMepServicesPermitController@downlaod')->name('mepServicesPermit.download');
    Route::get('fuf/mepServicesPermit/{fomTask}', 'FomMepServicesPermitController@index');
    Route::post('fuf/mepServicesPermit/{permit}/status', 'FomMepServicesPermitController@statusUpdate');
    Route::post('fuf/mepServicesPermit', 'FomMepServicesPermitController@store');

    Route::get('fuf/incidentNotification/{form}/download', 'FomIncidentNotificationController@downlaod')->name('incidentNotification.download');
    Route::get('fuf/incidentNotification/{fomTask}', 'FomIncidentNotificationController@index');
    Route::post('fuf/incidentNotification/{permit}/status', 'FomIncidentNotificationController@statusUpdate');
    Route::post('fuf/incidentNotification', 'FomIncidentNotificationController@store');

    Route::get('fuf/hseBreachReport/{form}/download', 'FomHseBreachReportController@downlaod')->name('hseBreachReport.download');
    Route::get('fuf/hseBreachReport/{fomTask}', 'FomHseBreachReportController@index');
    Route::post('fuf/hseBreachReport/{permit}/status', 'FomHseBreachReportController@statusUpdate');
    Route::post('fuf/hseBreachReport', 'FomHseBreachReportController@store');

   /* Route::get('fuf/hseBreachReport/{fomTask}', 'FomHseBreachReportController@index');
    Route::post('fuf/hseBreachReport/{permit}/status', 'FomHseBreachReportController@statusUpdate');
    Route::post('fuf/hseBreachReport', 'FomHseBreachReportController@store');*/

    //fom
    Route::put('fom-projects/{fom_task_group}/groups/update', 'FomTaskGroupSettingsController@groupsTasksUpdate');
    Route::put('fom-projects/{fom_task}/tasks/update', 'FomTaskGroupSettingsController@tasksUpdate');
    Route::put('fom-projects/{fom_task}/tasks/notify', 'FomTaskGroupSettingsController@tasksNotify');
    Route::get('fom-projects/{fom_project}/groups/tasks', 'FomTaskGroupSettingsController@groupsTasks');

    Route::apiResource('task-group-assign', 'TaskGroupAssignController');
    Route::apiResource('defects', 'DefectController');
    Route::apiResource('fom-projects', 'FomProjectController');
    Route::apiResource('areas', 'AreaController');
    Route::apiResource('fom-tasks-groups', 'FomTaskGroupSettingsController');
    Route::get('fom-tasks/{fom_task}/{user}/file-download', 'FomTaskController@downloadFile')->name('fom-tasks-file-download');
    Route::apiResource('statuses', 'StatusController');
    Route::apiResource('services', 'ServiceController');
    Route::apiResource('countries', 'CountryController');
    Route::apiResource('client-contacts', 'ClientContactController');
    Route::apiResource('elements', 'ElementController');
    Route::apiResource('organizations', 'OrganizationController');
    Route::get('users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::put('users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::apiResource('roles', 'RoleController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::get('roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::put('roles/{role}/info', 'RoleController@updateInfo')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ROLE_MANAGE_INFO);;
    Route::apiResource('permissions', 'PermissionController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);


    //Select recordscreate-defect
    Route::get('/comments/projects/{fom_project}', 'CommentController@getByProject');
    Route::apiResource('comments', 'CommentController');


    Route::put('building-survey-defects/sort', 'BuildingSurveyDefectController@sort');
    Route::apiResource('building-survey-defects', 'BuildingSurveyDefectController');

    Route::apiResource('building-survey-properties', 'BuildingSurveyPropertyController');

    Route::apiResource('building-survey-assignment', 'BuildingSurveyAssignmentController');
    Route::put('building-survey-assignment/{assignment}/update-attributes', 'BuildingSurveyAssignmentController@updateAttribute');

    Route::get('resource-selection/{resource}', 'ResourceSelectionController@get');




    Route::get('records/users', 'RecordsController@getUsers');
    Route::get('records/clients', 'RecordsController@getClients');
    Route::get('records/services', 'RecordsController@getServices');
    Route::get('records/countries', 'RecordsController@getCountries');
    Route::get('records/elements', 'RecordsController@getElements');
    Route::get('records/buildings', 'RecordsController@getBuildings');
    Route::get('records/property-types', 'RecordsController@getPropertyTypes');

    Route::get('suggestions/building-survey-defects/{keyword}', 'SuggestionController@getDefects');

    Route::post('images/create-defect', 'DefectController@create');
    Route::apiResource('images', 'ImageController');
//    Route::post('avatar', 'ImageController@store');


    // Fake APIs
    Route::get('/table/list', function () {
        $rowsNumber = mt_rand(20, 30);
        $data = [];
        for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
            $row = [
                'author' => Faker::randomString(mt_rand(5, 10)),
                'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
                'id' => mt_rand(100000, 100000000),
                'pageviews' => mt_rand(100, 10000),
                'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
                'title' => Faker::randomString(mt_rand(20, 50)),
            ];

            $data[] = $row;
        }

        return response()->json(new JsonResponse(['items' => $data]));
    });

    Route::get('/orders', function () {
        $rowsNumber = 8;
        $data = [];
        for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
            $row = [
                'order_no' => 'LARAVUE' . mt_rand(1000000, 9999999),
                'price' => mt_rand(10000, 999999),
                'status' => Faker::randomInArray(['success', 'pending']),
            ];

            $data[] = $row;
        }

        return response()->json(new JsonResponse(['items' => $data]));
    });

    Route::get('/articles', function () {
        $rowsNumber = 10;
        $data = [];
        for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
            $row = [
                'id' => mt_rand(100, 10000),
                'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
                'title' => Faker::randomString(mt_rand(20, 50)),
                'author' => Faker::randomString(mt_rand(5, 10)),
                'comment_disabled' => Faker::randomBoolean(),
                'content' => Faker::randomString(mt_rand(100, 300)),
                'content_short' => Faker::randomString(mt_rand(30, 50)),
                'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
                'forecast' => mt_rand(100, 9999) / 100,
                'image_uri' => 'https://via.placeholder.com/400x300',
                'importance' => mt_rand(1, 3),
                'pageviews' => mt_rand(10000, 999999),
                'reviewer' => Faker::randomString(mt_rand(5, 10)),
                'timestamp' => Faker::randomDateTime()->getTimestamp(),
                'type' => Faker::randomInArray(['US', 'VI', 'JA']),

            ];

            $data[] = $row;
        }

        return response()->json(new JsonResponse(['items' => $data, 'total' => mt_rand(1000, 10000)]));
    });

    Route::get('articles/{id}', function ($id) {
        $article = [
            'id' => $id,
            'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
            'title' => Faker::randomString(mt_rand(20, 50)),
            'author' => Faker::randomString(mt_rand(5, 10)),
            'comment_disabled' => Faker::randomBoolean(),
            'content' => Faker::randomString(mt_rand(100, 300)),
            'content_short' => Faker::randomString(mt_rand(30, 50)),
            'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
            'forecast' => mt_rand(100, 9999) / 100,
            'image_uri' => 'https://via.placeholder.com/400x300',
            'importance' => mt_rand(1, 3),
            'pageviews' => mt_rand(10000, 999999),
            'reviewer' => Faker::randomString(mt_rand(5, 10)),
            'timestamp' => Faker::randomDateTime()->getTimestamp(),
            'type' => Faker::randomInArray(['US', 'VI', 'JA']),

        ];

        return response()->json(new JsonResponse($article));
    });

    Route::get('articles/{id}/pageviews', function ($id) {
        $pageviews = [
            'PC' => mt_rand(10000, 999999),
            'Mobile' => mt_rand(10000, 999999),
            'iOS' => mt_rand(10000, 999999),
            'android' => mt_rand(10000, 999999),
        ];
        $data = [];
        foreach ($pageviews as $device => $pageview) {
            $data[] = [
                'key' => $device,
                'pv' => $pageview,
            ];
        }

        return response()->json(new JsonResponse(['pvData' => $data]));
    });

});
