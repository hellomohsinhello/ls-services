<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('clean-data',function (){
    ini_set('max_execution_time', '0');

    dd('ok');
/*
    $comps = \App\Models\PropertyValuationComparable::take(10000)->whereNull('area_new')->whereNotNull('area')->get();

    foreach ($comps as $comp){
        try{
            $comp->area_new = (float) $comp->area;
            $comp->area = null;
            $comp->save();
        }catch (Exception $e){

        }
    }

    dd($comps->count());*/

});
Route::get('properties/{property}/report', 'PropertyController@getReport');
Route::get('properties/report/footer.html', function(){
    $query = request()->all();
    return view('valuations-reports.layout.footer',compact('query'));
})->name('report.footer');

Route::get('valuations/{assignment}/properties/{property}/print-preview', 'ValuationReportController@preview');



Route::prefix('/api/app')->group(function () {
    Route::get('assignments', 'BSAppController@getAssignments');
    Route::get('assignments/{assignment}/properties', 'BSAppController@getProperties');
    Route::post('assignments/{assignment}/properties/create', 'BSAppController@saveProperty');
    Route::get('properties/{property}/defects', 'BSAppController@getDefects');
    Route::post('properties/{property}/defects/create', 'BSAppController@saveDefects');

});

Route::post('/api/auth/user',function (){
    return auth()->user();
});

Route::group(['middleware' => 'web'], function () {
    Route::get('/{any}', 'LaravueController@index')->where('any', '.*');
});


